(in-package :my-project)

   #:Graphical-View #:Graphical-View-3D #:Graphical-View-Text #:Graphical-Graph
   #:make-graphical-text #:make-graphical-graph #:make-spherebb #:make-cubebb #:make-bounding-box
   #:refresh-graphic-view #:make-graphic-view-3d #:make-graphic-view-text


(define-application-frame superapp ()
  ()
  (:pointer-documentation t)
  (:panes
   (hello-world :application :display-function 'display-hello-world)
   (some-pane :application :display-function 'display-some-pane)
   (another-pane :application :display-function 'display-another-pane)
   (int :interactor :height 200 :width 800))
  (:layouts
   (default 
       (horizontally (:height 600 :width 800)
	 (1/4 hello-world)
	 (1/4 some-pane)
	 (1/4 draw-graph)
	 (:fill int)))))

(defun main-program ()
  (format t "The program is run from here.")
  (format t "~%Step 1: Create a graph (node, edge,)")
  (format t "~%Step 2: Randomize positions")
  (format t "~%Step 3: Create interface: canvas") 
  (format t "~%Step 4: Draw and layout")
  (run-frame-top-level (make-application-frame 'superapp)))

(define-superapp-command (com-quit :name t) ()
  (frame-exit *application-frame*))

(defmethod display-hello-world ((frame superapp) stream)
  (format stream "Hello, world"))

(defmethod display-another-pane ((frame superapp) stream)
  (declare (ignore stream))
  (let ((pane (get-frame-pane *application-frame* 'another-pane)))
    (window-clear pane)
    (draw-rectangle* pane 10 10 150 100 :filled nil :line-thickness 2)
    (draw-ellipse* pane 100 250 50 0 0 30 :filled nil :line-thickness 2)))

(defmethod display-some-pane ((frame superapp) stream)
  (declare (ignore stream))
  (let ((pane (get-frame-pane *application-frame* 'some-pane)))
    (window-clear pane)
    (draw-line* pane 20 20 100 80)
    (draw-arrow* pane 20 100 100 150 :to-head t :head-width 10 :head-length 10)))

;"http://groups.google.com/group/comp.lang.lisp/browse_thread/thread/3c329422f09e46ea/815b862108d8e650#815b862108d8e650"
;(defun load-file-meta-tool (path)
 ; (with-open-file (stream path)         ; do not hard code pathnames!
  ;                                      ; :direction :intput is the default so...
   ; (let ((data '()))
    ;  (do ((line (read-line stream nil 'eof) ; what if the file is empty?
     ;            (read-line stream nil 'eof)))
      ;    ((eq line 'eof)
       ;    ;; "Reached end of file." ; And you find that funny?
        ;   ;; Return something useful!
         ;  data)
       ; (when (and (plusp (length line)) (string= "-CAT" (string-trim " " line)))
          ;(print line)
          ;(let ((line (refine-reaction line))) ; do not use setf! let is more efficient.
            ;; DO NOT USE GLOBAL VARIABLES! values is more efficient!
           ; (multiple-value-bind (name input output) (parse-reaction line)
            ;  (push (list name input output) data)
             ; (format t "~%Name: ~a~&- Input: ~s~&- Output: ~s~&"
              ;        name input output))))

;;; using ltk for GUIs
;Ltk is quite popular, very portable, and reasonably well documented through the Tk docs. 
;Installation on SBCL is as easy as saying:
;(require :asdf-install)
;(asdf-install:install :ltk)

;(setf *debug-ltk* t)
(in-package :ltk)

(defun hello-world-1 ()
  (with-ltk ()
    (let ((b (make-instance 'button
			    :master nil
			    :text "Press Me"
			    :command (lambda () (format t "Hello, World!~&")))))
      (pack b))))

(defun hello-world-3 ()
  (with-ltk ()
    (let* ((f (make-instance 'frame))
           (b (make-instance 'button
                             :master f
                             :text "Hello World"
                             :command (lambda ()
                                        (do-msg "Bye!" "Hello World!")
                                        (setf *exit-mainloop* t)))))
      (pack f)
      (pack b :side :center)
      (configure f :borderwidth 3)
      (configure f :width 300 :height 180 :relief :flat))))

(defun scribble ()
  (with-ltk ()
    (let* ((canvas (make-instance 'canvas))
	   (down nil))
      (pack canvas)
      (bind canvas "<ButtonPress-1>"
	    (lambda (evt)
	      (setf down t)                                    
	      (create-oval canvas
			   (- (event-x evt) 10) (- (event-y evt) 10)
			   (+ (event-x evt) 10) (+ (event-y evt) 10))))
      (bind canvas "<ButtonRelease-1>" (lambda (evt) 
					 (declare (ignore evt))
					 (setf down nil)))
      (bind canvas "<Motion>"
	    (lambda (evt)
	      (when down
		(create-oval canvas
			     (- (event-x evt) 10) (- (event-y evt) 10)
			     (+ (event-x evt) 10) (+ (event-y evt) 10))))))))
;;; create canvas
(defun canvas-test ()
  (with-ltk ()
    (let* ((sc (make-instance 'scrolled-canvas))
           (c (canvas sc))
           (l1 (create-line c (list 100 100 400 50 700 150)))
           (l2 (create-line c (list 10 10 5 80 65 80 10 10)))
           (polygon (create-polygon c (list 50 150 250 160 250 300 50 330)))
           (text (create-text c 260 250 "Canvas test")))
      (pack sc :expand 1 :fill :both)
      (scrollregion c 0 0 800 800))))
;;;;http://algowiki.net/wiki/index.php/Kosaraju's_algorithm
import java.util.ArrayList;

public class Kosaraju {

   private ArrayList<Node> stack;

   public ArrayList<ArrayList<Node>> getSCC(Node root, AdjacencyList list){
       stack = new ArrayList<Node>();

       // search the graph (depth-first search), adding nodes to the stack
       search(root, list, true);

       // reverse all the edges in the graph
       list.reverseGraph();

       // search the graph again in the stack's order
       ArrayList<ArrayList<Node>> SCC = new ArrayList<ArrayList<Node>>();
       while(stack.size() != 0){
           ArrayList<Node> component = new ArrayList<Node>();
           search(stack.get(0), list, false);

           // any components we visited are strongly connected
           // remove them from the stack and add them to the component
           for(int i=0; i<stack.size(); i++){
               if(!stack.get(i).visited){
                   component.add(stack.remove(i));
                   // list shifted left, offset the index accordingly
                   i--;
               }
           }

           // add the component to the result set
           SCC.add(component);
       }
       return SCC;
   }

   private void search(Node root, AdjacencyList list, boolean forward){
       root.visited = forward;
       if(list.getAdjacent(root) == null){
           if(forward) stack.add(0, root);
           return;
       }
       for(Edge e : list.getAdjacent(root)){
           if(e.to.visited != forward){
               search(e.to, list, forward);
           }
       }
       if(forward) stack.add(0, root);
   }
}

;;;;
    
//This code works for turbo c++
//Solution for strongly connected components or Kosaraju's algorithm
//for nitc students
//I use this because there is no other way i know how to do post a //code.




///////code:-


#include<iostream.h>
//#include<math.h>
#include<conio.h>

//int time;
//time=0;
char color[10];
int s[10];
int f[10];
int nn;
int fin[20];

struct node
{
int no;
node* next;
}*beg,*np,*np1,*temp,*temp1,*temp2;

node *graph[50];
node *grapht[50];

node* createnxtn(int n)
{
np=new node;
np->no=n;
np->next=NULL;
return np;
}

void insert(node* np)
{
if(beg==NULL)
{ beg=np;
}
else
{
temp=beg;
beg=np;
np->next=temp;
}
}

void display(node* np)
{
while(np!=NULL)
{
cout<<np->no<<"-";
np=np->next;
}
}

void del(node*np,node*np1)
{
np->next=np1->next;
delete np1;
}

void visit(int j,node *a[])
{
color[j]='g';
//time=time+1;
//s[j]=time;
temp=a[j];
while(temp!=NULL)
{
int v=temp->no;
if(color[v]=='w')
{
visit(v,a);
temp=a[j]->next;
}
else
{
temp=temp->next; }
}
cout<<j<<",";
color[j]='b';
//time=time+1;
// f[j]=time;
}

void dfs(int n,node *a[])
{
for(int j=1;j<=n;j++)
{
color[j]='w';
}
for(int i=1;i<=n;i++)
{
if(color[i]=='w')
visit(i,a);
cout<<"\n";
}
}

void trans(int n)
{
for(int i=1;i<=n;i++)
{
color[i]='w';
np=createnxtn(i);
grapht[i]=np;
}
for(int j=1;j<=n;j++)
{
temp=graph[j]->next;
while(temp!=NULL)
{
int x=temp->no;
np=createnxtn(j);
temp1=grapht[x];
np->next=temp1->next;
temp1->next=np;
temp=temp->next;
}
}
}

void scc(int n)
{
dfs(n,graph);
for(int k=1;k<=n;k++)
{
color[k]='w';
int t=f[k];
fin[t]= k;
}
trans(n);
for(k=(2*n);k>=1;k--)
{
if((fin[k]!=NULL)&&(color[fin[k]])=='w')
{
int z=fin[k];
visit(z,grapht);
cout<<"\n";
}
}
}






void crteG(int n)
{
int i=1;
char ch='y';
int sd;
nn=0;
int x=0;
while(i<=n)
{
cout<<"Enter no. of nodes adjacent to "<<i<<" ";
cin>>sd;
while(x<sd)
{
cout<<"enter the ajacent node of node no:"<<i<<" ";
cin>>nn;
createnxtn(nn);
insert(np);
x++;
}
graph[i]=beg;
beg=NULL;
i++;
x=0;
}
}


int main()
{
int n;
cout<<"enter the no of nodes";
cin>>n;
crteG(n);
scc(n);
getch();
return 0;
} 
;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;; Implement in detail

;; (defun draw-arrow (start vector size color)
;;   "Draw thick line (tube) running from 'start, length 'vector with (head) 'size and 'color"
;;   (setf head-height (* size 2.0))
;;   (setf quad (glu:new-quadric))
;;   (gl:color-3f color)
;;   (gl:push-matrix)
;;   )

;; http://www.itee.uq.edu.au/~patterns/repository/network-diagram/
;; void NCanvas::drawArrow(gl_pos orig, gl_pos dest, bool act)
;; {
;;       // get vector
;;       double x = dest.x - orig.x;
;;       double y = dest.y - orig.y;
;;       double z = dest.z - orig.z;
 
;;       // angle of head arm (180 degrees == PI radians)
;;       double t = 10.0 * PI / 180;
 
;;       // length of arrow arm
;;       double d = 0.08;
 
;;       // vector angle from x axis
;;       double g_xy = 0;
;;       double g_xz = 0;
;;       // fix angles if x <= 0
;;       if (x == 0)
;;       {
;;             if (y > 0)
;;                   g_xy = PI / 2;
;;             if (y < 0)
;;                   g_xy = -PI / 2;
;;             if (z > 0)
;;                   g_xz = PI / 2;
;;             if (z < 0)
;;                   g_xz = -PI / 2;
;;       }
;;       // y/-x -> negative angle, -y/-x -> positive angle
;;       else if (x < 0)
;;       {
;;             g_xy = PI + atan(y/x);
;;             g_xz = PI + atan(z/x);
;;       }
;;       else
;;       {
;;             g_xy = atan(y/x);
;;             g_xz = atan(z/x);
;;       }
 
;;       // point 1
;;       gl_pos p1;
;;       p1.x = dest.x + d * cos(g_xy + PI - t);
;;       p1.y = dest.y + d * sin(g_xy + PI - t);
;;       p1.z = dest.z + d * sin(g_xz + PI - t);
 
;;       // point 2
;;       gl_pos p2;
;;       p2.x = dest.x + d * cos(g_xy + PI + t);
;;       p2.y = dest.y + d * sin(g_xy + PI + t);
;;       p2.z = dest.z + d * sin(g_xz + PI + t);
 
;;       // draw arrow
;;       SetCurrent();
;;       if (act)
;;       {
;;             glBegin(GL_TRIANGLES);
;;             glVertex3f(dest.x, dest.y, dest.z);
;;             glVertex3f(p1.x, p1.y, p1.z);
;;             glVertex3f(p2.x, p2.y, p2.z);
;;             glEnd();
;;       }
;;       else
;;       {
;;             glBegin(GL_LINE_LOOP);
;;             glVertex3f(dest.x, dest.y, dest.z);
;;             glVertex3f(p1.x, p1.y, p1.z);
;;             glVertex3f(p2.x, p2.y, p2.z);
;;             glEnd();
;;       }
;; }

(defun run-sdl-event-loop (surface update-fn)
  (sdl:event-loop
   (:key-down (scan-code key mod unicode)
	      (cond ((= key (char-code #\q))
		     (return))
		    ((= key (char-code #\w))
		     (setf *bot-vy* -1))
		    ((= key (char-code #\s))
		     (setf *bot-vy* 1))
		    ((= key (char-code #\d))
		     (setf *bot-vx* 1))
		    ((= key (char-code #\a))
		     (setf *bot-vx* -1))))
   (:key-up (scan-code key mod unicode)
	    (cond ((= key (char-code #\q))
		   (return))
		  ((= key (char-code #\w))
		   (setf *bot-vy* 0))
		  ((= key (char-code #\s))
		   (setf *bot-vy* 0))
		  ((= key (char-code #\d))
		   (setf *bot-vx* 0))
		  ((= key (char-code #\a))
		   (setf *bot-vx* 0))))
   (:mouse-button-up (button x y)
		     (format t "Mouse button up: ~A (~A, ~A)~%" button x y)
		     (setf *bot-x* x
			   *bot-y* y))
   (:mouse-button-down (button x y)
		       (format t "Mouse button dn: ~A (~A, ~A)~%" button x y))
   (:quit ()
	  (return))
   (:resize (width height)
	    (format t "Resized width = ~A height = ~A~%" width height))
   (:idle ()
	  (funcall update-fn))))

(defun add-within-bounds (value delta lower-bound upper-bound)
  (max lower-bound (min upper-bound (+ value delta))))

(defun random-value ()
  (- (random 3) 1))

(defun make-update-fn (surface)
  (let ((r 128) (g 128) (b 128)
	(num-rectangles 0) (start-time (get-universal-time))
	(prev-time nil))
    (setf prev-time start-time)
    #'(lambda ()
	(loop repeat 1 do
	      (sdl:draw-filled-rectangle surface *bot-x* *bot-y* 100 100 r g b)
	      (sdl:update-rect surface *bot-x* *bot-y* 101 101)
	      (setf r (add-within-bounds r (random-value) 50 255)
		    g (add-within-bounds b (random-value) 50 255)
		    b (add-within-bounds g (random-value) 50 255)
		    *bot-x* (add-within-bounds *bot-x* *bot-vx* 0 539)
		    *bot-y* (add-within-bounds *bot-y* *bot-vy* 0 380))
	      (incf num-rectangles))
	(let ((cur-time (get-universal-time)))
	  (when (> (- cur-time prev-time) 1)
	    (setf prev-time cur-time)
	    (format t "~&Rectangles per second: ~A~%"
		    (float (/ num-rectangles (- cur-time start-time)))))))))

(defun draw-solar-system-body (size)
  (glu:with-quadric (sphere)
    (glu:quadric-draw-style sphere glu:+fill+)
    (glu:quadric-normals sphere glu:+smooth+)
    (glu:sphere sphere size 25 20)))

(defun start ()
  (setf *bot-x* 270
	*bot-y* 190
	*bot-vx* 0
	*bot-vy* 0)
  (unwind-protect
      (progn
	(let ((surface (init-sdl)))
	  (run-sdl-event-loop surface
                              (make-update-fn surface))))
    (sdl:quit)))


;; ;;;; Moving camera
;; (defparameter *view-rotx* 0)
;; (defparameter *view-roty* 0)
;; (defparameter *view-rotz* 0)

;; (cffi:defcallback key-callback :void ((key :int) (action :int))
;;   (when (eql action glfw:+press+)
;;     (cond ((eql key (char-code #\Z))
;; 	   (if (eql (glfw:get-key glfw:+key-lshift+) glfw:+press+)
;; 	       (decf *view-rotz* 5)
;; 	       (incf *view-rotz* 5)))
;;           ((eql key glfw:+key-esc+)   (glfw:close-window))
;;           ((eql key glfw:+key-up+)    (incf *view-rotx* 5))
;;           ((eql key glfw:+key-down+)  (decf *view-rotx* 5))
;;           ((eql key glfw:+key-left+)  (incf *view-roty* 5))
;;           ((eql key glfw:+key-right+) (decf *view-roty* 5)))))

;; ;;;; Resize window routine
;; (cffi:defcallback window-size-callback :void ((width :int) (height :int))
;;   (let* ((h (/ height width))
;; 	 (znear 5)
;; 	 (zfar 30)
;; 	 (xmax (* znear 0.5)))

;;     (gl:viewport 0 0 width height)
;;     (gl:with-setup-projection 
;;       (gl:frustum (- xmax) xmax (* (- xmax) h) (* xmax h) znear zfar))

;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -20)))

    ;;(gl:get-doublev gl:+projection-matrix+ *proj-presel*)
;;     (glu:pick-matrix (coerce *mx* 'double-float)
;;                      (coerce (- (sgum:deref-array viewport gl:int 3) *my*) 'double-float)
;;                      1d0 1d0 viewport)
;; Go into feedback mode and draw a rectangle around the object or change the object's color
;; Using gl:feedback mode when we draw primitives graphical
(defun handle-select (surface choice)
  (let ((feedbackbuffer (sgum:allocate-foreign-object gl:float +feed-buffer-size+))
        (size 0)
        (i 0) (j) (count)
        (hits 0))

    ;; Set the feedback buffer
    (gl:feedback-buffer +feed-buffer-size+ gl:+2d+ feedbackbuffer)
    ;; Enter feedback mode
    (gl:render-mode gl:+feedback+)
    ;; Redraw the scene
    (gl-render-scene surface)
    ;; Leave the feedback mode
    (setf size (gl:render-mode gl:+render+))

    ;; Parse the feedback buffer and get the min and max X and Y window coordinates
    (do ((i))
        ((< i +feed-buffer-size+))
      ;; Search for appropriate token
      (when (eql (sgum:deref-array feedbackbuffer gl:int i) gl:+pass-through-token+)
        (when (eql (sgum:deref-array feedbackbuffer gl:int (1+ i)) (coerce choice 'float))
          (progn
            (setf i (+ i 2))
            ;; Loop until next token is reached
            
            
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Notes: khi dung multipe-value-bind thi khong can khai bao name, input, output trong let
;; (defun create-and-display-metabolic-networks-1 ()
;;   (let* ((pw0 (make-pathway "MitoChondria Muscle Network"))
;;          (pw1 (make-pathway "Respiratory Chain"))
;;          (pw2 (make-pathway "TCA Cycle"))
;;          (pw3 (make-pathway "Beta-Oxydation"))
;;          (pw4 (make-pathway "Transporter"))
;;          (rt1 (make-reaction 'R1i "NADH + 10H" "NAD + 10H_ext"))
;;          (rt2 (make-reaction 'R6i "Pyr + CO2_ext + ATP" "OAA + Pi_M + ADP"))
;;          (rt3 (make-reaction 'R27 "Acetoacetate + NADH + H" "HD + NAD"))
;;          (rt4 (make-reaction 'T1r "Cit + H + Mal_ext" "Mal + Cit_ext + H_ext")))
;;     (add pw1 rt1)
;;     (add pw2 rt2)
;;     (add pw3 rt3)
;;     (add pw4 rt4)
;;     (add pw0 pw1 pw2 pw3 pw4)
;;     (display pw0)))

;; (defun create-and-display-metabolic-networks-2 ()
;;   (let* ((reactions '())
;;          (pw0 (make-pathway "MitoChondria Muscle Network"))
;;          (pw1 (make-pathway "Respiratory Chain"))) ; gia su chi co 1 pathway
;;     (setf reactions (load-reactions-from-file +meta-tool-file+))
;;     (dolist (r reactions)
;;       (add pw1 r))
;;     (add pw0 pw1)
;;     (display pw0)))

;; ;;; Using ltk for making GUIs
;; (defun graph-gui ()
;;   (ltk:with-ltk ()
;;     (let* ((bg (make-graphic-network nil nil nil))
;;            (sc (make-instance 'ltk:scrolled-canvas))
;;            (c (ltk:canvas sc)))
      
;;       (loop for i from 1 to (length (nodes bg)) do
;;            (push (randomize-point-3d) (nodes bg)))

;;       #| Load the adjacency matrix of the graph |#
;;       ;(load-adjacency-matrix (bg +adj-matrix-file-name+)
      
;;       (#|Take 2 points, check connection and draw |#)
;;       (loop for i from 1 to 5 do
;;            (loop for j from 1 to 5
;;                 when (#|There is a link among them|#)
;;                 do (ltk:create-line c (list x1 y1 x2 y3))))
;;       (ltk:pack sc :expand 1 :fill :both)
;;       (ltk:scrollregion c 0 0 800 800))))

;; (defun graph-line ()
;;   (ltk:with-ltk ()
;;     (let* ((sc (make-instance 'ltk:scrolled-canvas))
;;             (c (ltk:canvas sc)))
;;          (loop for i from 1 to 3 do (ltk:create-line c (list 10 10 100 (* i 50))))
;;          (ltk:pack sc :expand 1 :fill :both)
;;          (ltk:scrollregion c 0 0 800 800))))

;; (defun test-1 ()
;;   "Randomize a list of 3D points"
;;   (let ((p (make-point3d 0 0 0)))
;;     (loop for i from 1 to 5 do
;;          (setf p (randomize-point-3d))
;;          (format t "P(~d, ~d, ~d)~%" (x p) (y p) (z p)))))

;; (defun test-2 ()
;;   "Create a graph with some nodes and edges, randomize nodes' positions"
;;   (let* ((bg (make-graphic-network nil nil  nil)))
;;     (format t "Vertex: ~d, Edge: ~d~%" (length (nodes bg)) (length (edges bg)))
;;     (loop for i from 1 to (length (nodes bg)) do
;;          (push (randomize-point-3d) (nodes bg)))
      
;;     ;; Print the nodes just ramdomly generated
;;     (loop for i from 1 to (length (nodes bg)) do
;;          (let ((p (pop (nodes bg))))
;;            (format t "The ~dth point: (~d, ~d, ~d)~%" i (x p) (y p) (z p))))))

;; (defun test-draw-graphic-network ()
;;   (ltk:with-ltk ()
;;     (let* ((sc (make-instance 'ltk:scrolled-canvas))
;;            (c (ltk:canvas sc)))
;;       (test-graphic-network-1)
;;       (dolist (e (edges my-graph))
;;         (ltk:create-line c (list (x (node-position (source-node e))) (y (node-position (source-node e)))
;;                                  (x (node-position (target-node e))) (y (node-position (target-node e))))))
;;       (ltk:pack sc :expand 1 :fill :both)
;;       (ltk:scrollregion c 0 0 800 800))))

;; (defun test-graph-line ()
;;   (ltk:with-ltk ()
;;     (let* ((sc (make-instance 'ltk:scrolled-canvas))
;;            (c (ltk:canvas sc)))
      
;;       (loop for i from 10 to 100 by 30 do
;;            (ltk:create-line c (list 10 10 300 (+ 10 i)))
;;            (ltk:create-text c 300 (+ 10 i) (write-to-string i)))
      
;;       (ltk:pack sc :expand 1 :fill :both)
;;       (ltk:scrollregion c 0 0 800 800))))

;; (defvar my-graph)

;; (defun test-graphic-network-1 () 
;;   (let* ((A (make-graphic-node (randomize-point-3d) "A" nil))
;;          (B (make-graphic-node (randomize-point-3d) "B" nil))
;;          (C (make-graphic-node (randomize-point-3d) "C" nil))
;;          (AB (make-graphic-edge A B "AB"))
;;          (AC (make-graphic-edge A C "AC"))
;;          (BC (make-graphic-edge B C "BC"))
;;          (graph (make-graphic-network nil nil nil)))
    
;;     (add-nodes-to-graphic-network (list A B C) graph)
;;     (add-edges-to-graphic-network (list AB AC BC) graph)
;;     (setf my-graph graph)
;;     (dolist (node (nodes graph))
;;       (format t "~%~A(~d, ~d, ~d)" (node-label node)
;;               (x (node-position node)) (y (node-position node)) (z (node-position node))))

;;     (dolist (edge (edges graph))
;;       (format t "~%~A: ~A(~d, ~d, ~d), ~A(~d, ~d, ~d)" (edge-label edge)
;;               (node-label (source-node edge)) (x (node-position (source-node edge)))
;;               (y (node-position (source-node edge))) (z (node-position (source-node edge)))
;;               (node-label (target-node edge)) (x (node-position (target-node edge)))
;;               (y (node-position (target-node edge))) (z (node-position (target-node edge)))))
;;     ))
    
;; ;;;; View
;; (defun test-graphic-view-1 ()
;;   (let* ((gv3d (make-graphic-view-3d))
;;          (gvtext (make-graphic-view-text)))
;;     (refresh-graphic-view gv3d)
;;     (refresh-graphic-view gvtext)))

;; ;;; Test cases
;; (defun draw-one-arrow ()
;;   "Draw a specified arrow"
;;   (glfw:do-window ("Arrow")
;;     ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50))
;;      (gl:load-identity))
    
;;     (gl:translate-f 0 0 -5)
;;     (gl:push-matrix)
;;     (gl:begin gl:+lines+)
;;     (gl:color-3f 1 0 0)
;;     (gl:Vertex-3f -1 1 0)
;;     (gl:Vertex-3f 0 0 0)
;;     ;; Draw arrow
;;     (gl:Vertex-3f -0.10 0.15 0)
;;     (gl:Vertex-3f 0 0 0)

;;     (gl:Vertex-3f -0.15 0.10 0)
;;     (gl:Vertex-3f 0 0 0)
;;     (gl:end)
;;     ;; Test one-line
;;     (one-line (list 0.5 -0.5 0.5) (list 1.0 1.0 0))
;;     (gl:pop-matrix)))

;; (defun draw-arrow3d ()
;;   (let ((orig (make-point3d  0.25  0.25 0))
;;         (dest (make-point3d  0  0 0)))
;;     (glfw:do-window ("Draw Arrow Test")
;;         ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50))
;;          (gl:load-identity))
;;       (gl:translate-f 0 0 -1)
;;       (gl:color-3d 1 0 0)
;;       (gl:push-matrix)
;;       (arrow3d orig dest t)
;;       (glfw:swap-buffers))))

;; (defun draw-circle3d ()
;;   (glfw:do-window ("A Circle Example")
;;       ((gl:with-setup-projection
;;          (glu:perspective 45 4/3 0.1 50)))
;;     (gl:clear gl:+color-buffer-bit+)
;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -5)
;;     (gl:with-push-matrix
;;       (gl:color-3d 0.0 0.0 1.0)
;;       (circle3d 0.25)
;;       (glfw:swap-buffers)))) 

;; (defun draw-line3d ()
;;   (glfw:do-window ("A Lines Drawing Example" 800 600 0 0 0 0 16 0 glfw:+window+)
;;       ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50)))
;;     (gl:clear gl:+color-buffer-bit+)
;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -5)
;;     (let ((src-pnt (make-point3d -1 0 1))
;;           (tgt-pnt (make-point3d -2 1 0))
;;           (color (make-color-3d 1 0 1)))
;;       (line3d src-pnt tgt-pnt 1 color))))

;; (defun draw-one-cube ()
;;   (glfw:do-window ("A Cube Example")
;;       ((gl:with-setup-projection
;;          (glu:perspective 45 4/3 0.1 50)))
;;     (gl:clear gl:+color-buffer-bit+)
;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -5)
;; ;;     (gl:rotate-f (* 10 (glfw:get-time)) 1.0 0.0 0.0)
;; ;;     (gl:rotate-f (* 90 (glfw:get-time)) 0.0 1.0 1.0)
;;     (gl:with-begin gl:+quads+
;;       ;; top of cube
;;       (gl:Color-3f   0.0  1.0  0.0)
;;       (gl:Vertex-3f  1.0  1.0 -1.0)
;;       (gl:Vertex-3f -1.0  1.0 -1.0)
;;       (gl:Vertex-3f -1.0  1.0  1.0)
;;       (gl:Vertex-3f  1.0  1.0  1.0)
;;       ;; bottom of cube
;;       (gl:Color-3f   1.0  0.5  0.0)
;;       (gl:Vertex-3f  1.0 -1.0  1.0)
;;       (gl:Vertex-3f -1.0 -1.0  1.0)
;;       (gl:Vertex-3f -1.0 -1.0 -1.0)
;;       (gl:Vertex-3f  1.0 -1.0 -1.0)
;;       ;; front of cube
;;       (gl:Color-3f   1.0  0.0  0.0)
;;       (gl:Vertex-3f  1.0  1.0  1.0)
;;       (gl:Vertex-3f -1.0  1.0  1.0)
;;       (gl:Vertex-3f -1.0 -1.0  1.0)
;;       (gl:Vertex-3f  1.0 -1.0  1.0)
;;       ;; back of cube
;;       (gl:Color-3f   1.0  1.0  0.0)
;;       (gl:Vertex-3f -1.0  1.0 -1.0)
;;       (gl:Vertex-3f  1.0  1.0 -1.0)
;;       (gl:Vertex-3f  1.0 -1.0 -1.0)
;;       (gl:Vertex-3f -1.0 -1.0 -1.0)
;;       ;; right side of cube
;;       (gl:Color-3f   1.0  0.0  1.0)
;;       (gl:Vertex-3f  1.0  1.0 -1.0)
;;       (gl:Vertex-3f  1.0  1.0  1.0)
;;       (gl:Vertex-3f  1.0 -1.0  1.0)
;;       (gl:Vertex-3f  1.0 -1.0 -1.0)
;;       ;; left side of cube
;;       (gl:Color-3f   0.0  1.0  1.0)
;;       (gl:Vertex-3f -1.0  1.0  1.0)
;;       (gl:Vertex-3f -1.0  1.0 -1.0)
;;       (gl:Vertex-3f -1.0 -1.0 -1.0)
;;       (gl:Vertex-3f -1.0 -1.0  1.0))))

;; (defun draw-cube ()
;;   "Draw a cube using the cube method"
;;   ;; loop program   
;;     (glfw:do-window
;;         ("A Sphere Example")
;;         ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50)))
;;       (gl:clear gl:+color-buffer-bit+)
;;       (gl:matrix-mode gl:+modelview+)
;;       (gl:load-identity)
;;       (gl:translate-f 0 0 -20)
;;       (cube 0.5 (make-point3d -1 -1 0))))
         
;; (defun draw-one-sphere ()
;;   (let ((l-pos (list 0.0 -0.5 -1.0 0.0))
;;         (quad (glu:new-quadric))
;;         (angle 0.0)
;;         (blue (list 0.0 1.0 0.0))
;;         (red (list 1.0 0.0 0.0)))
    
;;     ;; loop program    
;;     (glfw:do-window
;;         ("A Sphere Example")
;;         ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50))
;;          ;; Initialise
;;          (gl:enable gl:+lighting+)
;;          (gl:enable gl:+light0+)
       
;;          (gl:light-fv gl:+light0+ gl:+position+ l-pos)
;;          (gl:depth-func gl:+less+)
;;          (gl:enable gl:+depth-test+)
;;          (glfw:set-window-size-callback (cffi:callback window-size-callback))
;;          (glfw:set-key-callback (cffi:callback key-callback)))

;;       ;; Draw (render) scene
;;       (gl:clear gl:+color-buffer-bit+)
;;       (gl:matrix-mode gl:+modelview+)
;;       (gl:load-identity)
;;       (gl:translate-f 0 0 -5)
;;       (gl:rotate-f angle 1.0 0.0 0.0)
;;      (gl:with-push-matrix
;;         (gl:material-fv gl:+front+ gl:+ambient-and-diffuse+ blue)
;;         (setf quad (glu:new-quadric))
;;         (glu:quadric-draw-style quad glu:+fill+)
;;         (glu:quadric-normals quad glu:+smooth+)
;;         (gl:color-3d 0.0 0.0 1.0)
;;         (circle3d 0.25)
;;         (print quad)
;;         (glu:sphere quad 1 20 20)
;;         (gl:translate-f 0.0 0.7 0.0)
        
;;         (gl:material-fv gl:+front+ gl:+ambient-and-diffuse+ red)
;;         (glu:quadric-normals quad glu:+smooth+)
;;         (glu:sphere quad 0.4 20 20)
;;         (gl:pop-matrix)
;;         (gl:flush))
;;       (setf angle (1+ angle))
;;       (glfw:swap-buffers))))

;; (defun draw-sphere ()
;;   (glfw:do-window
;;       ("Sphere")
;;       ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50)))
    
;;     (init-interface)
;;     (gl:clear (logior gl:+color-buffer-bit+
;;                       gl:+depth-buffer-bit+))

;;     (gl:matrix-mode gl:+modelview+)
;;     (gl:load-identity)
;;     (gl:translate-f 0 0 -20)
;;     (gl:rotate-f (* 90 (glfw:get-time)) 0 1 0)
;;     (gl:rotate-f (* 10 (glfw:get-time)) 0 1 0)
;;     (sphere (make-point3d 1 -1 1) (make-color-3d 1 0 0))
;;     (sphere (make-point3d 0  1 1) (make-color-3d 0 0 1))))

;; (defun draw-sphere-2 ()
;;   (let ((quad (glu:new-quadric)))
;;     (glfw:do-window
;;         ("A Sphere Example")
;;         ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50)))
;;       (gl:translate-f 0 0 -5)
;;       (glu:sphere quad 1 20 20))))

;; (defun test-all ()
;;   (let ((size 0.8)
;;         (orig (make-point3d  -5  -5 0))
;;         (dest (make-point3d  0  0 0))
;;         (color (make-color-3d 1 1 0)))
    
;;     ;; Initialize OpenGL and draw
;;     (glfw:do-window
;;         ("Draw shapes on the screen")
;;         ((gl:with-setup-projection (glu:perspective 45 4/3 0.1 50)))
;;       (gl:matrix-mode gl:+modelview+)
;;       (gl:load-identity)
;;       (gl:translate-f 0 0 -30)
;;       (cube size (make-point3d -5 -1 1))
;;       (cube size (make-point3d 0 -10 -10))

;;       (line3d orig dest 2 color)
      
;;       (arrow3d orig dest t)
;;       (sphere orig color)
;;       (sphere dest color))))

       
;;   ;; Draw the first sphere
;;   (gl:color-3f 1f0 0f0 0f0)
;;   (gl:load-name 10)
    
;;   (draw-sphere 0.5d0)
;;   ;; Draw the second sphere
;;   (gl:translate-f 3f0 0f0 0f0)
;;   (gl:color-3f 0f0 0f0 1f0)
;;   (gl:load-name 20)
;;   (gl:pass-through 2.0f0)
;;   (draw-sphere 0.5d0)

                ;; If a single hit occurred, display the information
;;     (if (not (zerop hits))
;;         (let ((n 0)
;;               (minz (sgum:deref-array buffer gl:int 1)))
;;           (loop for i from 1 to hits do
;;                (if (< (sgum:deref-array buffer gl:int (1+ (* i 4))) minz)
;;                    (progn
;;                      (setf n i)
;;                      (setf minz (sgum:deref-array buffer gl:int (1+ (* i 4)))))))
;;           (handle-select (sgum:deref-array buffer gl:int (+ 3 (* n 4))))))

            
;; (defclass cabine ()
;;   ((metabolic-network
;;     :accessor metabolic-network
;;     :initarg :metabolic-network)
;;    (graph-metabolic
;;     :accessor graph-metabolic
;;     :initarg :graph-metabolic)
;;    (opengl-view
;;     :accessor opengl-view
;;     :initarg :opengl-view)))

;;(defgeneric make-cabine (metabolic-network graph-metabolic opengl-view))

;; (defgeneric load-meta-tool-file ()
;;   (:documentation "Load meta tool file."))

;; (defgeneric save-status ())

;; (defgeneric initialise-positions ()
;;   (:documentation "Initialise positions."))

;; (defgeneric apply-layout-paradigm ()
;;   (:documentation "Apply a layout algorithm."))

;; (defgeneric draw-metabolic-network (surface))

;; (defgeneric visualise-metabolic-network ()
;;   (:documentation "Visualise the metabolic network."))

;; (defgeneric run-application ()
;;   (:documentation "Run application."))

;;;; Implement the methods
;; (defmethod make-cabine (metabolic-network graph-metabolic opengl-view)
;;   (make-instance 'cabine :metabolic-network metabolic-network
;;                  :graph-metabolic graph-metabolic
;;                  :opengl-view opengl-view))

(defun draw-free (surface)
  (declare (ignorable surface))
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))
  
  (gl:matrix-mode gl:+modelview+)
  (gl:load-identity)
  (gl:translate-f 0.0f0 0.0f0 -5.0f0)

  (glu:look-at 0d0 0d0 -5d0
               0d0 0d0 0d0
               0d0 1d0 0d0)
  
  (gl:push-matrix)
  (gl:rotate-f *xspeed* 1f0 0f0 0f0)
  (gl:rotate-f *yspeed* 0f0 1f0 0f0)
  (gl:rotate-f *zspeed* 0f0 0f0 1f0)

  (draw-object (second (edges (graph-metabolic *m-cabine*))))
  (object-3d:cube 0.5f0 (object-3d:make-point3d 0.0f0 0.0f0 0.0f0))
  (object-3d:sphere 0.5f0 (object-3d:make-point3d 0.0f0 -1.0f0 0.0f0)
                    (object-3d:make-color-3d 0.0f0 0.0f0 1.0f0))
  (gl:pop-matrix)
  (sdl:gl-swap-buffers)
  t)

(defvar angle 0.0f0)

(defun draw-gl-scene (surface)
  (declare (ignorable surface))
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))
  
  (gl:matrix-mode gl:+modelview+)
  (glu:look-at 0d0 0d0 -5d0
               0d0 0d0 0d0
               0d0 1d0 0d0)
  (gl:load-identity)
  (gl:translate-f 0.0f0 0.0f0 -5.0f0)

  (gl:push-matrix)
  ;; Draw a line
  (gl:rotate-f angle 0.0f0 1.0f0 0.0f0)
  (object-3d:line3d (object-3d:make-point3d 0.0f0 0.0f0 0.0f0)
                    (object-3d:make-point3d 1.0f0 1.0f0 1.0f0)
                    2.0f0
                    (object-3d:make-color-3d 0.5f0 0.0f0 0.0f0))

  ;; Draw a cube
  (gl:rotate-f angle 0.0f0 1.0f0 -1.0f0)
  (object-3d:cube 0.2 (object-3d:make-point3d -1.0f0 -1.0f0 -1.0f0))
  
  ;; Draw a sphere
  (gl:rotate-f angle 0.0f0 1.0f0 1.0f0)
  (object-3d:sphere 0.3f0
                    (object-3d:make-point3d 1.0f0 0.0f0 0.0f0)
                    (object-3d:make-color-3d 1.0f0 0.5f0 0.0f0)) 
  (gl:pop-matrix)
  (sdl:gl-swap-buffers)
  (incf angle 0.1f0)
  t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defclass Graphical-View ()
  ())

(defclass Graphical-View-3D (Graphical-View)
  ())

(defclass Graphical-View-Text (Graphical-View)
  ())

;;;; View
(defgeneric refresh-graphic-view (view)
  (:documentation "Update the information gets from the Graphic Network"))

(defgeneric create-gui (view)
  (:documentation "Create the GUI for each view"))

(defgeneric make-graphic-view-3d ())

(defgeneric make-graphic-view-text ())

;;;; Define constructors
(defmethod make-graphic-view-3d ()
  (format t "~%Creating an instance of GraphicView3D")
  (make-instance 'Graphical-View-3D))

(defmethod make-graphic-view-text ()
  (format t "~%Creating an instance of GraphicViewText")
  (make-instance 'Graphical-View-Text))

;;;; Define the refresh-graphic-view method
(defmethod refresh-graphic-view ((view Graphical-View))
  (format t "~%Calling the refresh-graphic-view method of GraphicView"))

(defmethod refresh-graphic-view ((view Graphical-View-3D))
  (format t "~%Calling the refresh-graphic-view method of GraphicView3D"))

(defmethod refresh-graphic-view ((view Graphical-View-Text))
  (format t "~%Calling the refresh-graphic-view method of GraphicViewText"))

;;;;;;;;;;====================
(defun event-loop0 (surface update-fn resize-fn handle-keypress-fn bpp video-flags)
  (declare (ignorable surface))
  (sdl:event-loop
   (:key-down (key)
	      (if (= key (char-code #\q))
		(return)
                (funcall handle-keypress-fn key)))
   (:mouse-button-up (button x y)
		     (format t "Mouse button up: ~A (~A, ~A)~%" button x y))
   (:mouse-button-down (button x y)
		       (format t "Mouse button dn: ~A (~A, ~A)~%" button x y))
   (:quit ()
	  (return))
   (:resize (width height)
	    (format t "Resized width = ~A height = ~A~%" width height)
            (sdl:free-surface surface)
            (setf surface
                  (sdl:set-video-mode width height bpp video-flags))
            (funcall resize-fn surface width height))
   (:idle ()
          (funcall update-fn surface))))

;; GLdouble model_view[16];
;; glGetDoublev(GL_MODELVIEW_MATRIX, model_view);
;; GLdouble projection[16];
;; glGetDoublev(GL_PROJECTION_MATRIX, projection);

;; GLint viewport[4]; 
;; glGetIntegerv(GL_VIEWPORT, viewport);
    
;; double dx; double dy; double dz;
;; GLfloat depth[2];	 
;; glReadPixels (x, y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, depth); 
;; gluUnProject(x, y, depth[0], model_view, projection, viewport, &dx, &dy, &dz);
;; x3d = float(dx);
;; y3d = float(dy);
;; z3d = float(dz);
Khoi tao *scene*
;;   ;; Make the nodes
;;   (loop for i from 0 to (1- (length (nodes *m-graph*))) do
;;         (if (eql 'graph-molecule-node (type-of (nth i (nodes *m-graph*))))
;;             (push (make-sphere :center (node-position (nth i (nodes *m-graph*)))) *scene*)
;;             (push (make-cube :center (node-position (nth i (nodes *m-graph*)))) *scene*)))

;;   ;; Make the lines that connect to the nodes
;;   (loop for i from 0 to (1- (length (edges *m-graph*))) do
;;        (push (make-line3d :color (make-color3d 1.0f0 1.0f0 1.0f0)
;;                           :source (node-position (source-node (nth i (edges *m-graph*))))
;;                           :target (node-position (target-node (nth i (edges *m-graph*))))
;;                           :width 3.0f0) *scene*)))

;;   (dotimes (i (length *scene*))
;;        (gl:load-name i)
;;        (gl:pass-through (c2float i))
;;       (draw (nth i *scene*)))


;;   (with-ltk ()
;;     (let* ((sc (make-instance 'scrolled-canvas :borderwidth 2 :relief :raised))
;;            (c (canvas sc))
;;            (f (make-instance 'frame))
;;            (fr (make-instance 'frame :master f))
;;            (lr (make-instance 'label :master fr :text "Rotation:"))
;;            (b1 (make-instance 'button
;;                               :master fr
;;                               :text "X"
;;                               :command (lambda ()
;;                                          (rotate "X" 0.5))))
;;            (b2 (make-instance 'button
;;                               :master fr
;;                               :text "Y"
;;                               :command (lambda ()
;;                                          (rotate "Y" 0.5))))
           
;;            (b3 (make-instance 'button
;;                               :master fr
;;                               :text "Z"
;;                               :command (lambda ()
;;                                          (create-text c 10 20 "Rotate Z")
;;                                          (rotate "Z" 0.5))))
;;            (b4 (make-instance 'button
;;                               :master f
;;                               :text "Exit"
;;                               :command (lambda ()
;;                                          (format t "~%See you later. Bye bye!")
;;                                          (setf *exit-mainloop* t))))
;;            (fz (make-instance 'frame :master f))
;;            (lz (make-instance 'label :master fz :text "Zoom:"))
;;            (z-in (make-instance 'button
;;                                 :master fz
;;                                 :text "In"
;;                                 :command (lambda () (zoom 1))))
;;            (z-out (make-instance 'button
;;                                  :master fz
;;                                  :text "Out"
;;                                  :command (lambda () (zoom 2))))
;;            (z-normal (make-instance 'button
;;                                  :master fz
;;                                  :text "Normal"
;;                                  :command (lambda () (zoom 3))))
;;            ;; Create menu bar
;;            (mb (make-menubar))
;;            (mfile (make-menu mb "File" ))
;;            (mf-load (make-menubutton mfile "Load" (lambda ()
;;                                                     (load-meta-tool-file))
;;                                      :underline 0))

;;            (mf-init (make-menubutton mfile "Init" (lambda ()
;;                                                     (initialise-positions))
;;                                      :underline 0))
;;            (mf-run (make-menubutton mfile "Run" (lambda ()
;;                                                     (visualise-metabolic-network))
;;                                      :underline 0))
;;            (mf-save (make-menubutton mfile "Save" (lambda ()
;;                                                     (save-status))
;;                                      :underline 0))
;;            (sep1 (add-separator mfile))
;;            (mf-export (make-menu mfile "Export..."))
;;            (mfe-jpg (make-menubutton mf-export "svg" (lambda ()
;;                                                        (export-graphic 1))))
;;            (mfe-gif (make-menubutton mf-export "png" (lambda ()
;;                                                        (export-graphic 2))))
;;            (sep2 (add-separator mfile))
;;            (mf-print (make-menubutton mfile "Print" (lambda () (print2pdf c "mbe.pdf"))))
;;            (sep3 (add-separator mfile))
;;            (mf-exit (make-menubutton mfile "Exit" (lambda () (setf *exit-mainloop* t))
;;                                      :underline 1
;;                                      :accelerator "Alt Q"))
;;            ;; Create a popup menu
;;            (mp (make-menu nil "Popup"))
;;            (mp-1 (make-menubutton mp "Option 1" (lambda () (format t "Popup 1~&") (finish-output))))
;;            (mp-2 (make-menubutton mp "Option 2" (lambda () (format t "Popup 2~&") (finish-output))))
;;            (mp-3 (make-menubutton mp "Option 3" (lambda () (format t "Popup 3~&") (finish-output)))))
     
;;       (declare (ignore mf-print mf-exit mfe-gif mfe-jpg mf-save mf-load mf-init mf-run sep1 sep2 sep3 mp-1 mp-2 mp-3))
;;       ;;(bind *tk* "<Alt-q>" (lambda (event) (declare (ignore event)) (setf *exit-mainloop* t)))
;;       ;; Bind the popup menu to canvas
;;       (bind c "<1>" (lambda (event) (popup mp (event-root-x event) (event-root-y event))))
;;       (configure c :borderwidth 1 :relief :sunken)
;;       (pack sc :fill :both :expand t)
;;       (pack f)
;;       (scrollregion c 0 0 300 200)
;;       (pack (list fr lr b1 b2 b3) :side :left)
;;       (pack b4 :side :left)
;;       (pack (list fz lz z-in z-out z-normal) :side :left)
;;       (configure f :width 300 :height 200)
;;       (create-text c 10 10 "Metabolic Network Editor 0.1.0 Beta")
;;       (configure f :borderwidth 1)
;;       (configure f :relief :flat))));Other styles would be raised, ridge, groove, flat and solid
;; Below are the functions that are attached on the GUI with LTK
(defun rotate (axis ratio)
  (incf *xspeed* 0.5f0)
  (format t "~%~a" *xspeed*)
  (format t "~%Rotate on the ~A axis by ~A rate." axis ratio))

(defun zoom (val)
  (format t "~%~a."
          (case val
            (1 "Zoom in")
            (2 "Zoom out")
            (3 "Normal"))))

(defun save-status ()
  (format t "~%Saved status"))

(defun export-graphic (val)
  (format t "~%Export to ~a: "
          (case val
            (1 "svg")
            (2 "png")))
  (finish-output))

(defun print2pdf (canvas filename)
  (format t "~%Printed ~a to file: ~a" canvas filename))

   ;; Cac ham lien quan den UI cua LTK
   #:save-status #:export-graphic #:print2pdf #:zoom #:rotate #:load-ui

;;;;;
(defun draw-arrow-3d ()
  (let ((pA (make-point3d 1.0f0 0.0f0 0.0f0))
        (pB (make-point3d 0.0f0 0.0f0 2.0f0))
        (pC (make-point3d 0.0f0 0.0f0 0.0f0))
        (pD (make-point3d 0.0f0 0.0f0 0.0f0))
        (adj))
    
    (when (not (zerop (- (z pA) (z pB))))
      (setf (x pC) 1
            (y pC) 1
            (z pC) (/ (- (+ (- (x pA) (x pB))
                            (- (y pA) (y pB))))
                      (- (z pA) (z pB)))
            
            adj (/ (sqrt (+ (square (- (x pA) (x pB)))
                            (square (- (y pA) (y pB)))
                            (square (- (z pA) (z pB)))))
                   (* 4 (sqrt (+ (square (x pC))
                                 (square (y pC))
                                 (square (z pC)))))))

      (setf (x pC) (* (x pC) adj)
            (y pC) (* (y pC) adj)
            (z pC) (* (z pC) adj))

      ;; Getting the other side
      (setf (x pD) (- (x pC))
            (y pD) (- (y pC))
            (z pD) (- (z pC)))

      ;; Getting the points
      (setf (x pC) (+ (x pA) (x pC) (* (/ 3 4) (- (x pA) (x pB))))
            (y pC) (+ (y pA) (y pC) (* (/ 3 4) (- (y pA) (y pB))))
            (z pC) (+ (z pA) (z pC) (* (/ 3 4) (- (z pA) (z pB))))

            (x pD) (+ (x pA) (x pD) (* (/ 3 4) (- (x pA) (x pB))))
            (y pD) (+ (y pA) (y pD) (* (/ 3 4) (- (y pA) (y pB))))
            (z pD) (+ (z pA) (z pD) (* (/ 3 4) (- (z pA) (z pB)))))

      (gl:begin gl:+triangles+)
      (gl:color-3f 1.0f0 0.0f0 0.0f0)
      (gl:vertex-3f (x pB) (y pB) (z pB))
      (gl:color-3f 0.0f0 1.0f0 0.0f0)
      (gl:vertex-3f (x pC) (y pC) (z pC))
      (gl:color-3f 0.0f0 0.0f0 1.0f0)
      (gl:vertex-3f (x pD) (y pD) (z pD))
      (gl:end)
      ;;(display-point-3d pA)      (display-point-3d pB)      (display-point-3d pC)      (display-point-3d pD)
      (let ((line (make-line3d :source pA :target pB :color (make-color3d 0.0f0 1.0f0 1.0f0))))
        (draw line))
      (gl:flush)
      
      )))
(defmethod draw-arrow3d ((orig Point3D) (dest Point3D) act)
  (let ((x) (y) (z) (tt) (d) (g-xy) (g-xz) (p1) (p2))
    ;; get vector
    (setf x (- (x dest) (x orig)))
    (setf y (- (y dest) (y orig)))
    (setf z (- (z dest) (z orig)))

    ;; angle of head arm (180 degrees == PI radians)
    (setf tt (/ (* 10.0 +PI+) 180))

    ;; length of arrow arm
    (setf d 0.1)

    ;; vector angle from x axis
    (setf g-xy 0)
    (setf g-xz 0)
    ;; fix angles if x <= 0
    (cond ((eq x 0)
           (progn
             (when (> y 0) (setf g-xy (/ +PI+ 2)))
             (when (< y 0) (setf g-xy (/ (- +PI+) 2)))
             (when (> z 0) (setf g-xz (/ +PI+ 2)))
             (when (< z 0) (setf g-xz (/ (- +PI+) 2)))))
          ;; y/-x -> negative angle, -y/-x -> positive angle
          ((< x 0)
           (progn
             (setf g-xy (+ +PI+ (atan (/ y x))))
             (setf g-xz (+ +PI+ (atan (/ z x))))))
          (t
           (progn
             (setf g-xy (atan (/ y x)))
             (setf g-xz (atan (/ z x))))))     
    ;; point 1
    (setf p1 (make-point3d 0 0 0))
    (setf (slot-value p1 'x) (+ (x dest) (* d (cos (+ g-xy +PI+ (- tt))))))
    (setf (slot-value p1 'y) (+ (y dest) (* d (sin (+ g-xy +PI+ (- tt))))))
    (setf (slot-value p1 'z) (+ (z dest) (* d (sin (+ g-xz +PI+ (- tt))))))

    ;; point 2
    (setf p2 (make-point3d 0 0 0))
    (setf (slot-value p2 'x) (+ (x dest) (* d (cos (+ g-xy +PI+ tt)))))
    (setf (slot-value p2 'y) (+ (y dest) (* d (sin (+ g-xy +PI+ tt)))))
    (setf (slot-value p2 'z) (+ (z dest) (* d (sin (+ g-xz +PI+ tt)))))

    ;; draw arrow
    ;; set-current ()
    (if (eq act nil)
        (progn
          (gl:begin gl:+triangles+)
          (gl:vertex-3f (x dest) (y dest) (z dest))
          (gl:vertex-3f (x p1) (y p2) (z p1))
          (gl:vertex-3f (x p2) (y p2) (z p2))
          (gl:end))
        (progn
          (gl:begin gl:+line-loop+)
          (gl:vertex-3f (x dest) (y dest) (z dest))
          (gl:vertex-3f (x p1) (y p2) (z p1))
          (gl:vertex-3f (x p2) (y p2) (z p2))
          (gl:end)))))

;;; Reference from http://labmaster.mi.infn.it/Laboratorio2/root/htmldoc/src/TGLVector3.cxx.html
(defmethod draw-line ((start Point3D) (vector Point3D) head size (color Color3D))
  ;; Draw thick line (tube) running from 'start', length 'vector',
  ;; with head at end of shape 'head' - box/arrow/none,
  ;; (head) size 'size', color 'color'
  (let ((quad (glu:new-quadric))
        (head-height 0)
        (matrix)
        (m (sgum:allocate-foreign-object gl:double 16)))
    ;; Draw 3D line (tube) with optional head shape
    (set-draw-color color)
    (setf matrix (make-matrix-with start vector))
    (gl:push-matrix)
    (dotimes (i (length (fvals matrix)))
      (setf (sgum:deref-array m gl:double i)
            (d2float (nth i (fvals matrix)))))    
    (gl:mult-matrix-d m)
    (cond ((eql head +line-head-none+) (setf head-height 0.0f0))
          ((eql head +line-head-arrow+) (setf head-height (* size 2.0f0)))
          ((eql head +line-head-box+) (setf head-height (* size 1.4f0))))
    (setf size (d2float size)
          head-height (d2float head-height))
    ;; Line (tube) component
    ;;(gl:translate-f (x start) (y start) (z start))
    ;;(gl:rotate-f 90f0 ;;(c2float (* (angle start vector) +RAD2DEG+))
    ;;             0.0f0 1.0f0 0.0f0)
    (glu:cylinder quad (d2float (/ size 4)) (d2float (/ size 4))
                  (d2float (- (distance start vector) head-height)) 32 32)
    (glu:quadric-orientation quad glu:+outside+)
    (glu:partial-disk quad 0.0d0 (d2float (/ size 4)) 32 1 0.0d0 360.0d0)
    ;; Shift down local Z to end of line
    (gl:translate-f 0.0f0 0.0f0 (c2float (- (distance start vector) head-height)))
    
    (cond ((eql head +line-head-none+)
           (progn ;; Cap end of line
             (glu:quadric-orientation quad glu:+outside+)
             (glu:partial-disk quad 0.0d0 (d2float (/ size 4)) 32 1 0.0d0 360.0d0)))
          ((eql head +line-head-arrow+)
           (progn ;; Arrow base/end line cap
             (glu:partial-disk quad 0.0d0 size 32 1 0.0d0 360.0d0)
             (glu:quadric-orientation quad glu:+outside+)
             (glu:cylinder quad size 0.0d0 head-height 32 32)))
          (t
           (progn ;; Box: Drawing
             (glu:quadric-orientation quad glu:+outside+))))
    (gl:pop-matrix)))

