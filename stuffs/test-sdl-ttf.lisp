(require 'sdl)
(require 'sdl-ttf)
(require 'sdl-img)

;Screen attributes
(defconstant +screen-width+ 640)
(defconstant +screen-height+ 480)
(defconstant +screen-bpp+ 32)
;Surfaces
(defparameter *background* nil)
(defparameter *message* nil)
(defparameter *screen* nil)

;Event structure
(defparameter event nil)

;Font used
(defparameter font nil)

;Font color
(defparameter text-color '(255 255 255))

(defun load-image (filename)
  (let ((loaded-image nil)
	(optimized-image nil))

    (setf loaded-image (sdl-img:load filename))

    (unless (null loaded-image)
      (setf optimized-image (sdl:display-format loaded-image))
      (sdl:free-surface loaded-image))
    optimized-image))

(defun apply-surface (x y source destination)
  (sgum:with-foreign-objects ((offset sdl:rect))
    (setf
     (sdl:rect-x offset) x
     (sdl:rect-y offset) y)
    (sdl:blit-surface source nil destination offset)))

(defun init ()
  ;Initialisation of all SDL sub-systems
  (when (eq (sdl:init sdl:+init-everything+) -1)
      (return-from init nil))

  ;Set the screen
  (setf *screen* (sdl:set-video-mode +screen-width+ +screen-height+ 
	+screen-bpp+ sdl:+swsurface+))

  ;If an error occurs when setting the screen
  (when (null *screen*)
    (return-from init nil))
  
  ;SDL_TTF initialisation
  (when (eq (sdl-ttf:init) -1)
    (return-from init nil))

  ;Setting caption 
  (sdl:wm-set-caption "TTF Test" nil)

  t)

(defun load-files ()
  ;Loading background image
  (setf *background* (load-image "background.png"))

  ;Opening font
  (setf font (sdl-ttf:open-font "CaslonBold.ttf" 28))

  (when (null *background*)
    (return-from load-files nil))
  (when (null font)
    (return-from load-files nil))

  t)


(defun clean-up ()
  (sdl:free-surface *background*)  
  (sdl:free-surface *message*)

  (sdl-ttf:close-font font)
  (sdl-ttf:quit)

  (sdl:quit))

(defun main ()
  (when (null (init))
    (return-from main 1))
  (when (null (load-files))
    (return-from main 1))
    
  (setf *message* (apply #'cl-sdl-ttf:render-text-solid font 
                         "NGUYEN VU NGOC TUNG" 
                         text-color))
  (when (null *message*)
    (return-from main 1))
    
  (apply-surface 0 0 *background* *screen*)
  (apply-surface 100 100 *message* *screen*)

  (setf *message* (apply #'cl-sdl-ttf:render-text-solid font 
                         "Test SDL-TTF" 
                         text-color))

  (apply-surface 320 240 *message* *screen*)
  (when (eq (sdl:flip *screen*) -1)
    (return-from main 1))

  (unwind-protect 
       (sdl:event-loop 
	(:quit () (return))))

  (clean-up)

  t)
