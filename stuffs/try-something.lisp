(defpackage :try-st
  (:use :common-lisp)
  (:export #:test-1 #:display #:tuple #:name #:value #:*list-tuples*))

(in-package :try-st)

(defvar *list-tuples* '())

(defclass tuple ()
  ((name  :accessor name  :initarg :name  :initform "noname")
   (value :accessor value :initarg :value :initform 0)))

(defmethod make-tuple (name value)
  (make-instance 'tuple :name name :value value))

(defgeneric exist (tp the-list))
; http://groups.google.com/group/comp.lang.lisp/browse_thread/thread/4498511cf8f992a5/133a95bdaf225306?#133a95bdaf225306
; Xem them de chinh sua ham exist tot hon
(defmethod exist ((tp tuple) the-list)
  (count-if #'(lambda (x) (string= (name x) (name tp))) the-list))                

(defun add-tuple (tp the-list)
  (if (zerop (exist tp the-list))
      (cons tp the-list) the-list))

(defun add-tuples (the-list &rest tps)
  (setf the-list (add-tuple tps the-list)))

(defun test-1 ()
  (let ((t1 (make-tuple "A" 1))
        (t2 (make-tuple "B" 2))
        (t3 (make-tuple "C" 3)))
    (setf *list-tuples* (add-tuple t1 *list-tuples*))
    (setf *list-tuples* (add-tuple t2 *list-tuples*))
    (setf *list-tuples* (add-tuple t3 *list-tuples*))
    (setf *list-tuples* (add-tuple (make-tuple "D" 4) *list-tuples*))))
     
(defun display ()
  (dolist (tp *list-tuples*) 
    (format t "~%~A  ~d" (name tp) (value tp))))

(defclass AdjList ()
  ((vertex :accessor vertex :initarg :vertex :initform 0)
   (t-list :accessor t-list :initarg :t-list :initform nil :documentation "la mot danh sach lien ket, tro den Node")))

;;;; Mot so phuong thuc cua lop AdjList
(defmethod make-adj-list ((vertex number))
  (make-instance 'AdjList :vertex vertex :t-list '()))

(defgeneric begin (adj-list))

(defmethod begin ((adj-list AdjList))
  (if (equal nil (first adj-list))
      nil
      (value (first adj-list))))

(defgeneric next (adj-list))

;;; ================
(defclass Node ()
  ((value :accessor value :initarg :value :initform 0)
   (next  :accessor next  :initarg :next  :initform nil)))

(defclass Edge ()
  ((source :accessor source :initarg :source :initform 0)
   (target :accessor target :initarg :target :initform 0)))
    
(defclass Graph ()
  ((vertex-count
    :accessor vertex-count
    :initarg :vertex-count
    :initform 0
    :documentation "Number of vertices")
   (digraph
    :accessor digraph
    :initarg :digraph
    :initform t
    :documentation "digraph = t or nil: directed or undirected")
   (adj-matix ; danh sach cac dinh ke, la mot danh sach lien ket, list of list (nested list)
    :accessor adj-matrix
    :initarg :adj-matrix
    :initform '()
    :documentation "The adjacency matrix")))

;;;; Mot so phuong thuc cua lop Node
;;; Ham khoi tao cua lop Node
(defmethod make-node ((value number) (next Node))
  (make-instance 'Node :value value :next next))

;;;; Mot so phuong thuc cua lop Edge
;;; Ham khoi tao cua lop Edge
(defmethod make-edge ((source number) (target number))
  (make-instance 'Edge :source source :target target))

;;;; Mot so phuong thuc cua lop Graph
(defmethod make-graph ((vertex-count integer) digraph)
  "Make a graph (constructor)"
  (make-instance 'Graph :vertex-count vertex-count
                 :digraph digraph :adj-matrix (make-list vertex-count)))

(defmethod get-adjacency-vertices ((vertex integer) (G Graph))
  "Danh sach cac dinh ke voi dinh co gia tri la vertex."
  (nth vertex (adj-matrix G)))

(defmethod insert-edge ((e Edge) (G Graph))
  "Insert the edge e into the G's adjacency matrix"
  (let ((adj (get-adjacency-vertices (source e) G)))
    (setf (nth (source e) (adj-matrix G)) (add-to-sorted-list (target e) adj))))

(defun add-to-sorted-list-2 (element the-list)
  "Add the element into the-list which is a sorted list. If the element exists, the function returns the original list"
  (cond ((equal '() the-list)
         (append the-list (list element)))
        (t (let* ((i 0)
                  (the-temp-list '()))
    
             (loop if (< (nth i the-list) element)
                ;do (setf the-temp-list (append the-temp-list (list (nth i the-list)))) (setf i (1+ i))
                  do (print (nth i the-list)) (setf i (1+ i))
                else do (return))

             (if (not (equal (nth i the-list) element))
                 (setf the-temp-list (append the-temp-list (list element))))
    
             #|(if (< i (list-length the-list))
                 (loop if (< i (- (list-length the-list) 1))
                    do (progn
                         (setf the-temp-list (append the-temp-list (list (nth i the-list))))
                         (setf i (1+ i)))
                    else do (return)))|#
             the-temp-list))))

(defun add-to-sorted-list (elm lst &optional res)
  (cond ((endp lst)
         (reverse (cons elm res)))
        ((< elm (car lst))
         (append (reverse (cons elm res)) lst))
        ((= elm (car lst))
         (append (reverse res) lst))
        (t (add-to-sorted-list elm (cdr lst) (cons (car lst) res)))))

(defun binary-search (element the-list)
  (let* ((low 0)
         (high (1- (list-length the-list)))
         (mid)
         (median))
 
    (loop while (<= low high) do
         (setf mid (values (floor (+ low high) 2)))
         (setf median (nth mid the-list))
         (cond ((< element median) (setf high (1- mid)))
               ((> element median) (setf low (1+ mid)))
               ((= element median) (return mid))))))

(defmethod reverse-graph ((G Graph))
  "Return a graph that contructs from G which edges have reversed directions."
  (let ((R (make-graph (vertex-count G) t))
        (A '()))
    (dotimes (v (vertex-count G))
      (setf A (get-adjacency-vertices v G))
      (dolist (w A)
        (insert-edge (make-edge w v) R)))
    R))

(defclass GraphSCC ()
  ((cnt
    :accessor cnt
    :initarg :cnt
    :reader cnt)
   (scnt
    :accessor scnt
    :initarg :scnt
    :reader scnt)
   (id
    :accessor id
    :initarg :id
    :initform '()
    :reader id)
   (postI
    :accessor postI
    :initarg :postI
    :initform '()
    :reader postI)
   (postR
    :accessor postR
    :initarg :postR
    :initform '()
    :reader postR)))

(defvar cnt 0)
(defvar scnt 0)
(defvar id '())
(defvar postI '())
(defvar postR '())

(defmethod dfsR ((G Graph) (vertex integer))
  (setf (nth vertex id) scnt)
  (dolist (v (nth vertex (adj-matrix G)))
      (if (= (nth v id) -1)
          (dfsR G v)))
  (setf (nth (1+ cnt) postI) vertex))
  
(defmethod dfs-graph ((G Graph))
  "Depth First Search on the Graph using Kosaraju's algorithm"
  (let ((R (reverse-graph G)))
    ;;; Initialize the global variables
    (setf id (make-list (vertex-count G) :initial-element -1))
    (setf postI (make-list (vertex-count G)))
    (setf cnt 0)
    (setf scnt 0)

    (dotimes (v (vertex-count R))
      (if (= (nth v id) -1)
          (dfsR R v)))

    (setf postR (make-list (vertex-count G)))

    (dotimes (v (vertex-count R))
      (setf (nth v postR) (nth v postI)))

    (setf cnt 0)
    (setf scnt 0)
    (mapcar #'(lambda (x) (setf x -1)) id)
    
    (loop for v downfrom (- (vertex-count G) 1) to 0 do
         (if (= (nth (nth v postR) id) -1)
             (progn
               (dfsR G (nth v postR))
               (1+ scnt))))))

(defmethod count-strongly-components ((G Graph))
  scnt)

(defmethod strongly-reachable ((v integer) (w integer))
  (= (nth v id) (nth w id)))

(defmethod random-edge ((G Graph) (edge-count integer))
  (loop for i from 0 to (1- edge-count) do
       (insert-edge (make-edge (random (vertex-count G)) (random (vertex-count G))) G)))

(defmethod random-graph ((G Graph) (edge-count integer))
  (loop for i from 0 to (1- (vertex-count G)) do
       (loop for j from 0 to (1- (vertex-count G))
            when (and (evenp (random 10)) (not (equal i j)))
            do (insert-edge (make-edge i j) G))))

(defun quicksort (lst)
  (when lst
    (let ((pivot (car lst)))
      (append
       (quicksort (remove-if-not (lambda (x) (<= x pivot)) (cdr lst)))
       (list pivot)
       (quicksort (remove-if-not (lambda (x) (>  x pivot)) (cdr lst)))))))

(defmethod make-graph-for-test ()
  (let ((G (make-graph 8 t)))
    (insert-edge (make-edge 0 1) G)
    (insert-edge (make-edge 1 4) G)
    (insert-edge (make-edge 4 0) G)
    (insert-edge (make-edge 1 5) G)
    (insert-edge (make-edge 1 2) G)
    (insert-edge (make-edge 2 6) G)
    (insert-edge (make-edge 5 6) G)
    (insert-edge (make-edge 6 5) G)
    (insert-edge (make-edge 2 3) G)
    (insert-edge (make-edge 3 2) G)
    (insert-edge (make-edge 4 5) G)
    (insert-edge (make-edge 3 7) G)
    (insert-edge (make-edge 6 7) G)
    G))
    
(defun test (&optional v)
  (let ((G (make-graph-for-test))) ;make-graph 5 t)))
    ;(random-graph G v)
    (print G)
    (print (adj-matrix G))
    (dfs-graph G)
    (count-strongly-components G)))

(defclass speaker nil nil)

(defmethod speak ((s speaker) string)
  (format t "~A" string))

(speak (make-instance 'speaker) "life is not what it used to be")
