;; (require 'opengl)
;; (require 'sdl)
;; (require 'sdl-ttf)

(defparameter font-path "DATA/CaslonBold.ttf")
(defparameter font nil)
(defconstant +screen-width+ 640)
(defconstant +screen-height+ 480)

(sgum:def-foreign-type uint* (* gl:uint))
(defvar *texture* (sgum:allocate-foreign-object gl:uint 1))
(declaim (type uint* *texture*))
(declaim (inline get-texture))
(defun get-texture (i)
  (sgum:deref-array *texture* '(:array gl:uint) i))

(defun my-round (x)
  (multiple-value-bind (y z)
      (truncate (+ x 0.5))
    (declare (ignorable z))
    y))
  
(defun next-power-of-two (x)
  (let ((log-base2 (/ (log x) (log 2))))
    (my-round (expt 2 (ceiling log-base2)))))

(defun init-sdl (surface)
  (when (eql (sdl:init sdl:+init-video+) 1)
    (gl:get-error))
  
  (setf surface (sdl:set-video-mode +screen-width+ +screen-height+ 0 sdl:+opengl+))
  (sdl:wm-set-caption "C-Junkie's SDLGL text example" nil)

  (when (eql (sdl-ttf:init) 1)
    (return-from init-sdl nil))
  surface)
  
(defun cfloat (num)
  (coerce num 'float))

(defun hexa2dec (s)
  (print s))

(defun sdl-gl-render-text (text font lcolor location)
  (let ((w 0) 
	(h 0))
    
    (unwind-protect
	 (progn
	   ;; Use sdl-ttf to render our text
	   (sgum:with-foreign-objects ((color sdl:color) (initial sdl:surface)
				       (intermediary sdl:surface))
	     (setf (sdl:color-r color) (first lcolor)
		   (sdl:color-g color) (second lcolor)
		   (sdl:color-b color) (third lcolor))
	     (with-alien ((color sdl:color))
	       (setf initial (sdl-ttf:render-text-blended font text color)))
             
	     ;; Convert the rendered text to a known format
	     (setf w (next-power-of-two (sdl:surface-w initial))
		   h (next-power-of-two (sdl:surface-h initial)))

             (setf intermediary (sdl:create-rgb-surface sdl:+swsurface+ w h 32
                                                        #x00ff0000 #x0000ff00 #x000000ff #xff000000))

	     (sdl:blit-surface initial sgum:+null-pointer+ intermediary sgum:+null-pointer+)

	     ;; Tell GL about our new texture
	     (gl:gen-textures 1 *texture*)
	     (gl:bind-texture gl:+texture-2d+ (get-texture 0))
    
	     (gl:tex-image-2d gl:+texture-2d+ 0 4 w h 0
			      gl:+rgba+ gl:+unsigned-byte+ (sdl:surface-pixels intermediary))

	     ;; GL_NEAREST looks horrible, if scaled...
	     (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-min-filter+ gl:+linear+)
	     (gl:tex-parameter-i gl:+texture-2d+ gl:+texture-mag-filter+ gl:+linear+)
	     
	     ;; Prepare to render our texture
	     (gl:enable gl:+texture-2d+)
	     (gl:bind-texture gl:+texture-2d+ (get-texture 0))
	     (gl:color-3f 1.0f0 1.0f0 1.0f0)

	     ;; Draw a quad at location
	     (gl:begin gl:+quads+)
	     ;; Recall that the origin is in the lower-left corner. That is why the TexCoords specify different corners
	     ;; than the Vertex coors seem to.
	     (gl:tex-coord-2f 0.0f0 1.0f0) 
	     (gl:vertex-2f (cfloat (sdl:rect-x location))
			   (cfloat (sdl:rect-y location)))
	     (gl:tex-coord-2f 1.0f0 1.0f0) 
	     (gl:vertex-2f (cfloat (+ (sdl:rect-x location) w))
			   (cfloat (sdl:rect-y location)))
	     (gl:tex-coord-2f 1.0f0 0.0f0) 
	     (gl:vertex-2f (cfloat (+ (sdl:rect-x location) w))
			   (cfloat (+ (sdl:rect-y location) h)))
	     (gl:tex-coord-2f 0.0f0 0.0f0) 
	     (gl:vertex-2f (cfloat (sdl:rect-x location))
			   (cfloat (+ (sdl:rect-y location) h)))
	     (gl:end)
	     
	     ;; Bad things happen if we delete the texture before it finishes
	     (gl:finish)
	     
	     ;; Return the deltas in the unused w, h part of the rect
	     (setf (sdl:rect-w location) (sdl:surface-w initial)
		   (sdl:rect-h location) (sdl:surface-h initial))))))

    ;; Clean up
    (gl:delete-textures 1 *texture*))

(defun dfloat (num)
  (coerce num 'double-float))

(defun gl-enable2d ()
  (let ((vport (sgum:allocate-foreign-object gl:int 4)))
    (gl:get-integerv gl:+viewport+ vport)
    (gl:matrix-mode gl:+projection+)
    (gl:push-matrix)
    (gl:load-identity)

    (gl:ortho 0d0 (dfloat (sgum:deref-array vport gl:int 2))
	      0d0 (dfloat (sgum:deref-array vport gl:int 3))
	      -1d0 1d0)
    (gl:matrix-mode gl:+modelview+)
    (gl:push-matrix)
    (gl:load-identity)))
    
(defun gl-disable2d ()
  (gl:matrix-mode gl:+projection+)
  (gl:pop-matrix)
  (gl:matrix-mode gl:+modelview+)
  (gl:pop-matrix))

(defun init-gl ()
  ;; Irrelevant stuff for this demo
  (gl:shade-model gl:+smooth+)
  (gl:clear-color 0.0f0 0.0f0 0.0f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:depth-func gl:+lequal+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)

  ;; Required if you want alpha-blended textures (for our fonts)
  (gl:blend-func gl:+one+ gl:+one+)
  (gl:enable gl:+blend+)

  ;; Required setup stuff
  (gl:viewport 0 0 800 600)
  (gl:matrix-mode gl:+projection+)
  (gl:load-identity)
  (glu:perspective 45.0d0 (dfloat (/ +screen-width+ +screen-height+)) 0.1d0 50.0d0)
  (gl:matrix-mode gl:+modelview+)
  (gl:load-identity)
  t)

(defvar angle 0.45f0)

(defun draw-gl-scene ()
  ;; render a fun little quad
  (gl:clear (logior gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
  (gl:disable gl:+texture-2d+)
  (gl:load-identity)

  (gl:rotate-f angle 1.0f0 0.0f0 0.0f0)
  (object-3d:cube 0.3f0 (object-3d:make-point-3d -1.0f0 0.0f0 -5.0f0))

  (gl:load-identity)
  (gl:rotate-f angle 0.0f0 0.0f0 1.0f0)
  (object-3d:sphere 0.3f0
                    (object-3d:make-point-3d 1.0f0 0.0f0 -5.0f0)
                    (object-3d:make-color-3d 1.0f0 0.0f0 1.0f0)) 
  
  ;; Go in HUD-drawing mode
  (gl-enable2d)
  (gl:disable gl:+depth-test+)
  (gl:enable gl:+texture-2d+)

  ;; Draw some text
  (let ((color '(255 255 255)))
    (sgum:with-foreign-objects ((position sdl:rect))
      
      (setf (sdl:rect-x position) (floor (/ +screen-width+ 3))
	    (sdl:rect-y position) (floor (/ +screen-height+ 1.5)))
      ;; A quick note about position.
      ;; Enable2D puts the origin in the lower-left corner of the
      ;; screen, which is different from the normal coordinate
      ;; space of most 2D api's. position, therefore,
      ;; gives the X,Y coordinates of the lower-left corner of the rectangle
      ;;(gl:rotate-f angle 0.0f0 1.0f0 0.0f0)
      (sdl-gl-render-text "Hello, World!" font color position)
      
      (setf (sdl:rect-y position) (- (sdl:rect-y position) (sdl:rect-h position) 50))
      (sdl-gl-render-text "A line right underneath" font color position)
      
      (setf (sdl:rect-y position) (- (sdl:rect-y position) (sdl:rect-h position) 50))
      (sdl-gl-render-text "yay text rendering" font color position)))
  
  ;; Come out of HUD mode
  (gl:enable gl:+depth-test+)
  (gl-disable2d)

  ;; Show the screen
  (sdl:gl-swap-buffers)
  (incf angle 0.2f0))

(defun draw-gl-scene-2 (x y z)
  ;; render a fun little quad
  (gl:clear (logior gl:+color-buffer-bit+ gl:+depth-buffer-bit+))
  (gl:disable gl:+texture-2d+)
  (gl:load-identity)

  ;;(gl:rotate-f angle 1.0f0 0.0f0 0.0f0)
  (object-3d:cube 0.1f0 (object-3d:make-point-3d x y z)) ;-1.0f0 0.0f0 -5.0f0))

  ;; Go in HUD-drawing mode
  (gl-enable2d)
  (gl:disable gl:+depth-test+)
  (gl:enable gl:+texture-2d+)

  ;; Draw some text
  (gl:load-identity)
  (let ((color '(255 255 255)))
    (sgum:with-foreign-objects ((position sdl:rect))
      
      (setf (sdl:rect-x position) (floor x);; (- (floor (/ +screen-width+ 2)) 1 -1)
	    (sdl:rect-y position) (floor y));;(floor (/ +screen-height+ 2)))
      (sdl-gl-render-text "Acol" font color position)))
        
  ;; Come out of HUD mode
  (gl:enable gl:+depth-test+)
  (gl-disable2d)

  ;; Show the screen
  (sdl:gl-swap-buffers))

(defun main ()
  (let ((screen nil))
    ;; Do boring initialisation
    (when (null (init-sdl screen))
      (format t "Error while initialising")
      (return-from main nil))

    (setf font (sdl-ttf:open-font font-path 15))
    
    (when (null font)
      (format t "Error loading font")
      (return-from main nil))
    
    (init-gl)
    (sdl:event-loop
     (:quit () 
	    (return))
     (:idle ()
	    (draw-gl-scene-2 -1.0f0 0.0f0 -5.0f0)))

    ;; Clean up
    (sdl-ttf:close-font font)
    (sdl:quit)))
