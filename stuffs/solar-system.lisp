;;;;;; draw string
(defparameter *screen-width* 640)
(defparameter *screen-height* 480)
(defvar *surface*)
(defvar *font*)
(defparameter *font-sizes* (coerce (loop for x from 4 upto 24 by 1 collect x) 'vector))
(defparameter *font-path* (sdl-data:data-file "arial" "ttf"))
(defvar *frames-per-second* 0)
(defvar *frames* 0)
(defvar *previous-fps-calc* 0)

(defparameter *strings*
  (let ((vec (make-array 0
                         :element-type 'string
                         :adjustable t :fill-pointer t)))
    (loop for sym being each external-symbol in :common-lisp do
          (vector-push-extend (string-downcase (symbol-name sym)) vec))
    vec))

(defstruct visible string font-number r g b x y)

(defparameter *scale* 1.1)
(defvar *adjusted-scale* 1.1)

(defparameter *number-visible* 75)
(defvar *visible*)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun initgl0 ()
  (gl:shade-model gl:+smooth+)
  #+nil (sdl:set-gl-attributes :red-size 4 :blue-size 4 :green-size 4
			 :doublebuffer 1 :depth-size 16)
  (gl:clear-color 0.0f0 0.0f0 0.0f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+lequal+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)

  (sdl-ttf:init)
  (setf *visible* (make-array *number-visible* :element-type 'visible))
  (init-visible)

  ;; Load font
  (setf *font* (map 'vector 
                     #'(lambda (size) (sdl-ttf:open-font *font-path* size)) *font-sizes*))
           
  ;; Check the font
  (when (some #'(lambda (f) (sgum:null-pointer-p f)) *font*)
    (free-fonts *font*)
    (error "Unable to load fonts"))
  t)

(defun event-loop0 (surface update-fn resize-fn handle-keypress-fn bpp video-flags)
  (declare (ignorable surface))
  (sdl:event-loop
   (:key-down (key)
	      (if (= key (char-code #\q))
                  (return)
                  (funcall handle-keypress-fn key)))
   (:mouse-button-up (button x y)
		     (format t "Mouse button up: ~A (~A, ~A)~%" button x y))
   (:mouse-button-down (button x y)
		       (format t "Mouse button dn: ~A (~A, ~A)~%" button x y))
   (:quit ()
	  (return))
   (:resize (width height)
	    (format t "Resized width = ~A height = ~A~%" width height)
            (sdl:free-surface surface)
            (setf surface
                  (sdl:set-video-mode width height bpp video-flags))
            (funcall resize-fn surface width height))
   (:idle ()
	  (progn (funcall update-fn surface)
                 (incf-frames)))))

(defun init-sdl (&key (width 640) (height 480) (bpp 16)
                 (init-gl-fn #'initgl0)
                 (event-loop-fn #'event-loop0)
               ;;  (font-fn #'identity)
                 (update-fn #'identity)
                 (resize-fn #'resize0)
                 (handle-keypress-fn #'identity)
                 (video-flags (logior sdl:+opengl+ sdl:+resizable+ sdl:+gl-doublebuffer+ sdl:+hwpalette+)))
  
  (sdl:init sdl:+init-video+)
  
  (multiple-value-bind (hw-available-p blit-hw-p)
      (sdl:query-video-info :hw-available :blit-hw)
    (setf video-flags (logior video-flags
                              (if hw-available-p sdl:+hwsurface+ sdl:+swsurface+)
                              (if blit-hw-p sdl:+hwaccel+ 0))))
  (setf *frames* 0
        *start-time* (get-universal-time))
  (sdl:gl-set-attribute sdl:+gl-doublebuffer+ 1)
  
  (let ((surface (sdl:set-video-mode width height bpp video-flags)))
    (when (sgum:null-pointer-p surface)
      (error "Failed to obtain surface"))
    (unless (funcall init-gl-fn)
      (error "Failed to initialize GL"))
    (funcall resize-fn surface width height)
    (unwind-protect (funcall event-loop-fn surface update-fn resize-fn handle-keypress-fn bpp video-flags)
      (progn (sdl:free-surface surface)
             (free-fonts *fonts*)
             (sdl:quit)))
    (values)))

(defun incf-frames ()
  (incf *frames*)
  (let* ((time (get-universal-time))
         (delta (- time *start-time*)))
    (when (>= delta 5)
      (format t "~&~A frames in ~A seconds = ~A FPS~%"
              *frames* delta (float (/ *frames* delta)))
      (setf *frames* 0
            *start-time* time))))

;;; Draw solar system
(defvar *solar-system-sun*)
(defvar *solar-system-planet*)
(defvar *solar-system-satellite*)
(defparameter *xspeed-ss* 0f0)
(defparameter *yspeed-ss* 0f0)
(defparameter *z-ss* 10f0)

(declaim (inline sf))
(defun sf (x)
  (coerce x 'single-float))

(defun draw-solar-system-body (size)
  (glu:with-quadric (sphere)
    (glu:quadric-draw-style sphere glu:+fill+)
    (glu:quadric-normals sphere glu:+smooth+)
    (glu:sphere sphere size 25 20)))

(defun draw-sphere (size)
  (glu:with-quadric (sphere)
    (glu:quadric-draw-style sphere glu:+fill+)
    (glu:quadric-normals sphere glu:+smooth+)
    (glu:sphere sphere size 25 20)))

(defun initgl-sphere ()
  (setf *solar-system-planet* (gl:gen-lists 1))
  (gl:new-list *solar-system-planet* gl:+compile+)
  (draw-sphere 1.0d0)
  (gl:end-list)
  (gl:shade-model gl:+smooth+)
  (gl:clear-color 0.0f0 0.0f0 0.0f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+lequal+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)

  (sgum:with-single-float-values (mat-spec 0.5f0 0.5f0 0.3f0 1f0)
    (gl:material-fv gl:+front+ gl:+specular+ mat-spec))
  (sgum:with-single-float-values (mat-shin 30f0)
    (gl:material-fv gl:+front+ gl:+shininess+ mat-shin))
  (sgum:with-single-float-values (light-amb 0.2f0 0.2f0 0.2f0 1f0)
    (gl:light-fv gl:+light0+ gl:+ambient+ light-amb))
  (sgum:with-single-float-values (light-diff 1f0 1f0 1f0 1f0)
    (gl:light-fv gl:+light0+ gl:+diffuse+ light-diff))
  
  (gl:enable gl:+lighting+)
  (gl:enable gl:+light0+)

  (gl:enable gl:+color-material+)
  (gl:color-material gl:+front+ gl:+position+)
  
  t)

(defun initgl-solar-system ()
  (setf *solar-system-sun* (gl:gen-lists 1))
  (setf *solar-system-planet* (gl:gen-lists 1))
  (setf *solar-system-satellite* (gl:gen-lists 1))
  
  (gl:new-list *solar-system-sun* gl:+compile+)
  (draw-solar-system-body 0.40d0)
  (gl:end-list)

  (gl:new-list *solar-system-planet* gl:+compile+)
  (draw-solar-system-body 0.20d0)
  (gl:end-list)

  (gl:new-list *solar-system-satellite* gl:+compile+)
  (draw-solar-system-body 0.10d0)
  (gl:end-list)
  
  (gl:shade-model gl:+smooth+)
  (gl:clear-color 0.0f0 0.0f0 0.0f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+lequal+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)

  (sgum:with-single-float-values (mat-spec 0.5f0 0.5f0 0.3f0 1f0)
    (gl:material-fv gl:+front+ gl:+specular+ mat-spec))
  (sgum:with-single-float-values (mat-shin 30f0)
    (gl:material-fv gl:+front+ gl:+shininess+ mat-shin))
  (sgum:with-single-float-values (light-amb 0.2f0 0.2f0 0.2f0 1f0)
    (gl:light-fv gl:+light0+ gl:+ambient+ light-amb))
  (sgum:with-single-float-values (light-diff 1f0 1f0 1f0 1f0)
    (gl:light-fv gl:+light0+ gl:+diffuse+ light-diff))
  
  (gl:enable gl:+lighting+)
  (gl:enable gl:+light0+)

  (gl:enable gl:+color-material+)
  (gl:color-material gl:+front+ gl:+position+)
  
  t)

(flet ((init-body-data (array n &rest args)
         ;;tx ty tz cr cg cb ra rx ry rz r1 r2
         (loop for arg in args
               for i from 0 do
               (setf (aref array n i) arg))))
  
  (let* ((rot-vec (make-array 2 :element-type 'single-float
                              :initial-element 0f0))
         (num-bodies 5)
         (body-data (make-array `(,num-bodies 12)
                                :element-type 'single-float
                                :initial-element 0f0))
         (satellite-p (make-array num-bodies
                                  :element-type 'boolean
                                  :initial-element nil))
         (satellite-data (make-array `(,num-bodies 12)
                                     :element-type 'single-float
                                     :initial-element 0f0)))
    (init-body-data body-data 0
                    0f0 0f0 0f0
                    1f0 1f0 0f0
                    1f0 0f0 0f0 1f0
                    0f0
                    0f0)
    
    (init-body-data body-data 1
                    0f0 6.0f0 0f0
                    0.9f0 0.2f0 0.2f0
                    2f0 0f0 0f0
                    1f0 0f0 0f0)
    
    (init-body-data body-data 2
                    2.5f0 0f0 0f0
                    0.5f0 0f0 0.5f0
                    2f0 1f0 1f0 0f0
                    0f0
                    0f0)
    (init-body-data body-data 3
                    1.5f0 0f0 0f0
                    0.6f0 0.5f0 0.5f0
                    2f0 1f0 1f0 0f0
                    0f0
                    0f0)
    (init-body-data body-data 4
                    4f0 0f0 0f0
                    0f0 0.5f0 1f0
                    0.5f0 0f0 1f0 0f0
                    0f0
                    0f0)
    (init-body-data satellite-data 4
                    0.7f0 0f0 0f0
                    0.3f0 0.3f0 0.3f0
                    0.3f0 0f0 1f0 0f0
                    0f0
                    0f0)
    (setf (aref satellite-p 4) t)
    
    (defun draw-gl-scene-solar-system (surface)
      (declare (ignorable surface))
      (gl:clear (logior gl:+color-buffer-bit+
                        gl:+depth-buffer-bit+))
      
      (gl:load-identity)
      (glu:look-at 0d0 0d0 (coerce *z-ss* 'double-float)
                   0d0 0d0 0d0
                   0d0 1d0 0d0)
      
      (sgum:with-single-float-values (light-pos 0f0 0f0 0f0 1.0f0)
        (gl:light-fv gl:+light0+ gl:+position+ light-pos))
      (gl:rotate-f (aref rot-vec 0) 1f0 0f0 0f0)
      (gl:rotate-f (aref rot-vec 1) 0f0 1f0 0f0)

      (loop for i from 0 below num-bodies do
            (gl:push-matrix)

            (gl:rotate-f (aref body-data i 11)
                         0f0 0f0 1f0)
            
            (gl:translate-f (aref body-data i 0)
                            (aref body-data i 1)
                            (aref body-data i 2))
            
            
            (gl:push-matrix)
            (gl:rotate-f (aref body-data i 10)
                         (aref body-data i 7)
                         (aref body-data i 8)
                         (aref body-data i 9))
            
            
            (gl:color-3f (aref body-data i 3)
                         (aref body-data i 4)
                         (aref body-data i 5))
            
            (cond ((zerop i)
                   (sgum:with-single-float-values (emis 0.9f0 0.7f0 0.1f0 0f0)
                     (gl:material-fv gl:+front+ gl:+emission+ emis))
                   (gl:call-list *solar-system-sun*)
                   (sgum:with-single-float-values (emis 0.0f0 0.0f0 0.0f0 0f0)
                     (gl:material-fv gl:+front+ gl:+emission+ emis)))
                  (t (gl:call-list *solar-system-planet*)))

            (gl:pop-matrix)
           
            (when (aref satellite-p i)
              (gl:rotate-f (aref satellite-data i 11)
                           0f0 0f0 1f0)
              (gl:translate-f (aref satellite-data i 0)
                              (aref satellite-data i 1)
                              (aref satellite-data i 2))
              (gl:rotate-f (aref satellite-data i 10)
                           (aref satellite-data i 7)
                           (aref satellite-data i 8)
                           (aref satellite-data i 9))
              (gl:color-3f (aref satellite-data i 3)
                           (aref satellite-data i 4)
                           (aref satellite-data i 5))
              (gl:call-list *solar-system-satellite*)
              (incf (aref satellite-data i 10)
                    (aref satellite-data i 6))
              (let ((dist (abs (+ (aref satellite-data i 0)
                                  (aref satellite-data i 1)
                                  (aref satellite-data i 2)))))
                (unless (zerop dist)
                  (incf (aref satellite-data i 11)
                        (sf (/ dist))))))
            
            (incf (aref body-data i 10)
                  (aref body-data i 6))
            (let ((dist (abs (+ (aref body-data i 0)
                                (aref body-data i 1)
                                (aref body-data i 2)))))
              (unless (zerop dist)
                (incf (aref body-data i 11)
                      (sf (/ dist)))))

            (gl:pop-matrix))

      (incf (aref rot-vec 0) *xspeed-ss*)
      (incf (aref rot-vec 1) *yspeed-ss*)
      (sdl:gl-swap-buffers)
      
      t)))

(defun draw-gl-sphere (surface)
  (declare (ignorable surface))
      (gl:clear (logior gl:+color-buffer-bit+
                        gl:+depth-buffer-bit+))
      
      (gl:load-identity)
      (glu:look-at 0d0 0d0 0d0
                   0d0 0d0 0d0
                   0d0 1d0 0d0)
      
      (sgum:with-single-float-values (light-pos 0f0 0f0 0f0 1.0f0)
        (gl:light-fv gl:+light0+ gl:+position+ light-pos))
      (gl:rotate-f 0.6f0 0f0 1f0 0f0)
      (gl:push-matrix)
      (gl:call-list *solar-system-planet*)

      (gl:pop-matrix)
      (sdl:gl-swap-buffers))

(defparameter *rtri5* 0.2f0)
(defparameter *rquad5* 0.45f0)

(defparameter *tri5*
  '((1f0 0f0 0f0) (0f0 1f0 0f0)
    (0f0 1f0 0f0) (-1f0 -1f0 1f0)
    (0f0 0f0 1f0) (1f0 -1f0 1f0)

    (1f0 0f0 0f0) (0f0 1f0 0f0)
    (0f0 0f0 1f0) (1f0 -1f0 1f0)
    (0f0 1f0 0f0) (1f0 -1f0 -1f0)

    (1f0 0f0 0f0) (0f0 1f0 0f0)
    (0f0 1f0 0f0) (1f0 -1f0 -1f0)
    (0f0 0f0 1f0) (-1f0 -1f0 -1f0)

    (1f0 0f0 0f0) (0f0 1f0 0f0)
    (0f0 0f0 1f0) (-1f0 -1f0 -1f0)
    (0f0 1f0 0f0) (-1f0 -1f0 1f0)))

(defparameter *quad5*
  '((0.0f0 1.0f0 0.0f0)
    ( 1.0f0  1.0f0 -1.0f0)			
    (-1.0f0  1.0f0 -1.0f0)			
    (-1.0f0  1.0f0  1.0f0)			
    ( 1.0f0  1.0f0  1.0f0)	

    (1.0f0 0.5f0 0.0f0)
    ( 1.0f0 -1.0f0  1.0f0)
    (-1.0f0 -1.0f0  1.0f0)
    (-1.0f0 -1.0f0 -1.0f0)
    ( 1.0f0 -1.0f0 -1.0f0)

    (1.0f0 0.0f0 0.0f0)
    ( 1.0f0  1.0f0  1.0f0)
    (-1.0f0  1.0f0  1.0f0)
    (-1.0f0 -1.0f0  1.0f0)
    ( 1.0f0 -1.0f0  1.0f0)

    (1.0f0 1.0f0 0.0f0)
    ( 1.0f0 -1.0f0 -1.0f0)
    (-1.0f0 -1.0f0 -1.0f0)
    (-1.0f0  1.0f0 -1.0f0)
    ( 1.0f0  1.0f0 -1.0f0)

    (0.0f0 0.0f0 1.0f0)
    (-1.0f0  1.0f0  1.0f0)
    (-1.0f0  1.0f0 -1.0f0)
    (-1.0f0 -1.0f0 -1.0f0)
    (-1.0f0 -1.0f0  1.0f0)

    (1.0f0 0.0f0 1.0f0)
    ( 1.0f0  1.0f0 -1.0f0)
    ( 1.0f0  1.0f0  1.0f0)
    ( 1.0f0 -1.0f0  1.0f0)
    ( 1.0f0 -1.0f0 -1.0f0)))

(defun draw-triangles5 (tri)
  (gl:begin gl:+triangles+)
  (loop for (color-triple vertex-triple) on tri by #'cddr
        until (null vertex-triple) do
        (apply #'gl:color-3f color-triple)
        (apply #'gl:vertex-3f vertex-triple))
  (gl:end))

(defun 5cdr (x)
  (nthcdr 5 x))

(defun draw-quads5 (quad size)
  (gl:begin gl:+quads+)
  (loop for (color-triple v1 v2 v3 v4) on quad by #'5cdr
     until (or (null v1) (null v2) (null v3) (null v4)) do
     (apply #'gl:color-3f color-triple)
     (apply #'gl:vertex-3f (mapcar #'(lambda (x) (setf x (* x size))) v1))
     (apply #'gl:vertex-3f (mapcar #'(lambda (x) (setf x (* x size))) v2))
     (apply #'gl:vertex-3f (mapcar #'(lambda (x) (setf x (* x size))) v3))
     (apply #'gl:vertex-3f (mapcar #'(lambda (x) (setf x (* x size))) v4)))
  (gl:end))
        
(defun draw-gl-scene-triangle (surface)
  (declare (ignorable surface))
  ;(gl:clear-color 0f0 0f0 0f0 0f0)
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))

  (gl:load-identity)
  (gl:translate-f 0.0f0 0.0f0 -6.0f0)
  (gl:rotate-f *rtri5* 0.0f0 1f0 0f0)

  (draw-triangles5 *tri5*)
  
  (sdl:gl-swap-buffers)

  (incf *rtri5* 0.2f0)
  t)

(defun draw-gl-scene-quad (surface)
  (declare (ignorable surface))
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))

  (gl:load-identity)
  (gl:translate-f 0.0f0 0.0f0 -20.0f0)
  (gl:rotate-f *rquad5* 0.6f0 1f0 0f0)

  (gl:color-3f 0.5f0 0.5f0 1.0f0)
  
  (draw-quads5 *quad5* 0.5)

  (sdl:gl-swap-buffers)
  
  (decf *rquad5* 0.15f0)
  t)
  
(defun handle-keypress-solar-system (key)
  (cond
    ((eql 275 key)                      ; right arrow
     (incf *yspeed-ss* 0.05f0))
    ((eql 273 key)                      ; up arrow
     (decf *xspeed-ss* 0.05f0))
    ((eql 276 key)                      ; left arrow
     (decf *yspeed-ss* 0.05f0))
    ((eql 274 key)                      ; down arrow
     (incf *xspeed-ss* 0.05f0))
    ((eql 281 key)                      ; pgdown
     (incf *z-ss* 0.2f0))
    ((eql 280 key)                      ; pgup
     (decf *z-ss* 0.2f0))))

(defun init-visible (&optional (nth nil) (box-x *screen-width*) (box-y *screen-height*))
  (flet ((setn (n)
           (let ((string (aref *strings* (random (length *strings*))))
                 (font 0)
                 (x (- (random box-x)
                       (truncate box-x 2)))
                 (y (- (random box-y)
                       (truncate box-y 2)))
                 (r (random 256))
                 (g (random 256))
                 (b (random 256)))
             (when (and (= x 0) (= y 0))
               (setf x 1 y 1))
             (setf (aref *visible* n)
                   (make-visible :string string
                                 :font-number font
                                 :x x :y y :r r :g g :b b)))))
    (if (integerp nth)
        (setn nth)
        (dotimes (n *number-visible*)
          (setn n)))))

(defun init-gl-text ()
  (sdl:init sdl:+init-video+)
  (sdl-ttf:init)
  (setf *visible* (make-array *number-visible* :element-type 'visible))
  (init-visible)

  ;; Load font
  (setf *font* (map 'vector 
                     #'(lambda (size) (sdl-ttf:open-font *font-path* size)) *font-sizes*))
           
  ;; Check the font
  (when (some #'(lambda (f) (sgum:null-pointer-p f)) *font*)
    (free-fonts *font*)
    (error "Unable to load fonts")))

(defun draw-gl-text (surface)
  (declare (ignorable surface))
  (let* ((sw/2 (truncate *screen-width* 2))
         (sh/2 (truncate *screen-height* 2))
         (len (length *font*))
         (sw/2/len (truncate sw/2 len))
         (sh/2/len (truncate sh/2 len))
         (x.y-thresh (loop for i from 1 upto len collecting
                           `(,(* i sw/2/len) . ,(* i sh/2/len)))))
    
    (sdl:draw-filled-rectangle surface
                               0 0
                               *screen-width* *screen-height*
                               0 0 0)
    (loop for visible across *visible* 
          for n from 0 do
          (let ((string (visible-string visible))
                (font (aref *font* (visible-font-number visible)))
                (x (+ (truncate (visible-x visible))
                      sw/2))
                (y (+ (truncate (visible-y visible))
                      sh/2)))
            (multiple-value-bind (w h)
                (cl-sdl-ttf:size-text font string)
              (decf x (truncate w 2))
              (decf y (truncate h 2)))
            
            (cl-sdl-ttf:with-solid-text (s font string 
                                           (visible-r visible)
                                           (visible-g visible)
                                           (visible-b visible))
              (sgum:with-foreign-objects ((rect sdl:rect))
                (setf
                 (sdl:rect-x rect) x
                 (sdl:rect-y rect) y)
                (sdl:blit-surface s sgum:+null-pointer+
                                  surface rect)))


            ;; Adjust the scale relative to the frames per second
            (setf *adjusted-scale*
                  (if (zerop *frames-per-second*)
                      *scale*
                      (1+ (/ (1- *scale*)
                             *frames-per-second*
                             1/30))))
            
            (setf (visible-x visible)
                  (* *adjusted-scale* (visible-x visible)))
            (setf (visible-y visible)
                  (* *adjusted-scale* (visible-y visible)))
            (block determine-font
              (loop for (xt . yt) in x.y-thresh 
                    for i from 0 do
                    (when (and (< (abs (visible-x visible)) xt)
                               (< (abs (visible-y visible)) yt))
                      (setf (visible-font-number visible) i)
                      (return-from determine-font)))
              (init-visible n 49 49)))))
  (sdl:flip surface)
  (incf *frames*)
  (when (>= (- (get-universal-time)
               *previous-fps-calc*)
            5)
    (setf *frames-per-second* (/ *frames* 5)
          *frames* 0
          *previous-fps-calc* (get-universal-time))
    (format t "~&Frames per second: ~F~%"
            *frames-per-second*)))

;; 2 ham nay co the gop lai dung event-loop0
(defun handle-key (key)
  (cond ((= key (char-code #\c))
         (init-visible))))

;; Co the goi event-loop0 thay the duoc
;; (defun event-loop-text ()
;;   (sdl:event-loop
;;    (:idle ()
;;           (draw-gl-text))
;;    (:key-down (scan-code key mod unicode)
;;               (cond ((= key (char-code #\q))
;;                      (return))
;;                     (t (handle-key key))))))

(defun free-fonts (fonts)
  (loop for font across fonts do
        (unless (sgum:null-pointer-p font)
          (sdl-ttf:close-font font))))

;;; main program
(defun start ()
  ;;   (init-sdl :init-gl-fn #'initgl-solar-system
  ;;             :event-loop-fn #'event-loop0
  ;;             :update-fn #'draw-gl-scene-solar-system
  ;;             :handle-keypress-fn #'handle-keypress-solar-system))

  ;;   (init-sdl :init-gl-fn #'initgl0
  ;;             :event-loop-fn #'event-loop0
  ;;             :update-fn #'draw-gl-scene-quad))

  ;;   (init-sdl :init-gl-fn #'initgl-sphere
  ;;             :event-loop-fn #'event-loop0
  ;;             :update-fn #'draw-gl-quad))

    (init-sdl :init-gl-fn #'initgl0
              :event-loop-fn #'event-loop0
              :update-fn #'draw-gl-text
              :handle-keypress-fn #'handle-key))
