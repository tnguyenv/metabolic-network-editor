
;;(in-package #:bio-app)

(defparameter *x* 0.0f0)
(defparameter *y* 0.0f0)
(defparameter *z* -6.0f0)

; Delta for keyboard scroll
(defparameter *dx* 0.5f0)
(defparameter *dy* 0.5f0)
(defparameter *dz* 0.5f0)
; Delta for mouse scroll
(defparameter *dxm* 0.04f0)
(defparameter *dym* -0.04f0) ; negative for invert mouse
;Rotational position around the x and y axes
(defparameter *xr* 0f0)
(defparameter *yr* 0f0)
; Delta for mouse rotate
(defparameter *dxr* 0.5f0)
(defparameter *dyr* 0.5f0)

;;; SDL_MouseDown
(defparameter *select* nil)
(defparameter *projection-base* (sgum:allocate-foreign-object gl:float 16))
(defparameter *proj-presel* (sgum:allocate-foreign-object gl:double 16))

(defconstant +selection-buffer-len+ 512)
(defconstant +feed-buffer-size+ 4096)

; Current mouse position
(defparameter *mx* 0)
(defparameter *my* 0)

; Mouse buttons
(defparameter *mb* (make-array 7 :initial-element nil))
; 319 is the highest key code returned by SDL based on experimentation
(defparameter *keys* (make-array 319 :initial-element nil))

(defparameter *scene* nil)

(defparameter *mvm* (sgum:allocate-foreign-object gl:double 16))
  
(defun gl-init (width height)
  (gl:shade-model gl:+smooth+)
  (sdl:set-gl-attributes :red-size 4 :blue-size 4 :green-size 4
			 :doublebuffer 1 :depth-size 16)
  (gl:clear-color 0.0f0 0.0f0 0.0f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+lequal+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)
  (gl:viewport 0 0 width height)
  (gl:matrix-mode gl:+projection+)
  (gl:load-identity)
  (glu:perspective 60d0 1d0 0d0001 1000d0)
  (gl:matrix-mode gl:+modelview+)
  t)

(defun draw-sphere (size)
  (glu:with-quadric (sphere)
    (glu:quadric-draw-style sphere glu:+fill+)
    (glu:quadric-normals sphere glu:+smooth+)
    (glu:sphere sphere size 25 20)))

(defun gl-normal-render (surface)
  ())

(defun gl-picking-render (surface)
  (declare (ignorable surface))
  (gl:clear-color 0f0 0f0 0f0 0f0)
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))

  ;; Save the matrix state and do the rotations
  (gl:matrix-mode gl:+modelview+)
  (gl:push-matrix)

  ;; Translate the whole scene out and into view
  (gl:translate-f -1.5f0 0.0f0 -6.0f0)

  ;; Initialize the names stack
  (gl:init-names)
  (gl:push-name 0)

  ;; Draw the first sphere
  (gl:color-3f 1f0 0f0 0f0)
  (gl:load-name 1)
  (gl:pass-through 1.0f0)
  (draw-sphere 0.5d0)
  
  ;; Draw the second sphere
  (gl:translate-f 3f0 0f0 0f0)
  (gl:color-3f 0f0 0f0 1f0)
  (gl:load-name 2)
  (gl:pass-through 2.0f0)
  (draw-sphere 0.5d0)

  ;; Restore the matrix state (model matrix)
  (gl:pop-matrix)
  (gl:flush)
  (sdl:gl-swap-buffers)
  t)

(defun gl-render-scene (surface)
  (declare (ignorable surface))
  (gl:clear-color 0f0 0f0 0f0 0f0)
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))

  ;; Save the matrix state and do the rotations
  (gl:matrix-mode gl:+modelview+)
  (gl:push-matrix)
  ;; Translate the whole scene out and into view
  (gl:translate-f -1.5f0 0.0f0 -6.0f0)
  ;;(gl:scale-f 2.5f0 2.5f0 2.5f0)
  ;; Initialize the names stack
  (gl:init-names)
  (gl:push-name 0)
  ;; Draw the first sphere
  (gl:color-3f 1f0 0f0 0f0)
  (gl:load-name 10)
  (gl:pass-through 1.0f0)
  (draw-sphere 0.5d0)
  ;; Draw the second sphere
  (gl:translate-f 1f0 0f0 0f0)
  (gl:color-3f 0f0 0f0 1f0)
  (gl:load-name 20)
  (gl:pass-through 2.0f0)
  (draw-sphere 0.5d0)
  ;; Restore the matrix state (model matrix)
  (gl:pop-matrix)
  (gl:flush)
  (sdl:gl-swap-buffers)
  t)

(defun list-hits (hits buffer)
  (format t "~A hits:~%" hits)
  (dotimes (i hits)
    (format t "Number: ~A~% Min Z: ~A~% Max Z: ~A~% Name on stack: ~A~%"
            (sgum:deref-array buffer gl:int (* i 4))
            (sgum:deref-array buffer gl:int (+ (* i 4) 1))
            (sgum:deref-array buffer gl:int (+ (* i 4) 2))
            (sgum:deref-array buffer gl:int (+ (* i 4) 3)))))

(defun zoom1 () ;;in-out rate)
  (gl:viewport 0 0 1000 800)
  (gl:matrix-mode gl:+projection+)
  
  (gl:ortho 0.0d0 1.0d0 0.0d0 1.0d0 -1.0d0 1.0d0)
  (gl:load-identity))

(defun zoom2 () ;;in-out rate)
  (gl:viewport 0 0 640 480)
  (gl:matrix-mode gl:+projection+)
  
  (gl:ortho 0.0d0 1.0d0 0.0d0 1.0d0 -1.0d0 1.0d0)
  (gl:load-identity))

(defun gl-move-object (x y z)
  ())

(defun handle-select (choice)
  (format t "~%Inverse the color of the object clicked ~A~%" choice)
  (gl:color-3f 1f0 0f0 1f0))

(defun gl-select-object (surface x y)
  ;; buffer: space for selection buffer
  ;; viewport: viewport storage
  ;; hits: hit counter
  (let ((buffer (sgum:allocate-foreign-object gl:uint +selection-buffer-len+))
        (viewport (sgum:allocate-foreign-object gl:int 4))
        (hits 0))
    ;;; Start picking
    ;; Set up selection buffer
    (gl:select-buffer +selection-buffer-len+ buffer)
    ;; Get the viewport
    (gl:get-integerv gl:+viewport+ viewport)
    (format t "Viewport: ~A~%" (loop for n from 0 to 3 collect (sgum:deref-array viewport gl:int n)))
    ;; Switch to projection and save the matrix
    (gl:matrix-mode gl:+projection+)
    (gl:push-matrix)
    ;; Change render mode
    (gl:render-mode gl:+select+)
    ;; Initialize the names stack
    (gl:init-names)
    (gl:push-name 0)
    ;; Establish new clipping volume to be unit cube around mouse cusor point (x, y) and extending two pixels
    ;; in the vertical and horizontal direction
    (gl:load-identity)
    (glu:pick-matrix (coerce x 'double-float)
                     (coerce y 'double-float)
                     1d0 1d0 viewport)
    ;; Apply perspective matrix
    (glu:perspective 45d0
                     (coerce
                      (/ (- (sgum:deref-array viewport gl:int 2)
                            (sgum:deref-array viewport gl:int 0))
                         (- (sgum:deref-array viewport gl:int 3)
                            (sgum:deref-array viewport gl:int 1)))
                      'double-float)
                     0.1d0 100.0d0)
    ;; Draw scene
    (gl-render-scene surface)

    ;;; Stop picking and process the object picked
    ;; Collect the hits and display
    (setf hits (gl:render-mode gl:+render+))
    (list-hits hits buffer)

    ;; Restore the projection matrix
    (gl:matrix-mode gl:+projection+)
    (gl:pop-matrix)
    ;; Go back to modelview for normal rendering
    (gl:matrix-mode gl:+modelview+)

    ;; If a single hit occurred, display the information
    (if (= hits 1)
        (handle-select (sgum:deref-array buffer gl:int 3)))))

(defun gl-unselect-object ()
  (setf *select* nil))

(defun handle-mouse-down (surface x y)
  (if *select*
      (gl-unselect-object)
      (gl-select-object surface x y)))

(defparameter *pressed-button* 1)

(defun my-event-loop (surface update-fn resize-fn handle-keypress-fn bpp video-flags)
  (sdl:event-loop
   (:key-down (key)
	      (if (= key (char-code #\q))
                  (return)
                  (funcall handle-keypress-fn key)))
   (:mouse-button-up (x y button)
                     (if (eql nil button)
                         (progn
                           (format t "Mouse button up: WHEEL (~A, ~A)~%" x y)
                           ;; When the user scrolls the wheel
                           (zoom1)
                           (setf *z* (+ *z* *dz*)))
                         (progn
                           (format t "Mouse button up: ~A (~A, ~A)~%" button x y)
                           ;;(gl:scale-f 0.5f0 0.5f0 0.5f0)
                           ;;(gl:pixel-zoom
                           ;; (coerce x 'float) (coerce y 'float))
                           (cond ((string= "LEFT" button)
                                  (setf (aref *mb* sdl:+button-left+) nil))
                                 ((string= "MIDDLE" button)
                                  (setf (aref *mb* sdl:+button-middle+) nil))
                                 (t
                                  (setf (aref *mb* sdl:+button-right+) nil))))))
   (:mouse-button-down (x y button)                    
                       (if (eql nil button)
                           (progn
                             (format t "Mouse button dn: WHEEL (~A, ~A)~%" x y)
                             (zoom2)
                             (setf *z* (- *z* *dz*)))
                           (progn
                             (format t "Mouse button dn: ~A (~A, ~A)~%" button x y)
                             (cond ((string= "LEFT" button)
                                    (setf *pressed-button* sdl:+button-left+)
                                    (setf (aref *mb* sdl:+button-left+) t))
                                   ((string= "MIDDLE" button)
                                    (setf *pressed-button* sdl:+button-middle+)
                                    (setf (aref *mb* sdl:+button-middle+) t))
                                   (t
                                    (setf *pressed-button* sdl:+button-right+)
                                    (setf (aref *mb* sdl:+button-right+) t)))))

                       (if (= sdl:+button-left+ *pressed-button*) 
                           (progn
                             (handle-mouse-down surface x (- (sdl:surface-h surface) y)))))                             
   (:mouse-motion (x y xrel yrel state)
                  (setf *mx* x)
                  (setf *my* y)
                  (if (aref *mb* 3)
                      (progn
                        (incf *x* (* *dxm* xrel))
                        (incf *y* (* *dym* yrel))))

                  (if (aref *mb* 2)
                      (progn
                        ;; Moving mouse left <-> right rotates around Y axis and vice versa
                        (incf *xr* (* *dxr* yrel))
                        (incf *yr* (* *dyr* xrel))))
                  
                  (if (aref *mb* 1)
                      ;; if there is a selected object
                      (if *select*
                          (let ((obj (nth (car *select*) *scene*)))
                            ;; drag the selected object
                            (incf (pos-x (pos obj)) (* *dxm* xrel))
                            (incf (pos-y (pos obj)) (* *dym* yrel))))))
  (:quit ()
         (return))
  (:resize (width height)
           (format t "Resized width = ~A height = ~A~%" width height)
           (sdl:free-surface surface)
           (setf surface
                 (sdl:set-video-mode width height bpp video-flags))
           (funcall resize-fn surface width height))
  (:idle ()
         (funcall update-fn surface))))

(defun resize (surface width height)
  (declare (ignorable surface))
  (when (zerop height)
    (setf height 1))
  (let ((ratio (coerce (/ width height) 'double-float)))
    (gl:viewport 0 0 width height)
    (gl:matrix-mode gl:+projection+)
    (gl:load-identity)
    (gl:get-floatv gl:+projection-matrix+ *projection-base*)
    (glu:perspective 45.0d0 ratio 0.1d0 100.0d0)
    (gl:matrix-mode gl:+modelview+)
    (gl:load-identity)
    t))

(defun init-sdl (&key (width 640) (height 480) (bpp 16) (title "OpenGL in Common Lisp")
                 (init-gl-fn #'gl-init)
                 (event-loop-fn #'my-event-loop)
                 (update-fn #'identity)
                 (resize-fn #'resize)
                 (handle-keypress-fn #'identity)
                 (video-flags (logior sdl:+opengl+ sdl:+resizable+ sdl:+gl-doublebuffer+ sdl:+hwpalette+)))
  ;; Initilize SDL
  (sdl:init sdl:+init-video+)
  
  (multiple-value-bind (hw-available-p blit-hw-p)
      (sdl:query-video-info :hw-available :blit-hw)
    (setf video-flags (logior video-flags
                              (if hw-available-p
                                  sdl:+hwsurface+
                                  sdl:+swsurface+)
                              (if blit-hw-p
                                  sdl:+hwaccel+
                                  0))))
  (sdl:gl-set-attribute sdl:+gl-doublebuffer+ 1)
  
  ;; Set video mode
  (let ((surface (sdl:set-video-mode width height bpp video-flags)))
    (when (sgum:null-pointer-p surface)
      (error "Failed to obtain surface"))
    (unless (funcall init-gl-fn width height)
      (error "Failed to initialize GL"))
    (sdl:wm-set-caption title nil)
    ;; Call resize routine to the window
    (funcall resize-fn surface width height)
    ;; Call update ... functions
    (unwind-protect (funcall event-loop-fn surface update-fn resize-fn handle-keypress-fn bpp video-flags)
      (progn (sdl:free-surface surface)
             (sdl:quit)))
    (values)))

(defun start ()
  (init-sdl :title "Selection and pick object up demo"
            :init-gl-fn #'gl-init
            :event-loop-fn #'my-event-loop
            :update-fn #'gl-render-scene
            :resize-fn #'resize))
