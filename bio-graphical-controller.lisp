;;; -*- Mode: Lisp; Package: BIO-CONTROLLER -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines objects that control graphical view and data model

(in-package :bio-controller)

;;;; Dau vao mot Metabolic-Graph
;;;; Dau ra mot Metabolic-Graph da duoc optimize placements
;;;; Controller tac dong len Model, lam thay doi data, sau co notify View de tien hanh cap nhat view

(defclass GraphicViewController ()
  ((model
    :accessor model
    :initarg :model
    :initform '()
    :documentation "Model")
   (view
    :accessor view
    :initarg :view
    :initform '()
    :documentation "")))

(defclass GraphicView3DController (GraphicViewController)
  ())

(defclass GraphicViewTextController (GraphicViewController)
  ())

(defgeneric update-model (view))

(defgeneric act-event ())

(defgeneric optimize-placements ())


