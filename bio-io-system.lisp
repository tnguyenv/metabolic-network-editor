;;; -*- Mode: Lisp; Package: BIO-IOS -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines objects that handle input/output operations

(in-package :bio-ios)

(defparameter *meta-tool-file* "DATA/mito-muscle-R10Rev-2.dat")
(defparameter *base-coef* "1")

(defclass Reaction-String-Analyser ()
  ((reaction-string
    :accessor reaction-string
    :initform ""
    :initarg :reaction-string
    :documentation "String represents a reaction contains name, reactants and products")
   (reaction-name
    :accessor reaction-name
    :initform ""
    :initarg :reaction-name
    :documentation "String represents the name of a reaction")
   (reaction-reactants
    :accessor reaction-reactants
    :initform ""
    :initarg :reaction-reactants
    :documentation "A list contains the reactants of a reaction")
   (reaction-products
    :accessor reaction-products
    :initform ""
    :initarg :reaction-products
    :documentation "A list contains the products of a reaction")
   (number-of-reactants
    :reader number-of-reactants
    :documentation "Number of reactants")
   (number-of-products
    :reader number-of-products
    :documentation "Number of products")
   (base-coef
    :reader base-coef
    :initform *base-coef*
    :documentation "Default base coefficent of a chemical compound in reaction. The default is 1.")))

(defclass Biological-Data-Analyser ()
  ((file-name
    :accessor file-name
    :initarg :file-name
    :initform *meta-tool-file*
    :documentation "The name of the biological data file")
   (list-of-reactions
    :accessor list-of-reactions
    :initform '()
    :initarg :list-of-reactions
    :documentation "List of reactions")))

;;; This part defines the local functions using in the bio-ios package
(defun list-remove-if (P L)
  "Return a list containing all elements of list L
   except for those satisfying predicate P."
  (if (null L)
      nil
      (if (funcall P (first L))
          (list-remove-if P (rest L))
          (cons (first L) (list-remove-if P (rest L))))))

(defun list-find-equal-sign (L)
  (loop for i from 0 to (1- (length L))
     when (equal (nth i L) "=") do
       (return-from list-find-equal-sign i))
  -1)

(defun list-copy-at (start end source)
  (if (null source)
      nil
      
      (if (equal start end)
          (cons (nth start source) '())
          (cons (nth start source)
                (list-copy-at (1+ start) end source)))))

(defun list-insert-before (val lst)
  "Insert val before an element of the lst if it has no any val at previous.
   ex: ('a '2 'b 'c) > ('1 'a '2 'b '1 'c)"
  (when (eql lst nil)
    (return-from list-insert-before nil))

  (when (not (numberp (read-from-string (car lst))))
    (setf lst (cons val lst)))
  
  (let ((pre-status "")
        (result nil))
      
    (dolist (e lst)
      (if (numberp (read-from-string e))
          (setf pre-status "so")
          (progn
            (when (string= pre-status "chuoi")
              (setf result (append result (list val))))
            (setf pre-status "chuoi")))
      (setf result (append result (list e))))
    result))

;;; This part defines the methods of the Biological-Reaction-Operator class
(defgeneric make-reaction-string-analyser (reaction-string &optional reaction-name))

(defmethod make-reaction-string-analyser (reaction-string &optional (reaction-name "R"))
  (make-instance 'reaction-string-analyser
                 :reaction-string reaction-string :reaction-name reaction-name))

(defgeneric trim-reaction-string (Reaction-String-Analyser)
  (:documentation "Trim  all spaces and tabs place at the begin
and end of the reaction-string of the reaction-string-analyser"))

(defmethod trim-reaction-string ((analyser reaction-string-analyser))
  (let ((s (reaction-string analyser)))
    (setf s (string-trim '(#\Space #\Tab) s)
          (reaction-string analyser) s)))

(defgeneric split-components-of-reaction-string (Reaction-String-Analyser)
  (:documentation "Split components of a reaction string after trimming it all.
The reaction string is a sequence of characters such as : + . etc.
We remove these characters. The subsequent is a list of strings."))

(defmethod split-components-of-reaction-string ((analyser Reaction-String-Analyser))
  (trim-reaction-string analyser)
  ;; ex: This is an example of a reaction "R6i : Pyr + CO2_ext + ATP = OAA + Pi_M + ADP"
  (let* ((X (reaction-string analyser))
         (tokens (cl-ppcre:split " +" X))
         (name (car tokens))
         (input nil)
         (output nil))

    ;;remove +, : and . sign in tokens
    (setf tokens (list-remove-if #'(lambda (s) (equal s ":")) tokens)
          tokens (list-remove-if #'(lambda (s) (equal s "+")) tokens)
          tokens (list-remove-if #'(lambda (s) (equal s ".")) tokens))
    ;;remove the coefficents of molecules,
    ;;more details: http://groups.google.com/group/comp.lang.lisp/
    ;;browse_thread/thread/6d37581fb3088b38#
    ;;(remove-if #'(lambda (str) (numberp (read-from-string str)))
    ;;'("a" "23" "b" "12" "c" "1.5")) 
    ;;(setq tokens (remove-if #'(lambda (x) (parse-integer x :junk-allowed t)) tokens))
    
    ;;get the input of the reaction
    (setf input (list-copy-at 1
                              (- (list-find-equal-sign tokens) 1) tokens)
          ;;get the output of the reaction
          output (list-copy-at (+ (list-find-equal-sign tokens) 1)
                               (1- (length tokens)) tokens))
    
    ;;set or change the coefficents of molecules to handle easily later
    (setf input (list-insert-before (base-coef analyser) input)
          output (list-insert-before (base-coef analyser) output))

    ;; update the components of analyser
    (setf (reaction-name analyser) name
          (reaction-reactants analyser) input
          (reaction-products analyser) output)))

(defmethod initialize-instance :after ((analyser Reaction-String-Analyser) &key)
  (split-components-of-reaction-string analyser)
  (setf (slot-value analyser 'number-of-reactants) (/ (length (reaction-reactants analyser)) 2)
        (slot-value analyser 'number-of-products) (/ (length (reaction-products analyser)) 2)))

;;; This part defines the methods of the Biological-Data-Analyser class
(defgeneric make-biological-data-analyser (&optional file-name lst))

(defmethod make-biological-data-analyser (&optional file-name lst)
  (make-instance 'Biological-Data-Analyser :file-name file-name :list-of-reactions lst))

(defgeneric load-reactions (Biological-Data-Analyser))

(defmethod load-reactions ((data-analyser Biological-Data-Analyser))
  (with-open-file (stream (file-name data-analyser)) 
    (format t "~%Start loading file...")
    (let ((list-of-reactions '()))
      (do ((line (read-line stream)
                 (read-line stream nil 'eof)))
          ((string= (string-trim '(#\space) line) "-CAT")
           (format t "~%and skip some lines...")))

      (do ((line (read-line stream)
                 (read-line stream nil 'eof)))
          ((eq line 'eof) "Reached end of file.")

        (when (plusp (length line))
          (push (make-reaction-string-analyser line) list-of-reactions)))

      (setf (list-of-reactions data-analyser) list-of-reactions)))
  t)
