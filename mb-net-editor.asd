;;; -*- Mode: Lisp; Package: ASDF -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This file defines metabolic network editor

(in-package :asdf)

(defsystem :mb-net-editor
  :name "MBNET-EDITOR"
  :author "Nguyen Vu Ngoc Tung"
  :license "Public Domain"
  :serial t
  :depends-on (:cl-ppcre :lispbuilder-sdl :cl-opengl :lispbuilder-sdl-ttf)
  :components ((:file "packages")
               (:file "cl-object-3d"
                      :depends-on ("packages"))
               (:file "bio-application-main"
                      :depends-on ("packages"))
               (:file "bio-application-test"
                      :depends-on ("packages"))
               (:file "bio-data-model"
                      :depends-on ("packages"))
               (:file "bio-graph-model"
                      :depends-on ("packages"))
               (:file "bio-graphical-view"
                      :depends-on ("packages"))
               (:file "bio-graphical-controller"
                      :depends-on ("packages"))
               (:file "bio-io-system"
                      :depends-on ("packages"))))
