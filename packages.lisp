(defpackage :common-lisp-object-3d ; Build 3D objects interact to OpenGL
  (:nicknames :object-3d)
  (:use :common-lisp)
  (:export
   ;; constants
   #:+line-head-none+ #:+line-head-arrow+ #:+line-head-box+
   #:+PI+ #:*max-value* #:+DEG2RAD+ #:+RAD2DEG+
   ;; Utilities functions
   #:square #:c2float #:d2float #:my-random
   ;; Classes definitions
   #:Screen #:Matrix #:Color3D #:Point3D #:Shape3D #:G3DElement
   #:Cube #:Sphere #:Line3D #:Arrow3D #:Text3D
   #:SphereBB #:CubeBB #:BoundingBox3D
   ;; Slots
   #:color #:selected
   #:objects #:width #:height #:depth
   #:center #:size
   #:source #:target #:style
   #:action #:tex #:font
   #:fvals #:elements
   ;; Matrix
   #:make-matrix #:make-matrix-with
   #:set-values #:set-values-with
   #:set-identity
   ;; GLOperator
   #:set-draw-color
   ;; Point3D
   #:x #:y #:z #:make-point3d #:display-point-3d #:randomize-point3d
   #:graph #:history #:whiteboard #:content #:shapes #:list-graphical-object
   #:draw-node-to-screen
   #:draw-edge-to-screen
   #:refresh #:set-color
   ;; The functions and methods deal with type of vector 3D (as usual Point3D and Vector3D have some similar methods)
   #:copy #:clone #:length-of #:dot-product #:angle #:multiply #:multiply-to
   #:add #:add-to #:substract #:substract-from #:align-with #:distance #:cross-product #:null-point
   #:normalise-point #:normalise-point #:normalise-vector
   #:set-coordinates-from
   ;; Color3D
   #:make-color3d #:make-screen #:make-opengl-visualisation #:make-cube #:make-sphere #:make-line3d
   #:initialise-opengl #:rotate-object #:move-object #:select-object #:turn-on/off #:visualise
   #:window-size-callback
   #:key-callback
   #:*view-rotx* #:*view-roty* #:*view-rotz*
   #:draw-sphere #:draw-cube #:draw-triangle #:draw-line3d  #:draw-arrow3d #:draw-text3d
   #:add-element #:remove-element
   #:draw #:move))

(defpackage :biological-data-model ; Build the data model: biological data
  (:nicknames :bio-data)
  (:use :common-lisp :object-3d)
  (:export
   #:Biological-Process
   #:Biological-Reaction
   #:Biological-Pathway
   #:Biological-Molecule
   #:Biological-Metabolite
   #:Biological-Enzyme
   #:Metabolic-Network
   ;; Slots and fields
   #:process-name #:process-position
   #:reactants #:products #:enzyme
   #:reactions #:molecule-name #:molecule-position
   #:biological-processes #:biological-objects
   ;; functions and methods
   #:insert-biological-process
   #:remove-biological-process
   #:display-biological-process
   #:insert-molecule
   #:insert-process
   ;; constructor functions
   #:make-biological-pathway
   #:make-biological-reaction
   #:make-biological-molecule
   #:make-metabolic-network))

(defpackage :biological-io-system ; Handle input and output operations
  (:nicknames :bio-ios)
  (:use :common-lisp :bio-data)
  (:export
   ;; Classes definitions
   #:Reaction-String-Analyser
   #:Biological-Data-Analyser
   ;; Slots and methods of the Reaction-String-Analyser class
   #:reaction-string #:reaction-name
   #:reaction-reactants #:reaction-products
   #:number-of-reactants #:number-of-products #:base-coef
   #:make-reaction-string-analyser
   #:trim-reaction-string
   #:split-components-of-reaction-string
   ;; Slots and methods of the Biological-Data-Analyser class
   #:file-name #:list-of-reactions
   #:make-biological-data-analyser
   #:load-reactions
   ;; Local functions
   #:list-remove-if
   #:list-find-equal-sign
   #:list-copy-at
   #:list-insert-before
   ;; Global constants and variables declaration
   #:*meta-tool-file* #:*base-coef*))

(defpackage :biological-graph-model ; Build the graph model
  (:nicknames :bio-graph)
  (:use :common-lisp :bio-data :bio-ios :object-3d)
  (:export
   ;; Global constants and variables
   #:*scene*
   ;; Classes definitions
   #:Graph-Element
   #:Graph-Node #:Graph-Reaction-Node #:Graph-Molecule-Node
   #:Graph-Edge #:Graph-Metabolic
   #:node-position #:node-label #:make-graph-nodet
   #:make-graph-reaction-node #:make-graph-molecule-node
   #:source-node #:target-node #:edge-label #:make-graph-edge
   ;; Slots and methods of the Graph-Metabolic class
   #:nodes #:edges #:g3d-element
   #:make-graph-metabolic
   #:add-node-to-graph-metabolic #:add-nodes-to-graph-metabolic
   #:add-edge-to-graph-metabolic #:add-edges-to-graph-metabolic
   #:draw-object
   #:load-and-build-graph-metabolic
   #:random-layout #:circular-layout #:linear-layout
   #:update-views #:update-graph-metabolic
   #:get-reaction-nodes #:get-molecule-nodes
   #:get-color-reaction-node #:get-color-molecule-node #:get-color-graph-edge))

(defpackage :biological-graphical-view  ; Display graphs in OpenGL
  (:nicknames :bio-view)
  (:use :common-lisp :bio-data :bio-ios :bio-graph :object-3d)
  (:export
   ;; The global constants and variables
   #:*xspeed* #:*yspeed* #:*zspeed* #:*m-network* #:*m-graph*
   ;; The main functions to create and run the application
   #:draw-axis
   #:load-meta-tool-file
   #:initialise-positions
   #:apply-layout-paradigm
   #:draw-metabolic-network
   #:visualise-metabolic-network
   ;; The functions handle with OpenGL and SDL
   #:handle-keypress-cabine #:zoom0 #:resize0 #:initgl0 #:event-loop0 #:init-sdl #:reset-camera
   ;; To test
   #:test #:gl-draw))

(defpackage :biological-graphical-controller ; Control operations on the graph drawing in OpenGL
  (:nicknames :bio-controller)
  (:use :common-lisp :bio-data :bio-graph :bio-view)
  (:export
   #:GraphicViewController #:model #:view
   #:update-model #:act-event
   #:GraphicView3DController
   #:GraphicViewTextContrller))

(defpackage :biological-application-test ; Servce for testing all functionalites in the application
  (:nicknames :bio-test)
  (:use :common-lisp :bio-data :bio-graph :bio-view
        :bio-controller :bio-ios :object-3d)
  (:export
   #:graph-gui #:graph-line #:draw-one-line #:test-draw-lines-3d
   #:test-graph-line #:test-1 #:test-2
   #:test-graphic-network-1
   #:test-graphic-network-2
   #:test-draw-graphic-network
   #:create-and-display-metabolic-networks-1
   #:create-and-display-metabolic-networks-2
   #:test-graphic-view-1
   #:start
   #:*pressed-button*))

(defpackage :metabolic-network-editor ; Load data, initialize postions, apply layout paradigm and draw the graph
  (:nicknames :bio-app)
  (:use :common-lisp :bio-data :bio-graph
        :bio-view :bio-controller :bio-ios :object-3d) 
  (:export
   #:reset-app
   #:run-non-ui
   #:run-with-ui))
