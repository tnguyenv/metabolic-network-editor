;;; -*- Mode: Lisp; Package: BIO-APP -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines the main function

(in-package :metabolic-network-editor)

(defun run-non-ui ()
  "Run the application without interface"
  ;; from bio-ios
  (load-meta-tool-file)
  ;; from bio-model
  (initialise-positions)
  ;; from bio-controller
  (apply-layout-paradigm)
  ;; from bio-view
  (visualise-metabolic-network))

(defun run-with-ui ()
  "Run the application with Graphic User Interface allows user to choose functions."
  (load-ui))

(defun reset-app ()
  (reset-camera))
