;;; -*- Mode: Lisp; Package: BIO-GRAPH -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines objects that built biological graphs

(in-package :bio-graph)

(defparameter *scene* nil)

(defclass Graph-Element ()
  ((g3d-element :accessor g3d-element :initarg :g3d-element
                :documentation "This is a list of instances of the G3DElement class. The list can contain one element.")))

(defclass Graph-Node (Graph-Element)
  ((node-position :accessor node-position :initarg :node-position
                  :initform (error "Must supply a 3D coordinate")
                  :documentation "The position of node on the display")
   (node-label    :accessor node-label :initarg :node-label
                  :initform "node" :documentation "The label of node")))

(defclass Graph-Reaction-Node (Graph-Node)
  ())

(defclass Graph-Molecule-Node (Graph-Node)
  ())

(defclass Graph-Edge (Graph-Element)
  ((source-node :accessor source-node :initarg :source-node
                :documentation "The source node")
   (target-node :accessor target-node :initarg :target-node
                :documentation "The target node")
   (edge-label  :accessor edge-label  :initarg :edge-label  :initform "edge"
                :documentation "The label of edge, such as weight, cost...")))

(defclass Graph-Metabolic ()
  ((nodes :accessor nodes :initarg :nodes :initform '() :documentation "The list of nodes")
   (edges :accessor edges :initarg :edges :initform '() :documentation "The list of edges"))
  (:default-initargs :nodes '() :edges '()))

;;; Implements the methods of the Graphic-Node class
(defgeneric make-graph-node (g3d-element node-position node-label))
(defmethod make-graph-node (g3d-element node-position node-label) 
  (make-instance 'Graph-Node :g3d-element g3d-element :node-position node-position :node-label node-label))

(defgeneric make-graph-reaction-node (g3d-element node-position node-label))
(defmethod make-graph-reaction-node (g3d-element node-position node-label)
  (make-instance 'Graph-Reaction-Node :g3d-element g3d-element :node-position node-position :node-label node-label))

(defgeneric make-graph-molecule-node (g3d-element node-position node-label))
(defmethod make-graph-molecule-node (g3d-element node-position node-label)
  (make-instance 'Graph-Molecule-Node :g3d-element g3d-element :node-position node-position :node-label node-label))

(defmethod initialize-instance :after ((graph-reaction-node Graph-Reaction-Node) &key)
  (setf (slot-value graph-reaction-node 'g3d-element)
        (list (make-cube :size 0.2f0 :center (node-position graph-reaction-node)))))
  
(defmethod initialize-instance :after ((graph-molecule-node Graph-Molecule-Node) &key)
  (setf (slot-value graph-molecule-node 'g3d-element)
        (list (make-sphere :size 0.25f0 :center (node-position graph-molecule-node)))))

(defgeneric get-color-g3d-element (Graph-Node))
(defmethod get-color-g3d-element ((graph-node Graph-Reaction-Node))
  (color (first (g3d-element graph-node))))
(defmethod get-color-g3d-element ((graph-node Graph-Molecule-Node))
  (color (first (g3d-element graph-node))))

;;;; Implements the methods of the Graphic-Edge class
(defgeneric make-graph-edge (g3d-element source-node target-node edge-label)
  (:documentation "Create a new edge with the source, target and label which passed"))
(defmethod make-graph-edge (g3d-element source-node target-node edge-label)
  (make-instance 'Graph-Edge :g3d-element g3d-element
                 :source-node source-node :target-node target-node :edge-label edge-label))

(defmethod initialize-instance :after ((graph-edge Graph-Edge) &key)
  (with-slots (source-node target-node g3d-element edge-label) graph-edge
    (setf g3d-element (append (g3d-element source-node)
                              (g3d-element target-node)
                              (list (make-line3d :color (make-color3d 0.5f0 0.6f0 1.0f0)))))))

(defgeneric get-color-graph-edge (graph-edge))
(defmethod get-color-graph-edge ((edge Graph-Edge))
  (color (third (g3d-element edge))))

;;;; Implements the methods of the Graph-Metabolic class
(defgeneric make-graph-metabolic (nodes edges)
  (:documentation "Initialize and create a new graph"))
(defmethod make-graph-metabolic (nodes edges)
  (make-instance 'graph-metabolic :nodes nodes :edges edges))

(defgeneric exist (node the-list-nodes)
  (:documentation "Check existation of node in the-list-nodes"))
(defmethod exist ((node Graph-Node) the-list-nodes)
  (count-if #'(lambda (x) (string= (node-label x) (node-label node))) the-list-nodes))
(defmethod exist ((edge Graph-Edge) the-list-edges)
  (count-if #'(lambda (x) (and (string= (node-label (source-node x)) (node-label (source-node edge)))
                               (string= (node-label (target-node x)) (node-label (target-node edge))))) the-list-edges))
(defmethod exist (the-label the-list-nodes)
  (dolist (node the-list-nodes)
    (when (string= the-label (node-label node))
      (return-from exist node)))
  nil)

(defgeneric add-node-to-graph-metabolic (node graph)
  (:documentation "Add the node to the graph"))                 
(defmethod add-node-to-graph-metabolic ((node Graph-Node) (graph Graph-Metabolic))
  (if (zerop (exist node (nodes graph)))
      (push node (nodes graph))
      (nodes graph)))

(defgeneric add-edge-to-graph-metabolic (edge graph)
  (:documentation "Add the edge to the graph"))
(defmethod add-edge-to-graph-metabolic ((edge Graph-Edge) (graph Graph-Metabolic))
  (if (zerop (exist edge (edges graph)))
      (push edge (edges graph))
      (edges graph)))

(defgeneric get-molecule-nodes (graph-metabolic))
(defmethod get-molecule-nodes ((graph Graph-Metabolic))
  (let ((lst nil))
    (dolist (node (nodes graph))
      (when (eql 'Graph-Molecule-Node (type-of node))
        (push node lst)))
    lst))

(defgeneric get-rection-nodes (graph-metabolic))
(defmethod get-reaction-nodes ((graph Graph-Metabolic))
  (let ((lst nil))
    (dolist (node (nodes graph))
      (when (eql 'Graph-Reaction-Node (type-of node))
        (push node lst)))
    lst))

(defgeneric get-color-reaction-node (graph-metabolic))
(defmethod get-color-reaction-node ((graph Graph-Metabolic))
  (let ((rnodes (get-reaction-nodes graph)))
    (if (eql rnodes nil)
        nil
        (get-color-g3d-element (first rnodes)))))

(defgeneric get-color-molecule-node (graph-metabolic))
(defmethod get-color-molecule-node ((graph Graph-Metabolic))
  (let ((mnodes (get-molecule-nodes graph)))
    (if (eql mnodes nil)
        nil
        (get-color-g3d-element (first mnodes)))))

(defmethod get-color-graph-edge ((graph Graph-Metabolic))
  (get-color-graph-edge (first (edges graph))))

(defgeneric get-number-nodes (graph)
  (:documentation "Get the number of the nodes"))
(defmethod get-number-nodes ((graph Graph-Metabolic))
  (length (nodes graph)))

(defgeneric get-number-edges (graph)
  (:documentation "Get the number of the edges"))
(defmethod get-number-edges ((graph Graph-Metabolic))
  (length (edges graph)))

;;; This part defines the draw object method
(defgeneric draw-object (object)
  (:documentation "Draw an object on the screen"))
(defmethod draw-object ((object Graph-Element))
  (mapc #'draw (g3d-element object)))
(defmethod draw-object ((node Graph-Node))
  (when (null (node-position node))
    (format t "~%Position is null")
    (return-from draw-object nil))
  (mapc #'draw (g3d-element node)))
(defmethod draw-object ((node Graph-Reaction-Node))
  (mapc #'draw (g3d-element node)))
(defmethod draw-object ((node Graph-Molecule-Node))
  (mapc #'draw (g3d-element node)))
(defmethod draw-object ((edge Graph-Edge))
  (draw-object (source-node edge))
  (draw-object (target-node edge))
  (draw (first (g3d-element edge))))
(defmethod draw-object ((graph Graph-Metabolic))
  (setf *scene* nil)
  (dolist (node (nodes graph))
    (push (first (g3d-element node)) *scene*))
  (dolist (edge (edges graph))
    (push (third (g3d-element edge)) *scene*)))
;;; This part defines the move object method

;;; This part update the views of the graph
(defmethod update-views ((graph-metabolic Graph-Metabolic) (edge-color Color3D) (molecule-node-color Color3D))
  (dolist (node (nodes graph-metabolic))
    (when (eql 'Graph-Molecule-Node (type-of node))
      (setf (color (first (g3d-element node))) molecule-node-color))
    (setf (center (first (g3d-element node))) (node-position node)))
  (dolist (edge (edges graph-metabolic))
    (setf (source (third (g3d-element edge))) (node-position (source-node edge))
          (target (third (g3d-element edge))) (node-position (target-node edge))          
          (color (third (g3d-element edge))) edge-color)))
(defmethod update-graph-metabolic ((graph Graph-Metabolic))
  ())
;;; This part defines graph layout algorithms
;; This method generates randomly positions
(defgeneric random-layout (graph)
  (:documentation "Random the positions of the graph's nodes"))
(defmethod random-layout ((graph Graph-Metabolic))
  (dolist (node (nodes graph))
    (setf (node-position node) (randomize-point3d)))
  (nodes graph))

(defgeneric linear-layout (list-of-nodes start-point direction space xmin xmax)
  (:documentation "Linear Layout"))
(defmethod linear-layout (list-of-nodes (start-point Point3D) direction space xmin xmax)
  (let* ((dx 0) (dy 0) (dz 0))
    (cond ((string= direction "X")
           (setf dx space))
          
          ((string= direction "Y")
           (setf dy space))
          
          (t
           (setf dz space)))

    (dotimes (i (length list-of-nodes))  
      (setf (x (node-position (nth i list-of-nodes))) (+ (x start-point) (* i dx))
            (y (node-position (nth i list-of-nodes))) (+ (y start-point) (* i dy))
            (z (node-position (nth i list-of-nodes))) (+ (z start-point) (* i dz)))
      )))

(defgeneric circular-layout (list-of-nodes radius)
  (:documentation "Circular Layout"))
(defmethod circular-layout (list-of-nodes radius)
  (let* ((x 0) (y 0) (angle 0) (interval 0))
    
    (setf interval (/ (* 2.0 3.14159265) (length list-of-nodes)))
    (dotimes (id (length list-of-nodes))
      (setf angle (* id interval)
            x (* radius (cos angle))
            y (* radius (sin angle)))
      (set-coordinates-from
       (node-position (nth id list-of-nodes)) x y))))

(defgeneric sphere-layout (graph)
  (:documentation "Sphere Layout"))

(defgeneric load-and-build-graph-metabolic (file-name)
  (:documentation "Load and build the metabolic graph from a meta tool file"))
(defmethod load-and-build-graph-metabolic (file-name)
  (let* ((graph (make-graph-metabolic nil nil))
         (network (make-metabolic-network nil nil))
         (data-analyser (make-biological-data-analyser file-name))
         (list-reactions)
         (r) (flag nil)
         (edge)
         (reaction-node nil)
         (molecule-node nil))
    (load-reactions data-analyser)    
    (setf list-reactions (list-of-reactions data-analyser))

    (loop while (not (eql list-reactions nil)) do
         (setf r (pop list-reactions)
               reaction-node (make-graph-reaction-node nil (make-point3d) (reaction-name r))
               ;; add reaction-node into the graph
               (nodes graph) (add-node-to-graph-metabolic reaction-node graph))
         (loop for (coef mole) on (reaction-reactants r) by #'cddr until (null mole) do
              (setf molecule-node (exist mole (nodes graph)))
              (when (eql molecule-node nil)     
                (setf molecule-node (make-graph-molecule-node nil (make-point3d) mole)
                      flag t))
              (setf edge (make-graph-edge nil molecule-node reaction-node
                                          (concatenate 'string mole (reaction-name r)))
                    ;; add node and edge into the graph
                    (edges graph) (add-edge-to-graph-metabolic edge graph))
              (when flag
                    (setf (nodes graph) (add-node-to-graph-metabolic molecule-node graph))))

         (loop for (coef mole) on (reaction-products r) by #'cddr until (null mole) do
              (setf molecule-node (exist mole (nodes graph)))
              (when (eql molecule-node nil)
                (setf molecule-node (make-graph-molecule-node nil (make-point3d) mole)
                      flag t))
              (setf edge (make-graph-edge nil reaction-node molecule-node
                                          (concatenate 'string (reaction-name r) mole))
                    ;; add node and edge into the graph
                    (edges graph) (add-edge-to-graph-metabolic edge graph))
              (when flag
                    (setf (nodes graph) (add-node-to-graph-metabolic molecule-node graph)))))
    graph))
