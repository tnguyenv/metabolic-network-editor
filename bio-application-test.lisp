(in-package :bio-test)

(defparameter *x* 0.0f0)
(defparameter *y* 0.0f0)
(defparameter *z* 0.0f0)

; Delta for keyboard scroll
(defparameter *dx* 0.5f0)
(defparameter *dy* 0.5f0)
(defparameter *dz* 0.5f0)
; Delta for mouse scroll
(defparameter *dxm* 0.01f0)
(defparameter *dym* -0.01f0) ; negative for invert mouse
(defparameter *dzm* 0.01f0)
;Rotational position around the x and y axes
(defparameter *xr* 0f0)
(defparameter *yr* 0f0)
; Delta for mouse rotate
(defparameter *dxr* 0.5f0)
(defparameter *dyr* 0.5f0)
(defparameter *new-target* nil)
;;; SDL_MouseDown
(defparameter *projection-base* (sgum:allocate-foreign-object gl:float 16))
(defparameter *proj-presel* (sgum:allocate-foreign-object gl:double 16))

(defconstant +selection-buffer-len+ 512)
(defconstant +feed-buffer-size+ 4096)

; Current mouse position (relative to the current window
(defparameter *mx* 0)
(defparameter *my* 0)
(defparameter *mz* 0)

; Mouse buttons
(defparameter *mb* (make-array 7 :initial-element nil))
(defparameter *mshape* (make-array 5))
(defparameter *original-color* (make-color3d 1.0f0 0.0f0 0.0f0))
(defparameter *selected-object* nil)
(defparameter *pressed-button* 1)
(defparameter *mvm* (sgum:allocate-foreign-object gl:double 16))

(defun gl-render-scene (surface)
  (declare (ignorable surface))
  (gl:clear-color 0f0 0f0 0f0 0f0)
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))
  (gl:matrix-mode gl:+modelview+)
  (gl:push-matrix)
  (gl:load-identity)
  (glu:look-at 5d0 5d0 5d0
               0d0 0d0 0d0
               0d0 1d0 0d0)
  ;; Translate the whole scene out and into view
  (gl:translate-f *x* *y* *z*)
  (draw-axis 10.f0)
  (gl:get-doublev gl:+modelview-matrix+ *mvm*)
  ;; Initialize the names stack
  (gl:init-names)
  (gl:push-name 0)
  ;; Ve hinh
  (loop for i from 1 to 4 do
    (gl:color-3f 1.0f0 0.0f0 0.0f0)
    (gl:load-name i)
    (gl:pass-through (c2float i))
    (draw (aref *mshape* i)))
  ;; Ve them mot vai hinh linh tinh, vd ve mui ten
  (draw-arrow-3d)
  ;; Restore the matrix state (model matrix)
  (gl:pop-matrix)
  (gl:flush)
  (sdl:gl-swap-buffers)
  t)

(defun handle-selection (hits buffer)
  ;;(format t "~%~A hits:" hits)
  (when (> hits 0)
    (dotimes (i hits)
      ;;       (format t "~%Number: ~A ~%Min Z: ~A ~%Max Z: ~A ~%Name on stack: ~A"
      ;;               (sgum:deref-array buffer gl:int (* i 4))
      ;;               (sgum:deref-array buffer gl:int (+ (* i 4) 1))
      ;;               (sgum:deref-array buffer gl:int (+ (* i 4) 2))
      ;;               (sgum:deref-array buffer gl:int (+ (* i 4) 3)))
      (let* ((choice (sgum:deref-array buffer gl:int (+ (* i 4) 3)))
             (sp (aref *mshape* choice)))
        (when (not (eql sp nil))
          ;; sp.selected = !sp.selected
          (setf (selected sp) (not (selected sp)))
          (if (selected sp)
              (progn
                (format t "~%Point: ")
                (display-point-3d (center sp))
                ;; Inverse its color
                (setf (color sp) (make-color3d 1f0 0f0 1f0))            
                (setf *selected-object* sp))
              ;; else, restore the old color
              (setf (color sp) *original-color*))))
      (return-from handle-selection t)))
  (setf *selected-object* nil))

(defun handle-mouse-down (surface x y)
  ;;; Select objects
  (let ((buffer (sgum:allocate-foreign-object gl:uint +selection-buffer-len+))
        (viewport (sgum:allocate-foreign-object gl:int 4))
        (x3d) (y3d) (z3d)
        (dx (sgum:allocate-foreign-object gl:double 1))
        (dy (sgum:allocate-foreign-object gl:double 1))
        (dz (sgum:allocate-foreign-object gl:double 1))
        (modelview *mvm*) ;;(sgum:allocate-foreign-object gl:double 16))
        (projection (sgum:allocate-foreign-object gl:double 16))
        (depth (sgum:allocate-foreign-object gl:float 1))
        (winx) (winy)
        (winz) (realy)
        (hits 0))
    ;;; Start picking
    ;; Set up selection buffer
    (gl:select-buffer +selection-buffer-len+ buffer)
    ;; Get the viewport
    (gl:get-integerv gl:+viewport+ viewport)
    ;;(gl:get-doublev gl:+modelview-matrix+ modelview)
    (gl:get-doublev gl:+projection-matrix+ projection)
    ;;(format t "~%Viewport: ~A" (loop for n from 0 to 3 collect (sgum:deref-array viewport gl:int n)))
    ;; Switch to projection and save the matrix
    (gl:matrix-mode gl:+projection+)
    (gl:read-pixels x (- (sgum:deref-array viewport gl:int 3) y)
                    1 1 gl:+depth-component+ gl:+float+ depth)
    (setf winx (d2float x)
          winy (d2float y))
    (setf realy (- (sgum:deref-array viewport gl:int 3) winy 1))
    (setf winz (d2float (sgum:deref-array depth gl:float 1)))
    (glu:unproject winx realy winz
                   modelview projection viewport
                   dx dy dz)
    (setf x3d (sgum:deref-pointer dx gl:double)
          y3d (sgum:deref-pointer dy gl:double)
          z3d (sgum:deref-pointer dz gl:double))
    (format t "~%Object: (~A ~A ~A)" x3d y3d z3d)
    (setf *new-target* (make-point3d (c2float x3d)
                                     (c2float y3d)
                                     (c2float z3d)))
    (setf *dzm* z3d)
    (gl:push-matrix)
    ;; Change render mode
    (gl:render-mode gl:+select+)
    ;; Initialize the names stack
    (gl:init-names)
    (gl:push-name 0)
    (gl:load-identity)
    (glu:pick-matrix (coerce x 'double-float)
                     (coerce y 'double-float)
                     1d0 1d0 viewport)
    ;; Apply perspective matrix
    (glu:perspective 45d0
                     (coerce
                      (/ (- (sgum:deref-array viewport gl:int 2)
                            (sgum:deref-array viewport gl:int 0))
                         (- (sgum:deref-array viewport gl:int 3)
                            (sgum:deref-array viewport gl:int 1)))
                      'double-float)
                     0.1d0 100.0d0)
    ;; Draw scene
    (gl-render-scene surface) 

    ;;; Stop picking and process the object picked, Collect the hits and display
    (setf hits (gl:render-mode gl:+render+))
    (handle-selection hits buffer)
    ;; Restore the projection matrix
    (gl:matrix-mode gl:+projection+)
    (gl:pop-matrix)
    ;; Go back to modelview for normal rendering
    (gl:matrix-mode gl:+modelview+)))

(defun handle-mouse-motion (surface x y xrel yrel)
  (declare (ignorable surface))
  (if (eql *selected-object* nil)
      ()
      (progn
        (incf (x (center *selected-object*)) (* *dxm* xrel))
        (incf (y (center *selected-object*)) (* *dym* yrel))
        ;;(setf (z (center *selected-object*)) (c2float *dzm*))
        )
      ))

(defun handle-mouse-up (surface x y)
  (declare (ignorable surface))
  ;;(display-point-3d *new-target*)
  ;;(setf (center *selected-object*) *new-target*)
)
(defun event-loop1 (surface update-fn resize-fn handle-keypress-fn bpp video-flags)
  (sdl:event-loop
   (:key-down (key)
	      (if (= key (char-code #\q))
                  (return)
                  (funcall handle-keypress-fn key)))
   (:mouse-button-up (x y button)
                     (if (eql nil button)
                         (progn
                           ;;(format t "~%Mouse button up: WHEEL (~A, ~A)" x y)
                           ;; When the user scrolls the wheel
                           (setf *pressed-button* sdl:+button-middle+)
                           (incf *z* *dz*))
                         (progn
                           ;;(format t "~%Mouse button up: ~A (~A, ~A)" button x y)
                           (cond ((string= "LEFT" button)
                                  (progn
                                    (setf (aref *mb* sdl:+button-left+) nil)
                                    (handle-mouse-up surface x y)))
                                 ((string= "MIDDLE" button)
                                  (setf (aref *mb* sdl:+button-middle+) nil))
                                 (t
                                  (setf (aref *mb* sdl:+button-right+) nil))))))
   (:mouse-button-down (x y button)
                       ;; Check any mouse button whether clicked or not
                       (if (eql nil button)
                           (progn
                             ;;(format t "~%Mouse button dn: WHEEL (~A, ~A)" x y)
                             (setf *pressed-button* sdl:+button-middle+)
                             (decf *z* *dz*))
                           (progn
                             ;;(format t "~%Mouse button dn: ~A (~A, ~A)" button x y)
                             (cond ((string= "LEFT" button)
                                    (setf *pressed-button* sdl:+button-left+)
                                    (setf (aref *mb* sdl:+button-left+) t))
                                   ((string= "MIDDLE" button)
                                    (setf *pressed-button* sdl:+button-middle+)
                                    (setf (aref *mb* sdl:+button-middle+) t))
                                   (t
                                    (setf *pressed-button* sdl:+button-right+)
                                    (setf (aref *mb* sdl:+button-right+) t)))))
                       (if (= sdl:+button-left+ *pressed-button*)
                           (handle-mouse-down surface x
                                              (- (sdl:surface-h surface) y))))
   (:mouse-motion (x y xrel yrel state)
                  ;; Set the current postion of the mouse
                  (setf *mx* x)
                  (setf *my* y)
                  ;; when the right click event happens
                  (if (aref *mb* 3)
                      (progn
                        (incf *x* (* *dxm* xrel))
                        (incf *y* (* *dym* yrel))))
                  ;; when the middle click event happens
                  (if (aref *mb* 2)
                      (progn
                        ;; Moving mouse left <-> right rotates around Y axis and vice versa
                        (incf *xr* (* *dxr* yrel))
                        (incf *yr* (* *dyr* xrel))))
                  ;; when the left click event happens: hold and drag
                  (if (aref *mb* 1)
                      (progn 
                        (handle-mouse-motion surface x y xrel yrel))))
  (:quit ()
         (return))
  (:resize (width height)
           (format t "~%Resized width = ~A height = ~A" width height)
           (sdl:free-surface surface)
           (setf surface
                 (sdl:set-video-mode width height bpp video-flags))
           (funcall resize-fn surface width height))
  (:idle ()
         (funcall update-fn surface))))

(defun start ()
  (loop for i from 1 to 4 do
       (setf (aref *mshape* i) (make-sphere)))
  (init-sdl :title "Selection and pick object up demo"
            :init-gl-fn #'initgl0
            :event-loop-fn #'event-loop1
            :update-fn #'gl-render-scene
            :resize-fn #'resize0))
