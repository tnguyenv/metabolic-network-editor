\select@language {english}
\contentsline {chapter}{Abstract}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {paragraph}{}{1}
\contentsline {chapter}{Acknowledgement}{1}
\contentsline {paragraph}{}{2}
\contentsline {paragraph}{}{2}
\contentsline {chapter}{Introduction}{2}
\contentsline {chapter}{\numberline {1}Context}{3}
\contentsline {paragraph}{}{3}
\contentsline {section}{\numberline {1.1}P\IeC {\^o}les Universitaires Fran\IeC {\c c}ais}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{4}
\contentsline {section}{\numberline {1.2}Laboratoire Bordelais de Recherche en Informatique}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{5}
\contentsline {section}{\numberline {1.3}Working group}{5}
\contentsline {section}{\numberline {1.4}Motivation}{5}
\contentsline {paragraph}{}{5}
\contentsline {section}{\numberline {1.5}Problem}{6}
\contentsline {section}{\numberline {1.6}Requirements}{6}
\contentsline {paragraph}{The primary operations consist of the application:}{6}
\contentsline {subsection}{\numberline {1.6.1}Proposal functional operations}{6}
\contentsline {subsection}{\numberline {1.6.2}Proposal non-functional operations}{7}
\contentsline {chapter}{\numberline {2}Presentation of biological networks}{8}
\contentsline {section}{\numberline {2.1}Graph Theory}{8}
\contentsline {subsection}{\numberline {2.1.1}History of Graph Theory}{8}
\contentsline {paragraph}{}{9}
\contentsline {subsection}{\numberline {2.1.2}Definitions of Graph}{9}
\contentsline {paragraph}{}{9}
\contentsline {paragraph}{}{10}
\contentsline {subsection}{\numberline {2.1.3}Graph Drawing}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {subsection}{\numberline {2.1.4}Applications of Graph}{10}
\contentsline {paragraph}{}{10}
\contentsline {paragraph}{}{10}
\contentsline {section}{\numberline {2.2}Biological Graph}{10}
\contentsline {subsection}{\numberline {2.2.1}Introduction to Bioinformatics}{11}
\contentsline {subsection}{\numberline {2.2.2}Biological Graph}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{12}
\contentsline {section}{\numberline {2.3}Graph Visualization}{12}
\contentsline {subsection}{\numberline {2.3.1}Graphviz}{13}
\contentsline {paragraph}{}{13}
\contentsline {subsection}{\numberline {2.3.2}Tulip}{13}
\contentsline {subsection}{\numberline {2.3.3}BabelGraph}{13}
\contentsline {section}{\numberline {2.4}Common Lisp Programming Language}{13}
\contentsline {paragraph}{}{14}
\contentsline {section}{\numberline {2.5}OpenGL and Bindings for Common Lisp}{14}
\contentsline {paragraph}{}{14}
\contentsline {paragraph}{}{14}
\contentsline {subsection}{\numberline {2.5.1}cl-opengl, cl-glfw and cl-sdl}{15}
\contentsline {paragraph}{}{15}
\contentsline {paragraph}{}{15}
\contentsline {subsection}{\numberline {2.5.2}Drawing text in OpenGL}{15}
\contentsline {paragraph}{}{15}
\contentsline {subsection}{\numberline {2.5.3}The Sync project}{16}
\contentsline {paragraph}{}{16}
\contentsline {chapter}{\numberline {3}Cabine}{17}
\contentsline {section}{\numberline {3.1}Model}{17}
\contentsline {paragraph}{}{17}
\contentsline {subsection}{\numberline {3.1.1}Biological Graph}{17}
\contentsline {subsection}{\numberline {3.1.2}Biological Drawing}{18}
\contentsline {paragraph}{}{19}
\contentsline {section}{\numberline {3.2}Classes}{19}
\contentsline {paragraph}{}{19}
\contentsline {paragraph}{}{19}
\contentsline {subsection}{\numberline {3.2.1}Biological Objects}{19}
\contentsline {paragraph}{}{20}
\contentsline {subsection}{\numberline {3.2.2}Graph}{20}
\contentsline {subsection}{\numberline {3.2.3}3D Objects}{20}
\contentsline {subsection}{\numberline {3.2.4}OpenGL View}{21}
\contentsline {subsection}{\numberline {3.2.5}Main Application}{21}
\contentsline {paragraph}{}{22}
\contentsline {section}{\numberline {3.3}Functionality}{22}
\contentsline {subsection}{\numberline {3.3.1}Loading the data file}{22}
\contentsline {paragraph}{}{23}
\contentsline {subsection}{\numberline {3.3.2}Creating metabolic graph}{23}
\contentsline {subsection}{\numberline {3.3.3}Drawing metabolic network}{23}
\contentsline {paragraph}{}{24}
\contentsline {subsection}{\numberline {3.3.4}Selection}{24}
\contentsline {subsection}{\numberline {3.3.5}Transformation}{24}
\contentsline {paragraph}{Definition}{24}
\contentsline {paragraph}{}{24}
\contentsline {paragraph}{Translation}{24}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{Rotation}{25}
\contentsline {paragraph}{}{25}
\contentsline {paragraph}{Zoom}{25}
\contentsline {paragraph}{}{26}
\contentsline {paragraph}{}{26}
\contentsline {subsection}{\numberline {3.3.6}Layout}{26}
\contentsline {chapter}{\numberline {4}Results}{27}
\contentsline {paragraph}{}{27}
\contentsline {section}{\numberline {4.1}Setting up the application's environment}{27}
\contentsline {section}{\numberline {4.2}Steps to run the application}{27}
\contentsline {subsection}{\numberline {4.2.1}Loading description file}{27}
\contentsline {subsection}{\numberline {4.2.2}Initializing positions}{28}
\contentsline {subsection}{\numberline {4.2.3}Drawing the graph}{29}
\contentsline {section}{\numberline {4.3}Selection}{29}
\contentsline {paragraph}{}{30}
\contentsline {paragraph}{}{30}
\contentsline {subsection}{\numberline {4.3.1}Picking}{30}
\contentsline {paragraph}{}{30}
\contentsline {subparagraph}{Render mode (GL\_RENDER)}{30}
\contentsline {subparagraph}{Render mode (GL\_SELECT)}{30}
\contentsline {subparagraph}{Render mode (GL\_FEEDBACK)}{30}
\contentsline {paragraph}{}{30}
\contentsline {paragraph}{}{31}
\contentsline {subsection}{\numberline {4.3.2}Highlight}{31}
\contentsline {section}{\numberline {4.4}Transformation}{31}
\contentsline {subsection}{\numberline {4.4.1}Translation}{31}
\contentsline {subsection}{\numberline {4.4.2}Rotation}{32}
\contentsline {subsection}{\numberline {4.4.3}Zoom}{32}
\contentsline {chapter}{Conclusion and future work}{33}
\contentsline {paragraph}{Storing biological data in computer's memory}{33}
\contentsline {paragraph}{Defining biological graphs}{33}
\contentsline {paragraph}{Visualizing biological graphs}{33}
\contentsline {paragraph}{Interacting with biological graphs}{33}
\contentsline {paragraph}{}{33}
\contentsline {paragraph}{Finishing the module of show labels}{33}
\contentsline {paragraph}{Extending operations}{34}
\contentsline {paragraph}{Applying layout algorithms}{34}
\contentsline {paragraph}{Integrating GUI}{34}
\contentsline {chapter}{Bibliography}{36}
\contentsline {chapter}{Appendix}{36}
\contentsline {chapter}{\numberline {A}UML Diagrams}{37}
\contentsline {chapter}{\numberline {B}Implementation of the Classes}{38}
\contentsline {chapter}{\numberline {C}User's Guides}{77}
\contentsline {section}{\numberline {C.1}Introduction}{77}
\contentsline {section}{\numberline {C.2}Functionalities}{77}
\contentsline {section}{\numberline {C.3}Tutorials}{77}
\contentsline {chapter}{\numberline {D}Developer's Guides}{78}
\contentsline {section}{\numberline {D.1}Introduction}{78}
\contentsline {paragraph}{}{78}
\contentsline {section}{\numberline {D.2}Installation}{78}
\contentsline {paragraph}{}{78}
\contentsline {paragraph}{}{78}
\contentsline {section}{\numberline {D.3}Functions}{79}
