\documentclass[red]{beamer}
\usepackage[latin1,utf8]{inputenc}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage{geometry}
\usepackage[english]{babel}
\usetheme{Warsaw}
\title[CABINE: Biological Network Editor]{Internship Presentation\\Editor to draw Biological Graph in 3D}
\author{NGUYEN Vu Ngoc TUNG}
\institute[PUF and Bordeaux 1]{Professional Master of Software Engineering (2007-2009)\\Pôles Universitaires Français - Ho Chi Minh City, Vietnam\\LaBRI - University of Bordeaux 1\\Supervisor: Marie Beurton-AIMAR}
\date{}
%Page numbering
\newcommand*\oldmacro{}%
\let\oldmacro\insertshorttitle%
\renewcommand*\insertshorttitle{%
 \oldmacro\hfill%
 \insertframenumber\,/\,\inserttotalframenumber}
 
 % Make one image take up the entire slide area, including borders, in beamer.:
% centered/centred full-screen image, no title:
% This uses the entire whole screen
\newenvironment{changemargin}[2]{%
\begin{list}{}{%
\setlength{\topsep}{0pt}%
\setlength{\leftmargin}{#1}%
\setlength{\rightmargin}{#2}%
\setlength{\listparindent}{\parindent}%
\setlength{\itemindent}{\parindent}%
\setlength{\parsep}{\parskip}%
}%
\item[]}{\end{list}}

% Make one image take up the entire slide content area in beamer,.:
% centered/centred full-screen image, with title:
% This uses the whole screen except for the 1cm border around it
% all. 128x96mm
\newcommand{\titledFrameImage}[2]{
\begin{frame}{#1}
%\begin{changemargin}{-1cm}{-1cm}
\begin{center}
\includegraphics[width=108mm,height=\textheight,keepaspectratio]{#2}
\end{center}
%\end{changemargin}
\end{frame}
}
% Make one image take up the entire slide content area in beamer.:
% centered/centred full-screen image, no title:
% This uses the whole screen except for the 1cm border around it
% all. 128x96mm
\newcommand{\plainFrameImage}[1]{
\begin{frame}[plain]
%\begin{changemargin}{-1cm}{-1cm}
\begin{center}
\includegraphics[width=108mm,height=76mm,keepaspectratio]{#1}
\end{center}
%\end{changemargin}
\end{frame}
}

% Make one image take up the entire slide area, including borders, in
% centered/centred full-screen image, no title:
% This uses the entire whole screen
\newcommand{\maxFrameImage}[1]{
\begin{frame}[plain]
\begin{changemargin}{-1cm}{-1cm}
\begin{center}
\includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]
{#1}
\end{center}
\end{changemargin}
\end{frame}
}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\section*{Outline}
\begin{frame}
	\tableofcontents
\end{frame}
\section{Introduction}
\begin{frame}{Introduction}
\begin{block}{Internship}
\begin{itemize}
	\item Started: June 6th, 2009 (Professional Master of Software Engineering at PUF-HCM). 
	\item Environment: enterprises, institutes, universities or laboratories.
	\item Time: 6 months.
\end{itemize}
\end{block}
\begin{block}{Project}
\begin{itemize}
	\item Draw biological graphs in three dimensions.
	\item Base on BioFil framework.
	\item Implement in Common Lisp, OpenGL.
	\item Run on Linux
\end{itemize}
\end{block}
\end{frame}

\subsection*{Introduction of PUF-HCM and LaBRI}
\begin{frame}
\begin{block}{Pôles Universitaires Français (PUF) - HCM}
\begin{itemize}
	\item \small One of two centers of French universities in Vietnam.
	\item \small Established from 2006.
	\item \small Master Information: network and software engineering.
\end{itemize}
\end{block}
\begin{block}{Laboratoire Bordelais de Recherche en Informatique (LaBRI)}
\begin{itemize}
	\item \small An associated research unit of Aquitaine area.
	\item \small Teaching/researching staffs, doctoral students and post-docs.
	\item \small 6 teams: 
	\begin{itemize}
		\item Combinatorics and Algorithms
		\item Image and Sound
		\item Languages, Systems and Networks
		\item Formal Methods
		\item Models for Bioinformatics and Data Visualization
		\item Supports and Algorithms for High Performance Numerical Applications
	\end{itemize}
\end{itemize}
\end{block}
\end{frame}
\subsection*{Plan}
\begin{frame}
\begin{block}{Plan}
The process of development is divided into the phases:
\begin{itemize}
 	\item {From 18-Jun-2009 to 21-Jul-2009: learn tools and context application.}
 	\item {From 22-Jul-2009 to 24-Aug-2009: write technical and functional requirements.}
	\item {From 25-Aug-2009 to 30-Oct-2009: program the software.}
 	\item {From 01-Nov-2009 to 02-Dec-2009: test and delivery the software.}
\end{itemize}
\end{block}
\end{frame}

\section{Context}
\begin{frame}{Context}
	\begin{block}{Observations}
	\begin{itemize}
	\item Visualization becomes useful for Bioinformatics because it can be displayed huge data and large graph.
	\item Biological networks require to interact with users, so they should be drawn in 3D to observe more easily.
	\item Some existing applications are in 2D or using static images.
	\item Some applications can not integrate to biological data management system.
	\end{itemize}
	\end{block}
\end{frame}
\begin{frame}[fragile]
\frametitle{Set of data}
\begin{verbatim}
...
-METINT
CO2 aKG OAA cit mal NAD NADH coA AccoA...
-METEXT
ADP ATP...
-CAT
TCA1 : OAA + AccoA = cit + coA .
TCA1b : cit + coA + ATP = OAA + AccoA + ADP .
TCA2 : cit + NAD = aKG + CO2 + NADH .
TCA345 : aKG + 1.5 ADP + NAD = CO2 + mal + NADH + 1.5 ATP .
TCA6 : mal + NAD = NADH + OAA .
...
\end{verbatim}
\end{frame}
\begin{frame}{Set of data}
\begin{block}{Description of metabolism network of Tomato fruit}
	\begin{itemize}
		\item Generated by Metatool file \footnote{http://pinguin.biologie.uni-jena.de/bioinformatik/networks/metatool/metatool5.1/metatool5.1.html}
		\item Text file as description of metabolism network of Tomato fruit
		\item Biological reactions of animals, plants or humans in generally speaking
		\item A large number of reactions and molecules
	\end{itemize}
\end{block}
\end{frame}
\begin{frame}{Metabolism network of Tomato fruit}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.3]{images/mito-net-sample.png}
\end{figure}
}
\only<2>{
\begin{block}{}
	\begin{itemize}
		\item Draw linear or cycle pathways
		\item Show characteristics of cycles
		\item Support much in 3D simulation of these processes
		\item Allow users to interact with graphs
	\end{itemize}
\end{block}
}
\end{frame}

\begin{frame}{MetaCyc}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{images/tca-cycle-metacyc.png}
\end{figure}
}

\only<2>{
\begin{block}{Reactions of the TCA Cycle}
\begin{itemize}
	\item Show enzymes, substrates and products
	\item Duplicate molecules
	\item Be a simple cycle
	\item Display in 2D
\end{itemize}
\end{block}
}
\end{frame}

\begin{frame}{KEGG Drawing}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.25]{images/tca-cycle-pathway-kegg}
\end{figure}
}
\end{frame}

\maxFrameImage{images/tca-cycle-pathway-kegg.png}

\subsection{Biological Graph}
\begin{frame}{Biological Graph}
\begin{definition}
\begin{itemize}
	\item Biological Graph is a kind of graphs which nodes are divided into two groups: \textbf{reaction} 	and \textbf{molecule} nodes.
\end{itemize}
\end{definition}
\begin{block}{Example: TCA1: OAA + AccoA  -\textgreater  cit + coA}
	\centering
	\includegraphics[height=1.25in]{images/reaction2_pspdftex}\hspace{0.1 cm}
	\includegraphics[height=1.25in]{images/reaction_pspdftex}
\end{block}
\end{frame}
%\pause
\subsection{3D view}
\begin{frame}{3D view}
\begin{block}{Tulip}
\begin{itemize}
	\item Be written by David AUBER\footnote{http://tulip.labri.fr}
	\item Allow 3D visualization, drawing and edition of huge graphs
	\item Cluster automatically of the graphs
	\item Can't obtain acceptable drawings of metabolic networks
\end{itemize}
\end{block}
\begin{block}{BioLayout\textsuperscript{Java}(Goldovsky et al, 2005)}
\begin{itemize}
	\item Be written in Java language
	\item Display networks without analyzing
	\item Obtain difficultly biological features
\end{itemize}
\end{block}
\end{frame}
\subsection{Operations}
\begin{frame}{Operations}
\only<1>{
\begin{block}{Create Biological Graph}
\begin{itemize}
	\item Load from description files
	\item Reload from saved graphical files
\end{itemize}

\end{block}
\begin{block}{Draw Graph}
\begin{itemize}
	\item Generate automatically using random algorithm
	\item Apply the other layout algorithms to sub-graphs
	\begin{itemize}
		\item Karp's idea (Karp, 1994)
		\item Becker's idea (Becker, 2004)
		\item Force-directed layout (Tamassia, 1998)
		\item Clustering algorithm (Salvador and Chan, 2004)
	\end{itemize}
\end{itemize}
\end{block}
}
\only<2>{
\begin{block}{Interact with Graph}
\begin{itemize}
	\item Translation
	\item Rotation
	\item Zoom (Scale)
	\item Selection
	\item Picking up
	\item Highlighting
\end{itemize}
\end{block}
}
\end{frame}

\section{CABINE}
\begin{frame}
\begin{block}{Introduction}
\begin{itemize}
	\item CAbine is a BIological Network Editor
	\item Draw molecules
	\item Analyze and design in OOP
	\item Be a part of BioFil framework
		\begin{itemize}
			\item BioFil uses some components and data from BioCyc\footnote{http://biocyc.org}, Kegg\footnote{http://www.genome.jp/kegg}.
			\item BioCyc as well as Pathway Tools are written in pure of Lisp.
		\end{itemize}
	\item Be written in Common Lisp using McCLIM\footnote{Common Lisp Interface Manager}
	\item 2D Graphics
\end{itemize}
\end{block}
\end{frame}
\subsection{Architecture}
\begin{frame}{Architecture}
\begin{block}{Pipe-line features}
	\centering
	\includegraphics[scale=0.5]{images/Drawing2_pspdftex}
	\begin{itemize}
	\item This is a classical problem in Graph Drawing (Tamassia, 1998)
	\item The biological data analyzer generates reactions
	\item The biological graphs are built from the set of reactions
	\item The biological graphs are applied by graph layout algorithms
	\item The graphical view of the graphs in 3D are rendered by OpenGL's functions.
	\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Model}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.45]{images/data-model.png}
\end{figure}
}

\only<2>{
\begin{block}{Biological Data}
\begin{itemize}
	\item Biological data analyzer
	\item Composite Design Pattern
\end{itemize}
\end{block}
}

\only<3>{
\begin{columns}[c]
	\column{0.60\textwidth}
	\begin{block}{Biological Graph}
	\includegraphics[scale=0.45]{images/metabolic-graph-diagram-model.png}
	\end{block}
	\column{0.40\textwidth}
	\begin{block}{}
	\begin{itemize}
		\item Classical graph model
		\item Two types of node: reaction and molecule
		\item Every edge refers to two nodes as vertices.
	\end{itemize}
	\end{block}
\end{columns}
}
\only<4>{
\begin{columns}[c]
	\column{0.40\textwidth}
	\begin{block}{Graphical Graph}
	\centering
	\includegraphics[scale=0.45]{images/biological-drawing.png}
	\end{block}
	\column{0.60\textwidth}
	\begin{block}{}
	\begin{itemize}
		\item Add positions of graph's elements
		\item Add informations about layout
		\item Detect graph's topology
	\end{itemize}
	\end{block}
\end{columns}
}
\end{frame}

\begin{frame}{Biological Data part}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.35]{images/biological-model-diagram.png}
\end{figure}
}
\only<2>{
\begin{block}{Biological Data}
	\begin{itemize}
		\item Two kinds of biological groups: process and object
		\item Reaction includes in reactants and products
		\item Pathway is a series of chemical reactions
		\item Composite Design Pattern
	\end{itemize}
\end{block}
}
\end{frame}

\begin{frame}{Graphical Graph part}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.35]{images/metabolic-graph-diagram.png}
\end{figure}
}

\only<2>{
\begin{block}{Graphical view}
	\begin{itemize}
		\item Middle layer of biological data and biological viewing
		\item Typical graph: set of nodes and edges
		\item Two type of nodes: reaction and molecule
	\end{itemize}
\end{block}
}
\end{frame}

\begin{frame}{Drawing part}
\only<1>{
\begin{figure}
	\centering
	\includegraphics[scale=0.35]{images/cl-object-3d-diagram-new.png}
\end{figure}
}

\only<2>{
\begin{block}{3D Objects interact with OpenGL}
	\begin{itemize}
		\item Draw really nodes and edges in OpenGL
		\item Access the OpenGL's functions
		\item Add and remove operations on graph's components
		\item Interaction: modify graph or change OpenGL's environment
	\end{itemize}
\end{block}
}
\end{frame}

\subsection{Implementation}
\begin{frame}{Implementation}
\only<1>{
\begin{block}{OpenGL}
	\begin{itemize}
		\item \small Programming specification for 2D and 3D graphics applications.
		\item \small Cross-platform standard for 3D rendering and hardware acceleration.
		\item \small Hide the complexities of interfacing with different 3D accelerators and the differing capabilities of hardware platforms
		\item \small Bind with the other programming languages
		\item \small Associated utility libraries: GLU\footnote{OpenGL Utility}, GLUT\footnote{OpenGL Utility Toolkit}
		\item \small Mesa\footnote{http://www.mesa.org}
	\end{itemize}
\end{block}
}
\only<2>{
\begin{block}{Simple Directmedia Layer (SDL)}
	\begin{itemize}
		\item Be a cross-platform, free and open source software multimedia library
		\item Provide low level access to audio, keyboard, mouse, joystick, 3D hardware via OpenGL, and 2D video framebuffer
		\item Be written in C
		\item SDL\_ttf - TrueType font rendering support
		\item SDL\_rtf - simple Rich Text Format rendering
		\item cl-sdl is an OpenGL binding for Common Lisp
	\end{itemize}
\end{block}
}
\only<3>{
\begin{block}{Other libraries}
\begin{itemize}
	\item CLX
	\item McCLIM, ltk
	\item cl-opengl
	\item cl-glfw
	\item cl-graph
	\item imago
	\item cl-zlib
	\item cell-gtk
	\item gtk-server-wrapper
\end{itemize}
\end{block}
}
\only<4>{
\begin{block}{Visualization on Graphs}
\begin{itemize}
	\item Graphviz
	\item BabelGraph
	\item LinLogLayout
	\item VisAnt (Zhenjun, 2009)
	\item Cytoscape (Shannon, 2003)
	\item jung
	\item JiggleSource
\end{itemize}
\end{block}
}
\only<5>{
\begin{block}{Relationship among components}
	\begin{figure}
	\centering
	\includegraphics[scale=0.35]{images/cabine-sbcl-sdl-opengl}
\end{figure}
\end{block}
}
\end{frame}

\begin{frame}{Implementation}
\begin{columns}[c]
	\column{0.4\textwidth}
	\begin{block}{Tools}
	\begin{itemize}
		\item Written on Common Lisp (CL)
		\item Draw automatically biological graphs 
		\item Rotate, translate and scale graphs
		\item Select, highlight and move nodes
		\item Problems: display labels in 3D, create widgets
	\end{itemize}
	\end{block}
	\column{0.6\textwidth}
	\begin{block}{Demonstration}
	\centering
	\includegraphics[scale=0.25]{images/metabolic-network-editor_pspdftex}
	\end{block}
\end{columns}
\end{frame}

\subsection{Results}
\begin{frame}{Results}
\begin{block}{}
The results are achieved:
\begin{itemize}
	\item Load biological data from description files as metatool files
	\item Create biological graph
	\item Draw automatically biological graph
	\item Interact to biological graph such as move nodes, zoom scene, highlight nodes and edges, rotate etc.
\end{itemize}
\end{block}
\end{frame}

\section{Conclusion and future work}
\begin{frame}
\begin{block}{Conclusions}
The application is the result that we have obtained for 6 months. Finally, some functionalities have figured out and implemented:
	\begin{itemize}
	\item \small Store biological data in computer's memory
	\item \small Define biological graphs
	\item \small Visualize biological graphs
	\item \small Interact with biological graphs
	\end{itemize}
\end{block}
\pause
\begin{block}{Future works}
The below approaches need to pay attention when we want to upgrading the application:
	\begin{itemize}
	\item \small Finish the module of show labels
	\item \small Extend operations
	\item \small Apply layout algorithms
	\item \small Integrate GUI
	\end{itemize}
\end{block}

\end{frame}
%The end
\begin{frame}
\begin{center} \Huge Thanks for your attention! \end{center}
\end{frame}
\end{document}