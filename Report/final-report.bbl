\begin{thebibliography}{10}

\bibitem{Moritz:gladmp}
Moritz~Y. Becker and Isabel Rojas.
\newblock A graph layout algorithm for drawing metabolic pathways.
\newblock {\em Bioinformatics}, 17(5):461--647, 2001.

\bibitem{berge:hypergraphs}
Claude Berge.
\newblock {\em Hypergraphs: combinatorics of finite sets}.
\newblock Elsevier Science, 1987.

\bibitem{chaomei:graph-drawing-algorithms}
Chaomei Chen.
\newblock {\em Graph Drawing Algorithms}, chapter~3, pages 65--87.
\newblock Information Visualization. Springer London, second edition, October
  2006.
\newblock This is a full INBOOK entry.

\bibitem{lecture-notes:mark-bio-intro}
Mark Gerstein.
\newblock Bioinformatics introduction.
\newblock \url{http://bioinfo.mbb.yale.edu/mbb452a/intro}, 1999.
\newblock Accessed August 25, 2009.

\bibitem{gallo:hypergraphs-application}
Sang~Nguyen Giorgio~Gallo, Giustino~Longo and Stefano Pallottino.
\newblock Directed hypergraphs and applications.
\newblock {\em Discrete Applied Mathematics}, 42:177--201, 1993.

\bibitem{liaec:biolayout}
Leon Goldovsky, Ildefonso Cases, Anton~J. Enright, and Christos~A. Ouzounis.
\newblock Biolayout(java): versatile network visualisation of structural and
  functional relationships.
\newblock {\em Applied bioinformatics}, 4(1):71--74, 2005.

\bibitem{graham:onlisp}
Paul Graham.
\newblock {\em On Lisp: Advanced Techniques for Common Lisp}.
\newblock Prentice Hall, 2003.

\bibitem{jonathan:graph-applications}
Jonathan~L. Gross and Jay Yellen.
\newblock {\em Graph theory and its applications}.
\newblock Taylor and Francis Group, 2006.

\bibitem{website:opengl-org-viewing}
Khronos Group.
\newblock Using viewing and camera transforms, and glulookat().
\newblock \url{http://www.opengl.org/resources/faq/technical/viewing.htm},
  September 2009.

\bibitem{peter-herth:ltk}
Peter Herth.
\newblock Ltk - a lisp binding to the tk toolkit.
\newblock A guide of Lisp binding to the Tk toolkit, February 2006.

\bibitem{Zhenjun:VisANT}
Zhenjun Hu, Jui-Hung Hung, Yan Wang, Yi-Chien Chang, Chia-Ling Huang, Matt
  Huyck, and Charles DeLisi.
\newblock Visant 3.5: multi-scale network visualization, analysis and inference
  based on the gene ontology.
\newblock {\em Nucleic Acids Research}, 37(Web Server issue):115--121, 2009.

\bibitem{kamran:label-layout}
Knut~Hartmann Kamran~Ali and Thomas Strothotte.
\newblock Label layout for interactive 3d illustrations.
\newblock {\em The Journal of WSCG}, 13, January 2005.

\bibitem{Karp:ADMP}
Peter~D. Karp and Suzanne Paley.
\newblock Automated drawing of metabolic pathways.
\newblock {\em Bioinformatics}, 1994.

\bibitem{website:joseph-graph-intro}
Joseph Khoury.
\newblock Application to graph theory.
\newblock \url{http://aix1.uottawa.ca/~jkhoury/graph.htm}.
\newblock Accessed November 13, 2009.

\bibitem{website:rashid-graph-intro}
Rashid~Bin Muhummad.
\newblock Graph introduction.
\newblock
  \url{http://www.personal.kent.edu/~rmuhamma/GraphTheory/MyGraphTheory/graphI%
ntro.htm}.
\newblock Accessed November 13, 2009.

\bibitem{suzanne:pathwaytools}
Suzanne~M. Paley and Peter~D. Karp.
\newblock The pathway tools cellular overview diagram and omics viewer.
\newblock {\em Nucleic Acids Research}, 34(13):3771--3778, 2006.

\bibitem{ms-ka:OpenGL-Specification}
Mark Segal and Kurt Akeley.
\newblock {\em The OpenGL Graphics System: A Specification}.
\newblock Unknown, March 2009.

\bibitem{seibel:practicalcl}
Peter Seibel.
\newblock {\em Practical Common Lisp}.
\newblock Apress, 2005.

\bibitem{shannon:cytoscape}
Markiel~A Shannon~P and Ozier O.
\newblock Cytoscape: a software environment for integrated models of
  biomolecular interaction networks.
\newblock {\em Genome Research}, 13(11):2498--2504, 2003.

\bibitem{tid:graph-drawing}
Ioannis~G. Tollis, Giuseppe Di~Battista, Peter Eades, and Roberto Tamassia.
\newblock {\em Graph Drawing: Algorithms for the Visualization of Graphs}.
\newblock {Prentice Hall}, July 1998.

\bibitem{lecture-notes:helge-intro-bio}
Helge Weissig.
\newblock Introduction to bioinformatics.
\newblock \url{http://www.bioinformaticscourses.com/bioinform}, 2003.
\newblock Accessed August 25, 2009.

\bibitem{rw:openglApp}
Robert Whitrow.
\newblock {\em OpenGL Graphics Through Applications}.
\newblock Springer-Verlag London, 2008.

\bibitem{gl:superBible}
Richard~S. Wright, Jr.~Benjamin Lipchak, and Nicholas Haemel.
\newblock {\em OpenGL Super Bible: comprehensive tutorial and reference}.
\newblock Addison-Wesley, fourth edition, 2007.

\end{thebibliography}
