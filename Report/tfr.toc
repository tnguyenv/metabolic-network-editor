\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {paragraph}{}{3}{section*.3}
\contentsline {section}{\numberline {2}Analysis of the problem}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Overview of Metabolic Networks}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Graph drawing and layout algorithms}{4}{subsection.2.2}
\contentsline {paragraph}{General graph algorithms:}{4}{section*.5}
\contentsline {subparagraph}{Graph definition}{4}{section*.6}
\contentsline {subparagraph}{}{5}{section*.7}
\contentsline {paragraph}{Force-Directed Graph Layout}{5}{section*.8}
\contentsline {paragraph}{Applications in Biology}{5}{section*.9}
\contentsline {subparagraph}{The primary operations consist in the application:}{5}{section*.10}
\contentsline {subsection}{\numberline {2.3}Proposal functional operations}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Proposal non-functional operations}{6}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Implemention}{6}{subsection.2.5}
\contentsline {paragraph}{Choice the language: Common Lisp}{6}{section*.11}
\contentsline {paragraph}{Develop Environment: Emacs}{6}{section*.12}
\contentsline {section}{\numberline {3}Prototypes}{6}{section.3}
\contentsline {section}{\numberline {4}Testing}{6}{section.4}
\contentsline {section}{\numberline {5}Planning}{7}{section.5}
\contentsline {section}{\numberline {6}Works have done}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}Search open-source libraries for graph drawing}{7}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Learn Common Lisp language}{8}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Choose tools to make GUIs and interact with OpenGL}{8}{subsection.6.3}
\contentsline {paragraph}{}{8}{section*.17}
\contentsline {paragraph}{}{8}{section*.18}
\contentsline {subsection}{\numberline {6.4}Libraries}{8}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}Coding}{9}{subsection.6.5}
