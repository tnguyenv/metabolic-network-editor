\beamer@endinputifotherversion {3.07pt}
\select@language {english}
\beamer@sectionintoc {2}{Introduction}{3}{0}{1}
\beamer@sectionintoc {3}{Context}{6}{0}{2}
\beamer@subsectionintoc {3}{1}{Biological Graph}{15}{0}{2}
\beamer@subsectionintoc {3}{2}{3D view}{16}{0}{2}
\beamer@subsectionintoc {3}{3}{Operations}{17}{0}{2}
\beamer@sectionintoc {4}{CABINE}{19}{0}{3}
\beamer@subsectionintoc {4}{1}{Architecture}{20}{0}{3}
\beamer@subsectionintoc {4}{2}{Implementation}{31}{0}{3}
\beamer@subsectionintoc {4}{3}{Results}{37}{0}{3}
\beamer@sectionintoc {5}{Conclusion and future work}{38}{0}{4}
