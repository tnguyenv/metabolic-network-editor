;;; -*- Mode: Lisp; Package: OBJECT-3D -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines 3D objects interact with OpenGL

(in-package :common-lisp-object-3d)

;;; Declares global variables and contants
(defvar *max-value* 5.0)
(defconstant +PI+ 3.14159)
(defconstant +DEG2RAD+ (/ +PI+ 180))
(defconstant +RAD2DEG+ (/ 180 +PI+))
(defconstant +line-head-none+ 0)
(defconstant +line-head-arrow+ 1)
(defconstant +line-head-box+ 2)
(defconstant +sphere-radius+ 0.2f0)

;;; Defines the Point3D class randomizes a coordinate in 2,3-dimension space
(defclass Point3D ()
  ((x :accessor x :initarg :x :initform 0)
   (y :accessor y :initarg :y :initform 0)
   (z :accessor z :initarg :z :initform 0)))
;;; Defines the  Matrix class deals with data type of matrix
(defclass Matrix ()
  ((fvals :accessor fvals :initarg :fvals
          :initform (make-array 16 :initial-element 0.0)
          :documentation "The values of matrix")))

;;; Defines the Color3D class 
(defclass Color3D ()
  ((red :accessor red :initarg :red)
   (green :accessor green :initarg :green)
   (blue :accessor blue :initarg :blue)))

(defclass G3DElement ()
  ())

(defclass Shape3D (G3DElement)
  ((color :accessor color :initarg :color)
   (selected :accessor selected :initarg :selected)))

(defclass Screen ()
  ((objects :accessor object :initarg :object)
   (width   :accessor width  :initarg :width)
   (height  :accessor height :initarg :height)
   (depth   :accessor depth  :initarg :depth)))

(defclass Cube (Shape3D)
  ((size   :accessor size   :initarg :size)
   (center :accessor center :initarg :center)))

(defclass Sphere (Shape3D)
  ((size :accessor size :initarg :size)
   (center :accessor center :initarg :center)))

(defclass Line3D (Shape3D)
  ((source :accessor source :initarg :source)
   (target :accessor target :initarg :target)
   (width  :accessor width  :initarg :width)
   (style  :accessor style  :initarg :style)))

(defclass Arrow3D (Line3D)
  ((action :accessor action :initarg :action)))

(defclass Text3D (G3DElement)
  ((tex  :accessor tex  :initarg :tex)
   (font :accessor font :initarg :font)))

(defclass BoundingBox3D (G3DElement)
  ((elements :accessor elements :initarg :elements
             :documentation "This is the composited element.")))

;;; Defines the utility functions
(defun square (n)
  (* n n))

(defun c2float (num)
  (coerce num 'float))

(defun d2float (num)
  (coerce num 'double-float))

(defun my-random (max-value)
  (setf max-value (c2float max-value))
  (if (<= 4 (random 10))
      (- (random max-value))
      (random max-value)))

;;; Defines the methods that interact to GL Environment
(defgeneric set-draw-color (color))
(defmethod set-draw-color ((color Color3D))
  (with-slots (red green blue) color
    (gl:color (c2float red) (c2float green) (c2float blue))))

;;; Implements the methods of the Color3D class
(defgeneric make-color3d (red green blue))
(defmethod make-color3d (red green blue)
  (make-instance 'Color3D :red red :green green :blue blue))

;;; Implements the methods of the Point3D class. In practical, we don't need to separate Vector3D and Point3D,
;;; and therefore we build some methods which manipulate on Vector3D type.
(defgeneric make-point3d (&optional x y z))
(defmethod make-point3d (&optional (x 0.f0) (y 0.0f0) (z 0.0f0))
  (make-instance 'Point3D :x x :y y :z z))

(defgeneric display-point-3d (point)
  (:documentation "Display a point in the screen"))
(defmethod display-point-3d ((point Point3D))
  (format t "~%(~d, ~d, ~d)" (x point) (y point) (z point)))

(defgeneric randomize-point3d ())
(defmethod randomize-point3d ()
  (make-point3d (c2float (my-random *max-value*))
                (c2float (my-random *max-value*))
                (c2float (my-random *max-value*))))

(defgeneric copy (p1 p2)
  (:documentation "Copy p2 in p1"))
(defmethod copy ((p1 Point3D) (p2 Point3D))
  (setf (x p1) (x p2)
        (y p1) (y p2)
        (z p1) (z p2)))

(defgeneric set-coordinates-from (point &optional x y z)
  (:documentation "Set the coordinates of the point with x y z"))
(defmethod set-coordinates-from ((point Point3D) &optional (x 0.0f0) (y 0.0f0) (z 0.0f0))
  (setf (x point) x
        (y point) y
        (z point) z)
  point)

(defgeneric clone (point)
  (:documentation "Create a clone of point"))
(defmethod clone ((point Point3D))
  (make-point3d (x point) (y point) (z point)))

(defgeneric length-of (point)
  (:documentation "Give length of vector point"))

(defmethod length-of ((point Point3D))
  (ignore-errors (sqrt (dot-product point point))))
  
(defgeneric dot-product (p1 p2)
  (:documentation "Dot product of p1 and p2"))

(defmethod dot-product ((p1 Point3D) (p2 Point3D))
  (reduce #'+ (mapcar #'*
		      (list (x p1) (y p1) (z p1))
                      (list (x p2) (y p2) (z p2)))))

(defgeneric angle (p1 p2)
  (:documentation "Give the angle between p1 and p2"))

(defmethod angle ((p1 Point3D) (p2 Point3D))
  (acos (/ (dot-product p1 p2)
	   (* (length-of p1) (length-of p2)))))

(defgeneric multiply (point number)
  (:documentation "Create a new point which is point*number"))
(defmethod multiply ((point Point3D) number)
  (make-point3d (* number (x point))
                (* number (y point))
                (* number (z point))))

(defgeneric multiply-into (point number)
  (:documentation "Destructive version of multiply"))
(defmethod multiply-into ((point Point3D) number)
  (setf (x point) (* number (x point))
        (x point) (* number (y point))
        (x point) (* number (z point))))

(defgeneric add (p1 p2)
  (:documentation "Add p1 and p2"))
(defmethod add ((p1 Point3D) (p2 Point3D))
  (make-point3d (+ (x p1) (x p2))
                (+ (y p1) (y p2))
                (+ (z p1) (z p2))))

(defgeneric add-to (p1 p2)
  (:documentation "Destructive version of add, modifies p1"))
(defmethod add-to ((p1 Point3D) (p2 Point3D))
  (with-slots (x y z) p1
    (setf (x p1) (+ x (x p2))
          (y p1) (+ y (y p2))
          (z p1) (+ z (z p2)))))

(defgeneric substract (p1 p2)
  (:documentation "Substract p2 from p1"))
(defmethod substract ((p1 point3D) (p2 point3D))
  (make-point3d (- (x p1) (x p2))
                (- (y p1) (y p2))
                (- (z p1) (z p2))))

(defgeneric substract-from (p1 p2)
  (:documentation "Destructive version of substract, modifies p1"))
(defmethod substract-from ((p1 point3D) (p2 point3D))
  (with-slots (x y z) p1
    (setf x (- x (x p2))
          y (- y (y p2))
          z (- z (z p2)))))

(defgeneric align-with (p1 p2))
(defmethod align-with ((p1 point3D) (p2 point3D))
  (let ((length-p1 (length-of p1))
	(length-p2 (length-of p2)))
    (unless (zerop length-p1)
      (copy p2 (multiply (clone p1)
			 (/ length-p2 length-p1))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Useful methods for 3d operations ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defgeneric distance (p1 p2)
  (:documentation "Distance between p1 and p2"))

(defmethod distance ((p1 Point3D) (p2 Point3D))
  (sqrt (+ (expt (- (x p1) (x p2)) 2)
           (expt (- (x p1) (x p2)) 2)
           (expt (- (x p1) (x p2)) 2))))

(defgeneric cross-product (p1 p2)
  (:documentation "Cross product of p1 and p2"))

(defmethod cross-product ((p1 Point3D) (p2 Point3D))
  (let ((x1 (x p1)) 
	(y1 (y p1)) 
	(z1 (z p1))
	(x2 (x p2))
	(y2 (y p2))
	(z2 (z p2)))
    (make-point3d (- (* y1 z2) (* z1 y2))
                  (- (* z1 x2) (* x1 z2))
                  (- (* x1 y2) (* y1 x2)))))

(defgeneric null-point (point)
  (:documentation "Test if point is the (0,0,0) vector"))
(defmethod null-point ((p Point3D))
  (with-slots (x y z) p
    (let ((lst (list x y z)))
      (every #'zerop lst))))

(defgeneric normalise-point (point)
  (:documentation "Normalise point"))
(defmethod normalise-point ((point Point3D))
  (when (zerop (length-of point))
    (progn
      (format t "~%Normalise point: the length of the vector has zero")
      (return-from normalise-point nil)))
  (multiply-into point (/ 1 (length-of point))))

(defgeneric normalise-vector (p1 p2 p3))
(defmethod normalise-vector ((p1 point3D) (p2 point3D) (p3 point3D))
  (let ((vn (cross-product (substract p1 p2) (substract p2 p3))))
    (normalise-point vn)
    vn))

;;; Methods of the Matrix class
(defgeneric make-matrix (fvals)
  (:documentation "Initialise and create a matrix with the fvals values passed"))
(defmethod make-matrix (fvals)
  (make-instance 'Matrix :fvals fvals))

(defgeneric make-matrix-with (origin zaxis &optional xaxis)
  (:documentation "Construct martrix which when applied puts local origin at point and the local Z axis in direction"))
(defmethod make-matrix-with ((origin Point3D) (zaxis Point3D) &optional xaxis)
  (let ((matrix (make-matrix (make-array 16 :initial-element 0))))
    (set-identity matrix)
    (set-values-with matrix origin zaxis xaxis)))
   
(defgeneric set-values (matrix vals)
  (:documentation "Set matrix using the vals list"))
(defmethod set-values ((matrix Matrix) vals)
  (dotimes (i (length vals))
    (setf (aref (fvals matrix) i) (nth i vals)))
  matrix)

(defgeneric set-values-with (matrix origin zaxis &optional xaxis))
(defmethod set-values-with ((matrix Matrix) (origin Point3D) (zaxis Point3D) &optional xaxis)
  (let* ((zaxis-int (clone zaxis))
         (xaxis-int)
         (yaxis-int)
         (lst))
    
    (normalise-point zaxis-int)
    (cond ((not (eql xaxis nil))
           (progn
             (format t "~%Done here")
             (setf xaxis-int xaxis)))
          (t
           (let ((arbAxis (make-point3d 0 0 0)))
             (cond ((and (<= (abs (x zaxis-int)) (abs (y zaxis-int)))
                         (<= (abs (x zaxis-int)) (abs (z zaxis-int))))
                    (set-coordinates-from arbAxis 1.0f0 0.0f0 0.0f0))
                   
                   ((and (<= (abs (y zaxis-int)) (abs (x zaxis-int)))
                         (<= (abs (y zaxis-int)) (abs (z zaxis-int))))
                    (set-coordinates-from arbAxis 0.0f0 1.0f0 0.0f0))
                   (t
                    (set-coordinates-from arbAxis 0.0f0 0.0f0 1.0f0)))
             (setf xaxis-int (cross-product zaxis-int arbAxis)))))

    (normalise-point xaxis-int)
    (setf yaxis-int (cross-product zaxis-int xaxis-int))
    (setf lst (list (x xaxis-int) (y xaxis-int) (z xaxis-int) 0.0f0
                    (x yaxis-int) (y yaxis-int) (z yaxis-int) 0.0f0
                    (x zaxis-int) (y zaxis-int) (z zaxis-int) 0.0f0
                    (x origin) (y origin) (z origin) 1.0f0))
    (setf (fvals matrix) lst)
    matrix))
               
(defgeneric set-identity (matrix)
  (:documentation "Set matrix to identity:
   //
   // 1 0 0 0
   // 0 1 0 0
   // 0 0 1 0
   // 0 0 0 1"))
(defmethod set-identity ((matrix Matrix))
  (setf (aref (fvals matrix) 0) 1 (aref (fvals matrix) 4) 0 (aref (fvals matrix) 8)  0 (aref (fvals matrix) 12) 0
        (aref (fvals matrix) 1) 0 (aref (fvals matrix) 5) 1 (aref (fvals matrix) 9)  0 (aref (fvals matrix) 13) 0
        (aref (fvals matrix) 2) 0 (aref (fvals matrix) 6) 0 (aref (fvals matrix) 10) 1 (aref (fvals matrix) 14) 0
        (aref (fvals matrix) 3) 0 (aref (fvals matrix) 7) 0 (aref (fvals matrix) 11) 0 (aref (fvals matrix) 15) 1)
  matrix)

;;; Instructor functions
(defun make-cube (&key (selected nil)
                  (size 0.2f0)
                  (center (randomize-point3d)))
  (make-instance 'cube :center center
                 :size size :selected selected))

(defun make-sphere (&key (color (make-color3d 1.0f0 0.0f0 0.0f0))
                    (selected nil)
                    (size +sphere-radius+)
                    (center (randomize-point3d)))
  (make-instance 'sphere :center center :size size :color color :selected selected))

(defun make-line3d (&key (selected nil)
                    (color-3f (make-color3d 1.0f0 0.0f0 0.0f0))
                    (source (randomize-point3d))
                    (target (randomize-point3d))
                    (width 1.0f0)
                    (style 1.0f0))
  (make-instance 'Line3D :selected selected :color color
                 :source source :target target :width width :style style))
                   
;;; Generic functions to draw 3D objects in OpenGL
(defgeneric draw-arrow3d (orig n color))
(defgeneric draw-cube (size point))
(defgeneric draw-line3d (src-pnt tgt-pnt width color))
(defgeneric draw-sphere (radius point color))
(defgeneric draw-text3d (text pos))

;;; and implementations
(defmethod draw-arrow3d ((orig Point3D) n (color Color3D))
  (gl:push-matrix)
  (gl:translate (x orig) (y orig) (z orig))
  (set-draw-color color)
  (let ((theta)
        (quad (gl:new-quadric)))
    (setf theta (* +RAD2DEG+ (acos (make-array n :initial-element 2.0))))
;;    (setf theta (* +RAD2DEG+ (acos (sgum:deref-array n gl:float 2))))
    (gl:rotate (+ 180 theta)
                 (* -1.0f0 (make-array n :initial-element 1.0))
		 ;;(* -1.0f0 (sgum:deref-array n gl:float 1))
                 (make-array n :initial-element 0) 0.0f0)
    (quadric-orientation quad outside)
    (cylinder quad (d2float 0) (d2float 0.1) (d2float 0.3) 32 32))
  (gl:pop-matrix))

;;; From Francoise's code
(defmethod draw-cube (cube-size (p Point3D))
  ;;dessine un cube de taille cube-size autour de l'origine
  (let ((size (coerce cube-size 'float)))
    (gl:push-matrix)
    (gl:disable lighting)
    (gl:disable light0)
    (gl:translate (x p) (y p) (z p))
    (gl:begin quads)
    (color-3f 1.0f0 0.0f0 0.0f0)			
    (vertex-3f size size size)			
    (vertex-3f size (- size) size)			
    (vertex-3f (- size) (- size) size)			
    (vertex-3f (- size) size size)			
    (color-3f 1.0f0 1.0f0 0.0f0)			
    (vertex-3f size size (- size))			
    (vertex-3f size (- size) (- size))			
    (vertex-3f (- size) (- size) (- size))			
    (vertex-3f (- size) size (- size))			
    (color-3f 1.0f0 0.0f0 1.0f0)			
    (vertex-3f size size size)			
    (vertex-3f size (- size) size)			
    (vertex-3f size (- size) (- size))			
    (vertex-3f size size (- size))			
    (color-3f 1.0f0 1.0f0 1.0f0)			
    (vertex-3f (- size) size size)			
    (vertex-3f (- size) (- size) size)			
    (vertex-3f (- size) (- size) (- size))			
    (vertex-3f (- size) size (- size))			
    (color-3f 0.0f0 1.0f0 0.0f0)			
    (vertex-3f (- size) size (- size))			
    (vertex-3f (- size) size size)			
    (vertex-3f size size size)			
    (vertex-3f size size (- size))			
    (color-3f 0.0f0 0.0f0 1.0f0)			
    (vertex-3f (- size) (- size) (- size))			
    (vertex-3f (- size) (- size) size)			
    (vertex-3f size (- size) size)			
    (vertex-3f size (- size) (- size))			
    (end)
    (pop-matrix)))

(defmethod draw-line3d ((src-pnt Point3D) (tgt-pnt Point3D) width (color Color3D))
  (gl:begin lines)
  (gl:enable line-smooth)
  (gl:line-width width)
  (set-draw-color color)
  (vertex-3f (x src-pnt) (y src-pnt) (z src-pnt))
  (vertex-3f (x tgt-pnt) (y tgt-pnt) (z tgt-pnt))
  (gl:end)

  (let* ((n 3)
         (vx (- (x tgt-pnt) (x src-pnt)))
         (vy (- (y tgt-pnt) (y src-pnt)))
         (vz (- (z tgt-pnt) (z src-pnt)))
         (vector (make-point3d))
         (tt)
         (m-point (make-point3d))
         (mag))
    (setf mag (sqrt (+ (square vx) (square vy) (square vz))))
    (setf (make-array n :initial-element 0) (c2float (/ vx mag))
          (make-array n :initial-element 1) (c2float (/ vy mag))
          (make-array n :initial-element 2) (c2float (/ vz mag)))
    ;; Tinh toan diem nam ngoai hinh sphere
    ;; Vector chi phuong giua src-pnt va tgt-pnt
    (setf (x vector) vx
          (y vector) vy
          (z vector) vz)
    ;; Lap phuong trinh duong thang di qua src-pnt
    ;; x = (x src-pnt) + (x vector) * t
    ;; y = (y src-pnt) + (y vector) * t
    ;; y = (z src-pnt) + (z vector) * t
    (setf tt (1+ (/ +sphere-radius+ mag)))
    (setf tt (* tt 0.75))
    (setf (x m-point) (+ (x src-pnt) (* (x vector) tt))
          (y m-point) (+ (y src-pnt) (* (y vector) tt))
          (z m-point) (+ (z src-pnt) (* (z vector) tt)))
    (draw-arrow3d m-point n color)))

(defmethod draw-sphere (radius (p Point3D) (color Color3D))
  (setf radius (d2float radius))
  (gl:push-matrix)
  (gl:translate (x p) (y p) (z p))
  
  (single-float (no-mat (red color) (green color) (blue color) 1.0f0)
    (material front ambient no-mat))

;;  (sgum:with-single-float-values (mat-diffuse 0.0f0 0.5f0 0.8f0 1.0f0)
;;    (gl:material-fv gl:+front+ gl:+specular+ mat-diffuse))

;;   (sgum:with-single-float-values (mat-spec 0.0f0 0.0f0 0.0f0 1.0f0)
;;     (gl:material-fv gl:+front+ gl:+specular+ mat-spec))
  
   (single-float (high-shininess 100f0)
     (material front shininess high-shininess))

  (single-float (no-mat (red color) (green color) (blue color) 1.0f0)
    (material front emission no-mat))

  (gl:enable lighting)
  (gl:enable light0)

  (let ((quad (new-quadric)))
    (quadric-draw-style quad fill)
    (quadric-normals quad smooth)
    (sphere quad radius 50 20)
    (delete-quadric quad))
  (gl:pop-matrix)
  (gl:disable lighting)
  (gl:disable light0)
  (gl:flush))

;;; Generic functions for all
(defgeneric add-element (boundingbox3d &rest args))
(defmethod add-element ((boudingbox3d BoundingBox3D) &rest args)
  (setf (elements boundingbox3d) (append (elements boundingbox3d) args)))

(defgeneric remove-element (boundingbox3d &rest args))
(defmethod remove-element ((boundingbox3d BoundingBox3d) &rest args)
  (setf (elements boundingbox3d) (remove args (elements boundingbox3d) :count 1)))

(defgeneric draw (shape))
(defmethod draw ((g3delement G3DElement))
  nil)
(defmethod draw ((shape Cube))
  (draw-cube (size shape) (center shape)))
(defmethod draw ((shape Sphere))
  (draw-sphere (size shape) (center shape) (color shape)))
(defmethod draw ((shape Arrow3D))
  (draw-arrow3d (source shape) (target shape) (action shape)))

(defmethod draw ((shape Line3D))
  ;;(draw-line (source shape) (target shape) +line-head-arrow+ (width shape) (color shape)))
  (draw-line3d (source shape) (target shape) (width shape) (color shape)))
(defmethod draw ((shape Text3D))
  nil)
(defmethod draw :after ((boundingbox3d BoundingBox3D))
  (mapc #'draw (elements boundingbox3d)))

;;; Move the point to new position
(defgeneric move (point3d new-point3d))
(defmethod move ((source-point Point3D) (target-point Point3D))
  (setf (x source-point) (x target-point)
        (y source-point) (y target-point)
        (z source-point) (z target-point)))
;;; Move the shape to a new point
(defgeneric move (shape3d new-center-position))
(defmethod move ((shape3d Cube) (point3d Point3D))
  (setf (center shape3d) point3d))
(defmethod move ((shape3d Sphere) (point3d Point3D))
  (setf (center shape3d) point3d))
