;;; -*- Mode: Lisp; Package: BIO-VEW -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines the methods deal with graphical presentation of biological graph

(in-package :bio-view)

(declaim (type cl:single-float *xspeed* *yspeed* *zspeed*))
(defparameter *xspeed* 0f0)
(defparameter *yspeed* 0f0)
(defparameter *zspeed* 0f0)
(defparameter *in* 1)
(defparameter *out* 2)
(defparameter *ratio* 0.01f0)
(defparameter *x* 0.0f0)
(defparameter *y* 0.0f0)
(defparameter *z* 0.0f0)
;; Delta for keyboard scroll
(defparameter *dx* 0.5f0)
(defparameter *dy* 0.5f0)
(defparameter *dz* 0.5f0)
;; Delta for mouse scroll
(defparameter *dxm* 0.01f0)
(defparameter *dym* -0.01f0) ; negative for invert mouse
;; Rotational position around the x and y axes
(defparameter *xr* 0f0)
(defparameter *yr* 0f0)
;; Delta for mouse rotate
(defparameter *dxr* 0.5f0)
(defparameter *dyr* 0.5f0)
;; Selection buffer
(defconstant +selection-buffer-len+ 512)
(defconstant +feed-buffer-size+ 4096)
;; Current mouse position
(defparameter *mx* 0)
(defparameter *my* 0)
(defparameter *mvm* (sgum:allocate-foreign-object gl:double 16))
;; Mouse buttons
(defparameter *mb* (make-array 7 :initial-element nil))
(defparameter *original-color-molecule* (make-color3d 1.0f0 0.0f0 0.0f0))
(defparameter *original-color-edge* (make-color3d 1.0f0 0.0f0 1.0f0))
(defparameter *highlight-color-edge* (make-color3d 0f0 0f0 1f0))
(defparameter *highlight-color-molecule* (make-color3d 0f0 1f0 0f0))
(defparameter *running* nil)
(defparameter *m-graph* (make-graph-metabolic nil nil))
(defparameter *picked-object* nil)
(defparameter *selected-objects* nil)
(defparameter *rz* 1.0f0)
(defparameter *pressed-button* 1)
(defparameter *pressed-key* 0)
(defparameter *current-mode* gl:+render+)
(defparameter *screen-width* 800)
(defparameter *screen-height* 600)
;; Stuffs
(defparameter obj nil)
(defparameter on t)
(defparameter off nil)

(defun modded (mod-state modifier)
    (= 0 (logand mod-state modifier)))

(defun handle-keypress-cabine (key)
  (cond
    ;; Esc always clears selection
    ((eql sdl:+k-escape+ key)
     (clear-selection))
    ;; For zoom    
    ((and (= key sdl:+k-kp-plus+)
          (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+)))  ;; ctrl-fn-+ 
     (progn
       (decf *rz* *ratio*)
       (handle-zoom key)))

    ((and (modded (sdl:get-mod-state) sdl:+kmod-shift+)
          (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+))
          (= key sdl:+k-equals+)) ;; ctrl-shift-+
     (progn
       (decf *rz* *ratio*)
       (handle-zoom key)))

    ((and (= key sdl:+k-kp-minus+)
          (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+)))  ;; ctrl-fn--
;;          (and (= key sdl:+k-minus+)
;;               (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+)))) ;; ctrl--
      (progn
        (incf *rz* *ratio*)
        (handle-zoom key)))

;;     ((or (and (= key sdl:+k-kp-minus+)
;;               (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+)))  ;; ctrl-fn--
;;          (and (= key sdl:+k-minus+)
;;               (not (modded (sdl:get-mod-state) sdl:+kmod-ctrl+)))) ;; ctrl--
;;      (progn
;;        (decf *rz* *ratio*)
;;        (handle-zoom key)))
    ;; For rotation
    ((eql sdl:+k-right+ key)            ; right arrow
     (incf *yspeed* 1.0f0))
    ((eql sdl:+k-up+ key)               ; up arrow
     (decf *xspeed* 1.0f0))
    ((eql sdl:+k-left+ key)             ; left arrow
     (decf *yspeed* 1.0f0))
    ((eql sdl:+k-down+ key)             ; down arrow
     (incf *xspeed* 1.0f0))
    ((eql sdl:+k-pagedown+ key)         ; pgdown
     (incf *zspeed* 1.0f0))
    ((eql sdl:+k-pageup+ key)           ; pgup
     (decf *zspeed* 1.0f0))))

(defun resize0 (surface width height)
  (declare (ignorable surface))
  (when (zerop height)
    (setf height 1))
  (let ((ratio (d2float (/ width height))))
    (gl:viewport 0 0 width height)
    (gl:matrix-mode gl:+projection+)
    (gl:load-identity)
    (glu:perspective 45.0d0 ratio 0.1d0 100.0d0)
    (gl:matrix-mode gl:+modelview+)
    (gl:load-identity)
    t))

(defun initgl0 ()
  ;; Generally initialize
  (gl:shade-model gl:+smooth+)
  (gl:clear-color 0.1f0 0.1f0 0.2f0 0.0f0)
  (gl:clear-depth 1.0d0)
  (gl:enable gl:+depth-test+)
  (gl:depth-func gl:+lequal+)
  (gl:shade-model gl:+smooth+)
  (gl:hint gl:+perspective-correction-hint+ gl:+nicest+)
  (gl:enable gl:+color-material+)
  (gl:color-material gl:+front+ gl:+position+)

  ;; To draw sphere
  (sgum:with-single-float-values (light-ambient 0.3f0 0.3f0 0.3f0 1.0f0)
    (gl:light-fv gl:+light0+ gl:+ambient+ light-ambient))
  
  (sgum:with-single-float-values (light-position 0.0f0 3.0f0 2.0f0 0.0f0)
    (gl:light-fv gl:+light0+ gl:+position+ light-position))

  (sgum:with-single-float-values (light-diffuse 1.0f0 1.0f0 0.0f0 1.0f0)
    (gl:light-fv gl:+light0+ gl:+diffuse+ light-diffuse))

  (sgum:with-single-float-values (lmodel-ambient 0.4f0 0.4f0 0.4f0 1.0f0)
    (gl:light-model-fv gl:+light-model-ambient+ lmodel-ambient))

;;   (sgum:with-single-float-values (local-view 1.0f0)
;;     (gl:light-model-fv gl:+light-model-local-viewer+ local-view))
    
  (gl:enable gl:+lighting+)
  (gl:enable gl:+light0+)
  t)

(defun reset-camera ()
  (gl:clear-color 0.1f0 0.1f0 0.2f0 0.0f0)
  (gl:matrix-mode gl:+projection+)
  (gl:translate-f *x* *y* *z*)
  (gl:load-identity)
  )

(defun zoom0 ()
  ;;8.040 How do I implement a zoom operation?
  ;;http://www.opengl.org/resources/faq/technical/viewing.htm 
  (let* ((viewport (sgum:allocate-foreign-object gl:int 4))
         (x-orig) (y-orig)
         (x-size) (y-size))

    (gl:get-integerv gl:+viewport+ viewport)
    (setf x-orig (sgum:deref-array viewport gl:int 0)
          x-size (sgum:deref-array viewport gl:int 2)
          y-orig (sgum:deref-array viewport gl:int 1)
          y-size (sgum:deref-array viewport gl:int 3))
    
    (gl:matrix-mode gl:+projection+)
    (gl:load-identity)
    (glu:perspective (* 45d0 *rz*)
                     (d2float (/ (c2float x-size) (c2float y-size)))
                     0.1d0 100.0d0)
    (gl:matrix-mode gl:+modelview+)
    (gl:load-identity)))

(defun event-loop0 (surface update-fn resize-fn handle-keypress-fn bpp video-flags)
  (sdl:event-loop
   (:key-up (key)    
            (setf *pressed-key* nil
                  key nil))
   (:key-down (key)
              (setf *pressed-key* key)
	      (if (= key sdl:+k-q+)
                  (return)
                  (funcall handle-keypress-fn key)))
   (:mouse-button-up (x y button)
                     (if (eql nil button)
                         (progn
                           ;; When the user scrolls the wheel
                           (setf *pressed-button* sdl:+button-middle+)
                           (incf *z* *dz*))
                         (progn
                           (cond ((string= "LEFT" button)
                                  (progn
                                    (handle-mouse-up surface x y)
                                    (setf (aref *mb* sdl:+button-left+) nil)))
                                 ((string= "MIDDLE" button)
                                  (setf (aref *mb* sdl:+button-middle+) nil))
                                 (t
                                  (setf (aref *mb* sdl:+button-right+) nil))))))
   (:mouse-button-down (x y button)                    
                       (if (eql nil button)
                           (progn
                             (setf *pressed-button* sdl:+button-middle+)
                             (decf *z* *dz*))
                           (progn
                             (cond ((string= "LEFT" button)
                                    (setf *pressed-button* sdl:+button-left+)
                                    (setf (aref *mb* sdl:+button-left+) t))
                                   ((string= "MIDDLE" button)
                                    (setf *pressed-button* sdl:+button-middle+)
                                    (setf (aref *mb* sdl:+button-middle+) t))
                                   (t
                                    (setf *pressed-button* sdl:+button-right+)
                                    (setf (aref *mb* sdl:+button-right+) t)))))
                       (if (= sdl:+button-left+ *pressed-button*)
                           (handle-mouse-down surface x (- (sdl:surface-h surface) y)))
                       (if (= sdl:+button-right+ *pressed-button*)
                           (handle-rotation)))
   (:mouse-motion (x y xrel yrel state)
                  (declare (ignorable state))
                  ;; Set the current position of the mouse
                  (setf *mx* x)
                  (setf *my* y)
                  ;; when the right click event happens
                  (if (aref *mb* 3)
                      (progn
                        (incf *x* (* *dxm* xrel))
                        (incf *y* (* *dym* yrel))))
                  ;; when the middle click event happens
                  (if (aref *mb* 2)
                      (progn
                        ;; Moving mouse left <-> right rotates around Y axis and vice versa
                        (incf *xspeed* (* *dxr* yrel))
                        (incf *yspeed* (* *dyr* xrel))))
                  ;; when the left click event happens: hold and drag
                  (if (aref *mb* 1)
                      ;; Moving the object that is being selected
                      (handle-mouse-motion surface (* *dxm* xrel) (* *dym* yrel))))
  (:quit ()
         (return))
  (:resize (width height)
           (sdl:free-surface surface)
           (setf surface
                 (sdl:set-video-mode width height bpp video-flags))
           (funcall resize-fn surface width height))
  (:idle ()
         (funcall update-fn surface))))

(defun draw-axis (orig-point size)
  "Draw the axises of Decarte's coordinate system"
  (gl:translate-f (x orig-point) (y orig-point) (z orig-point))
  (setf size (c2float size))
  (gl:begin gl:+lines+)
  ;; Red for x axis
  (draw-line3d (make-point3d 0.0f0 0.0f0 0.0f0)
               (make-point3d size 0.0f0 0.0f0)
               1.0f0 (make-color3d 1.0f0 0.0f0 0.0f0))
  ;; Green for y axis
  (draw-line3d (make-point3d 0.0f0 0.0f0 0.0f0)
               (make-point3d 0.0f0 size 0.0f0)
               1.0f0 (make-color3d 0.0f0 1.0f0 0.0f0))
  ;; Blue for z axis
  (draw-line3d (make-point3d 0.0f0 0.0f0 0.0f0)
               (make-point3d 0.0f0 0.0f0 size)
               1.0f0 (make-color3d 0.0f0 0.0f0 1.0f0)))
  
;;; Functions are done when a mouse event happens
(defun handle-movement (object dx dy)
  (when (or (eq 'cube (type-of object))
            (eq 'sphere (type-of object)))
    (let ((new-center-position (center object)))
      (incf (x new-center-position) dx)
      (incf (y new-center-position) dy)
      (move object new-center-position))))

(defun handle-zoom (key)
  (zoom0)
  
;;   (format t "~%Done zoom: ~A"        
;;           (case key
;;             (sdl:+k-home+ "in")
;;             (sdl:+k-end+ "out")))
;;   (finish-output)
  )

(defun handle-rotation ()
  ())

(defun highlight (on-off highlight-color-edge highlight-color-molecule)
  (dolist (id *selected-objects*)
    (setf obj (nth id *scene*))
    (when (not (eq obj nil))
      (progn
        (setf (selected obj) on-off)
        (if (eql 'Line3D (type-of obj))                            
            (setf (color obj) highlight-color-edge))
        (if (eql 'Sphere (type-of obj))
            (setf (color obj) highlight-color-molecule))))))

(defun clear-selection ()
  (highlight off *original-color-edge* *original-color-molecule*)
  (setf *selected-objects* nil))
                               
(defun handle-selection (hits buffer)
  ;; Clear the last selections and restore objects' previous status
  (clear-selection)
  ;; Continue to handle with the selected operation if hits > 0
  (when (> hits 0)
    ;; Gets all the selected objects and place them into *selected-objects*
    ;; (format t "~%~A hits:" hits)
    (dotimes (i hits)
      ;;       (format t "~%Number: ~A ~%Min Z: ~A ~%Max Z: ~A ~%Name on stack: ~A"
      ;;               (sgum:deref-array buffer gl:uint (* i 4))
      ;;               (sgum:deref-array buffer gl:uint (+ (* i 4) 1))
      ;;               (sgum:deref-array buffer gl:uint (+ (* i 4) 2))
      ;;               (sgum:deref-array buffer gl:uint (+ (* i 4) 3)))
      ;; Get the objects' name on name stack and store into *selected-objects*
      (push (sgum:deref-array buffer gl:uint (+ (* i 4) 3)) *selected-objects*))
    ;; Get all the edges that connect to the nodes are being selected
    
    ;; Because the selection affects to the other operations such movement or rotation.
    ;; We will pick the latest object up and pass to the other ones. The *selected-objects*
    ;; is like a stack, thus the latest object is the last one in stack
    (setf *picked-object* (nth (car (last *selected-objects*)) *scene*)))
  ;; Highlight
  (highlight on *highlight-color-edge* *highlight-color-molecule*)
  *selected-objects*)
  
(defun handle-mouse-down (surface x y)
  ;; Select objects
  (let ((buffer (sgum:allocate-foreign-object gl:uint +selection-buffer-len+))
        (viewport (sgum:allocate-foreign-object gl:int 4))
        (x3d) (y3d) (z3d)
        (dx (sgum:allocate-foreign-object gl:double 1))
        (dy (sgum:allocate-foreign-object gl:double 1))
        (dz (sgum:allocate-foreign-object gl:double 1))
        (modelview *mvm*)
        (projection (sgum:allocate-foreign-object gl:double 16))
        (depth (sgum:allocate-foreign-object gl:float 1))
        (winx) (winy)
        (winz) (realy)
        (hits 0))
    ;; Start picking
    ;; Set up selection buffer
    (gl:select-buffer +selection-buffer-len+ buffer)
    ;; Get the viewport
    (gl:get-integerv gl:+viewport+ viewport)
    (gl:get-doublev gl:+modelview-matrix+ modelview)
    (gl:get-doublev gl:+projection-matrix+ projection)
    ;; (format t "~%Viewport: ~A" (loop for n from 0 to 3 collect (sgum:deref-array viewport gl:int n)))
    ;; Switch to projection and save the matrix
    (gl:matrix-mode gl:+projection+)
    (gl:read-pixels x (- (sgum:deref-array viewport gl:int 3) y)
                    1 1 gl:+depth-component+ gl:+float+ depth)
    (setf winx (d2float x)
          winy (d2float y))
    (setf realy (- (sgum:deref-array viewport gl:int 3) winy 1))
    (setf winz (d2float (sgum:deref-array depth gl:float 1)))
    (glu:unproject winx realy winz
                   modelview projection viewport
                   dx dy dz)
    (setf x3d (sgum:deref-pointer dx gl:double)
          y3d (sgum:deref-pointer dy gl:double)
          z3d (sgum:deref-pointer dz gl:double))
    ;; (format t "~%~A ~A ~A" x3d y3d z3d)
    (gl:push-matrix)
    ;; Change render mode
    (gl:render-mode gl:+select+)
    ;; Initialize the names stack
    (gl:init-names)
    (gl:push-name 0)
    ;; Establish new clipping volume to be unit cube around mouse cusor point (x, y) and extending two pixels
    ;; in the vertical and horizontal direction
    (gl:load-identity)
    (glu:pick-matrix (d2float x) (d2float y) 1d0 1d0 viewport)
    ;; Apply perspective matrix
    (glu:perspective 45d0
                     (d2float
                      (/ (- (sgum:deref-array viewport gl:int 2)
                            (sgum:deref-array viewport gl:int 0))
                         (- (sgum:deref-array viewport gl:int 3)
                            (sgum:deref-array viewport gl:int 1))))
                     0.1d0 100.0d0)
    ;; Draw scene
    (draw-metabolic-network surface)
    ;; Stop picking and process the object picked
    ;; Collect the hits, display or do another operations
    (setf hits (gl:render-mode gl:+render+))
    (handle-selection hits buffer)
    ;; Restore the projection matrix
    (gl:matrix-mode gl:+projection+)
    (gl:pop-matrix)
    ;; Go back to modelview for normal rendering
    (gl:matrix-mode gl:+modelview+)))

(defun handle-mouse-motion (surface dx dy)
  (declare (ignorable surface))
  (when (not (eql *picked-object* nil))
    (when (or (eql 'cube (type-of *picked-object*))
              (eql 'sphere (type-of *picked-object*)))
      ;;(draw-axis (center *picked-object*) 2.0)
      (let ((new-center-position (center *picked-object*)))
        (incf (x new-center-position) dx)
        (incf (y new-center-position) dy)
        (move *picked-object* new-center-position)))))
      
(defun handle-mouse-up (surface x y)
  (declare (ignorable surface x y))
  (when (or (eq 'cube (type-of *picked-object*))
            (eq 'sphere (type-of *picked-object*)))
    (display-point-3d (center *picked-object*))))

(defun init-sdl (&key (width 800) (height 600) (bpp 16) (title "Metabolic Network Editor 1.0")
                 (init-gl-fn #'initgl0)
                 (event-loop-fn #'event-loop0)
                 (update-fn #'identity)
                 (resize-fn #'resize0)
                 (handle-keypress-fn #'identity)
                 (video-flags (logior sdl:+opengl+ sdl:+resizable+ sdl:+gl-doublebuffer+ sdl:+hwpalette+)))
  
  (sdl:init sdl:+init-video+)

  (multiple-value-bind (hw-available-p blit-hw-p)
      (sdl:query-video-info :hw-available :blit-hw)
    (setf video-flags (logior video-flags
                              (if hw-available-p sdl:+hwsurface+ sdl:+swsurface+)
                              (if blit-hw-p sdl:+hwaccel+ 0))))
  
  (sdl:gl-set-attribute sdl:+gl-doublebuffer+ 1)
                                
  (let ((surface (sdl:set-video-mode width height bpp video-flags)))
    (when (sgum:null-pointer-p surface)
      (error "Unable to set video mode"))
    (unless (funcall init-gl-fn)
      (error "Failed to initialize GL"))
    (sdl:wm-set-caption title nil)
    ;; Call resize routine to the window
    (funcall resize-fn surface width height)
    (unwind-protect (funcall event-loop-fn surface update-fn resize-fn handle-keypress-fn bpp video-flags)
      (progn
        (update-graph-metabolic *m-graph*)
        (sdl:free-surface surface)
        (sdl:quit))))
  t)

(defun load-meta-tool-file ()
  "Load a graph from the meta tool file and return a metabolic graph"
  (format t "~%=======================================================")
  (format t "~%1. Load the meta tool file and create graph.")
  (setf *m-graph*
        (load-and-build-graph-metabolic *meta-tool-file*))
  t)

(defun initialise-positions ()
  "Initialise the postions and make nodes and edges"
  (format t "~%2. Initialise the positions of the nodes on the graph.")
  (setf *scene* nil)
  (random-layout *m-graph*)
  ;; Update the positions of the shapes. The *scene* is updated during exexcution of the below function
  (update-views *m-graph* *original-color-edge* *original-color-molecule*)
  ;; Get the original colors
  (setf *original-color-molecule* (get-color-molecule-node *m-graph*)
        *original-color-edge* (get-color-graph-edge *m-graph*)))

(defun apply-layout-paradigm ()
  "Apply a layout paradigm to the graph"
  (format t "~%3. Apply the layout algorithm on the graph.")
  ;;(circular-layout (get-reaction-nodes *m-graph*) 5.0)
  ;;(linear-layout (get-reaction-nodes *m-graph*) (make-point3d 0.0 0.0 0.0) "Z" 0.75 -5.0 5.0)
 )

(defun draw-metabolic-network (surface)
  (declare (ignorable surface))
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))  
  (gl:matrix-mode gl:+modelview+)
  (gl:push-matrix)
  (gl:load-identity)
  (glu:look-at 5d0 5d0 5d0
               0d0 0d0 0d0
               0d0 1d0 0d0)
  (gl:translate-f *x* *y* *z*)
  (gl:get-doublev gl:+modelview-matrix+ *mvm*)
  (gl:rotate-f *xspeed* 1f0 0f0 0f0)
  (gl:rotate-f *yspeed* 0f0 1f0 0f0)
  (gl:rotate-f *zspeed* 0f0 0f0 1f0)
  (gl:scale-f 0.5 0.5 0.5)
  (draw-axis (make-point3d *x* *y* *z*) 10.0f0)
  (draw-object *m-graph*)
  (gl:init-names)
  (gl:push-name 0)
  (dotimes (i (length *scene*))
    (gl:load-name i)
    (gl:pass-through (c2float i))
    (draw (nth i *scene*)))
  (gl:pop-matrix)
  (gl:flush)
  (sdl:gl-swap-buffers)
  t)

(defun visualise-metabolic-network ()
  "Visualise the metabolic network in OpenGL and SDL"
  (format t "~%4. Visualise the metabolic network in OpenGL and SDL.")
  (format t "~%=======================================================")
  (init-sdl :width *screen-width* :height *screen-height*
            :init-gl-fn #'initgl0
            :event-loop-fn #'event-loop0
            :update-fn #'draw-metabolic-network
            :resize-fn #'resize0
            :handle-keypress-fn #'handle-keypress-cabine)
  t)

(defun gl-draw (surface)
  (declare (ignorable surface))
  (gl:clear (logior gl:+color-buffer-bit+
                    gl:+depth-buffer-bit+))
  
  (gl:matrix-mode gl:+modelview+)
  (gl:push-matrix)
  (gl:load-identity)
  (glu:look-at 5d0 5d0 5d0
               0d0 0d0 0d0
               0d0 1d0 0d0)
  
  (gl:rotate-f *xspeed* 1f0 0f0 0f0)
  (gl:rotate-f *yspeed* 0f0 1f0 0f0)
  (gl:rotate-f *zspeed* 0f0 0f0 1f0)
  ;; Draw axises
  (draw-axis (make-point3d *x* *y* *z*) 5.0f0)
  ;; Draw an arrow
  (gl:translate-f *x* *y* *z*)
  (gl:rotate-f 90.0f0 0.0f0 0.0f0 0.0f0)
  (gl:scale-f 2.0 2.0 2.0)
  (draw-sphere 0.3f0 (make-point3d 1.0f0 0.0f0 0.0f0) (make-color3d 0.0f0 0.5f0 0.0f0))
  (draw-line3d (make-point3d 0.0f0 0.0f0 0.0f0)
               (make-point3d 1.5f0 2.0f0 0.0f0)
               2.0f0 (make-color3d 1.0f0 0.0f0 1.0f0))

;;  (draw-sphere 0.1f0 (make-point3d 0.0f0 2.0f0 3.0f0) (make-color3d 1.0f0 1.0f0 0.0f0))
;;  (draw-line3d (make-point3d 0.0f0 0.0f0 0.0f0)
;;               (make-point3d 2.0f0 3.0f0 0.0f0)
;;               2.0f0 (make-color3d 1.0f0 0.0f0 1.0f0))

  (gl:pop-matrix)
  (gl:flush)
  (gl:disable gl:+lighting+)
  (gl:disable gl:+light0+)
  (sdl:gl-swap-buffers))

(defun test ()
  (format t "~%Visualise the metabolic network.~%")
  (init-sdl :init-gl-fn #'initgl0
            :event-loop-fn #'event-loop0
            :update-fn #'gl-draw
            :resize-fn #'resize0
            :handle-keypress-fn #'handle-keypress-cabine)
  t)

(defun load-ui ()
  (format t "~%In construction"))
