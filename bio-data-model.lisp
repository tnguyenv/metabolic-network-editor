;;; -*- Mode: Lisp; Package: BIO-DATA -*-

;;; Copyright (c) 2009, The CABINE CREW
;;;   Nguyen V. N. TUNG      (nvntung@gmail.com)
;;;   Marie Beurton-AIMAR    (marie@labri.fr)
;;;   Francois VALLE         (valle@labri.fr)

;;; 
;;; This file is part of CABINE.
;;;

;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 2 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this library; if not, write to the Free Software 
;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
;;; 02111-1307 USA.


;;; This package defines objects that store biological objects

(in-package :bio-data)

;;; Defines the biological data model using the composite pattern
(defclass Biological-Process ()
  ((process-name   :accessor process-name   :initform nil   :initarg :process-name
                   :documentation "The name of biological process")
   (process-position    :accessor process-position    :initarg :process-position    :initform nil
                        :documentation "The postion of the biological process")))

(defclass Biological-Reaction (Biological-Process)
  ((reactants :accessor reactants    :initarg :reactants    :initform nil
              :documentation "The leftside of a reaction in biology.")
   (products  :accessor products     :initarg :products    :initform nil
              :documentation "The rightside of a reaction in biology.")
   (enzyme    :accessor enzyme       :initarg :enzyme    :initform nil
              :documentation "The chemical compound that catalysing in the reaction")))

(defclass Biological-Pathway (Biological-Process)
  ((reactions    :accessor reactions     :initarg :reactions    :initform nil
                 :documentation "Metabolic pathway is a series of chemical reactions occuring within a cell.")))

(defclass Biological-Molecule ()
  ((molecule-name     :accessor molecule-name  :initarg :molecule-name
                      :documentation "Molecule's name")
   (molecule-position :accessor molecule-position :initarg :molecule-position :initform nil
                      :documentation "Molecule's position")))

(defclass Biological-Metabolite (Biological-Molecule)
  ())

(defclass Biological-Enzyme (Biological-Molecule)
  ())

(defclass Metabolic-Network ()
  ((biological-processes :accessor biological-processes :initarg :biological-processes :initform nil
                         :documentation "The list of the processes occuring in the metabolic network.")
   (biological-objects   :accessor biological-objects   :initarg :biological-objects   :initform nil
                         :documentation "A metabolic network consists of biological objects and biological graphs.")))

;;; Implements the constructor functions
(defgeneric make-biological-molecule (name position)
  (:documentation "Create a new biological molecule which properties are name and position."))
(defmethod make-biological-molecule (name position)
  (make-instance 'Biological-Molecule :molecule-name name :molecule-position position))

(defgeneric make-biological-pathway (name position reactions)
  (:documentation "Create a new biological pathway which properties are name, position and reactions."))
(defmethod make-biological-pathway (name position reactions)
  (make-instance 'Biological-Pathway :process-name name :process-position position :reactions reactions))

(defgeneric make-biological-reaction (name position reactants products enzyme)
  (:documentation "Create a new biological reaction which properties are passed."))
(defmethod make-biological-reaction (name position reactants products enzyme)
  (make-instance 'Biological-Reaction :process-name name :process-position position
                 :reactants reactants :products products :enzyme enzyme))
(defgeneric make-metabolic-network (processes objects)
  (:documentation "Create a new metabolic network which properties are passed."))
(defmethod make-metabolic-network (processes objects)
  (make-instance 'Metabolic-Network :biological-processes processes :biological-objects objects))

;;; Implements the methods
(defgeneric display-biological-process (process) 
  (:documentation "Display information of a biological process. Just used for testing."))
(defmethod display-biological-process ((process Biological-Process))
  (format t "~%~A" (process-name process)))
(defmethod display-biological-process ((process Biological-Reaction))
  (format t "~%~A : ~A = ~A" (process-name process) (reactants process) (products process)))
(defmethod display-biological-process :after ((pathway Biological-Pathway))
  (mapc #'display-biological-process (reactions pathway)))

(defgeneric insert-biological-process (process &rest args) 
  (:documentation "Insert a new biological process(es) into the biological pathway."))
(defmethod insert-biological-process ((pathway Biological-Pathway) &rest args)
  (setf (reactions pathway) (append args (reactions pathway))))

(defgeneric remove-biological-process (pathway &rest args)
  (:documentation "Remove reaction(s) from pathway."))
(defmethod remove-biological-process ((pathway Biological-Pathway) &rest arg)
  (setf (reactions pathway) (remove arg (reactions pathway) :count 1)))

;;; Implements the methods of the Metabolic-Network class
(defgeneric insert-process (process network)
  (:documentation "Insert a biological process into the network."))
(defmethod insert-process ((process Biological-Process) (mb-network Metabolic-Network))
  (setf (biological-processes mb-network) (cons process (biological-processes mb-network))))

(defgeneric insert-molecule (molecule mb-network)
  (:documentation "Insert the molecule into the metabolic network"))
(defmethod insert-molecule ((molecule Biological-Molecule) (mb-network Metabolic-Network))
  (setf (biological-objects mb-network) (cons molecule (biological-objects mb-network))))
